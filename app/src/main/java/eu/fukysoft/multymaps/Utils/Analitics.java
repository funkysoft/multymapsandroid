package eu.fukysoft.multymaps.Utils;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
/**
 * Created by Funky on 1/27/2018.
 */

public class Analitics {

    private static FirebaseAnalytics mFirebaseAnalytics;

    public static void init(Context context){
        Fabric.with(context, new Crashlytics());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public static void logEvents(String type, String name){
        Bundle bundle = new Bundle();
        bundle.putString(type, name);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }
}
