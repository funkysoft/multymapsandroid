package eu.fukysoft.multymaps.Utils;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.SphericalUtil;
import com.jrummyapps.android.colorpicker.ColorPickerDialog;

import java.util.ArrayList;
import java.util.List;

import eu.fukysoft.multymaps.Activitys.ActivityGoogleMap;
import eu.fukysoft.multymaps.Adpaters.AdapterOnlineTheme;
import eu.fukysoft.multymaps.Api.ApiClient;
import eu.fukysoft.multymaps.Api.MapApi;
import eu.fukysoft.multymaps.Api.Models.Style;
import eu.fukysoft.multymaps.Api.Models.Themes;
import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Majo on 22.7.2017.
 */

public class Dialogs {
    private static int submit;

    public static MaterialDialog infoDialog(Context context, List<LatLng> latLngList, final DialogDimmis listener) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_info_dialog, null, false);

        TextView areaText = (TextView) view.findViewById(R.id.text_area);
        areaText.setText("" + Tools.toUnitsQuad(SphericalUtil.computeArea(latLngList),context));

        TextView distanceText = (TextView) view.findViewById(R.id.text_distance);
        distanceText.setText("" + Tools.toUnitsLenght(SphericalUtil.computeLength(latLngList),context));

        TextView pointsText = (TextView) view.findViewById(R.id.text_points);
        pointsText.setText("" + latLngList.size());

        view.findViewById(R.id.text_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDimmis();
            }
        });

        return new MaterialDialog.Builder(context).customView(view, false).build();

    }

    public static MaterialDialog saveMapsDialog(final Context context, final DialogMapsSave listener) {

        View saveDialogView = LayoutInflater.from(context).inflate(R.layout.layout_save_dialog, null);
        final EditText editTextSaveMapName = (EditText) saveDialogView.findViewById(R.id.edittext_save_map_name);
        saveDialogView.findViewById(R.id.button_save_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSave(editTextSaveMapName.getText().toString());


            }
        });
        return new MaterialDialog.Builder(context).customView(saveDialogView, false).build();

    }

    public static MaterialDialog markerInfoDialog(Context context, List<LatLng> latLngList, String note, final DialogDimmis listener, final MarkerNoteChaneg listenerNote) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_marker_info_dialog, null, false);

        TextView areaText = (TextView) view.findViewById(R.id.text_latitude);
        TextView distanceText = (TextView) view.findViewById(R.id.text_longitude);

        if (!latLngList.isEmpty()) {
            areaText.setText("" + latLngList.get(0).latitude);
            distanceText.setText("" + latLngList.get(0).longitude);
        }

        EditText noteText = (EditText) view.findViewById(R.id.edittext_note);
        noteText.setText(note);
        noteText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (listenerNote != null) {
                    listenerNote.onChange(s.toString());
                }
            }
        });

        view.findViewById(R.id.text_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDimmis();
            }
        });

        return new MaterialDialog.Builder(context).customView(view, false).build();

    }

    public static MaterialDialog textInputDialog(Context context, String note, final DialogDimmis listener, final MarkerNoteChaneg listenerNote) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_text_input_dialog, null, false);


        EditText noteText = (EditText) view.findViewById(R.id.edittext_note);
        noteText.setText(note);
        noteText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (listenerNote != null) {
                    listenerNote.onChange(s.toString());
                }
            }
        });

        view.findViewById(R.id.text_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDimmis();
            }
        });

        return new MaterialDialog.Builder(context).customView(view, false).build();

    }

    public static MaterialDialog weightDialog(Context context, final int weight, final DialogClose listener) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_weight_dialog, null, false);

        submit = weight;
        SeekBar weightSeekBar = (SeekBar) view.findViewById(R.id.seek_bar_weight);
        weightSeekBar.setProgress(submit);

        final TextView textViewWeight = (TextView) view.findViewById(R.id.text_weight_dialog);
        textViewWeight.setText("" + submit);

        weightSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (weight != progress) {
                    submit = progress;
                }

                textViewWeight.setText("" + submit);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        view.findViewById(R.id.text_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onSubmit(submit);
            }
        });

        view.findViewById(R.id.text_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDimmis();
            }
        });

        return new MaterialDialog.Builder(context).cancelable(false).customView(view, false).build();

    }

    public static MaterialDialog markerDialog(Context context, final DialogClose listener) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_marker_dialog, null, false);


        view.findViewById(R.id.text_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDimmis();
            }
        });

        return new MaterialDialog.Builder(context).customView(view, false).build();

    }

    public static MaterialDialog aboutDialog(Context context, final DialogDimmis listener) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_about_dialog, null, false);


        view.findViewById(R.id.text_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDimmis();
            }
        });

        return new MaterialDialog.Builder(context).customView(view, false).build();

    }

    public static MaterialDialog saveDialog(Context context, String type, final DialogSave listener) {
        View saveDialogView = LayoutInflater.from(context).inflate(R.layout.layout_save_dialog, null);
        final EditText editTextSaveMapName = (EditText) saveDialogView.findViewById(R.id.edittext_save_map_name);
        TextView dialogText = (TextView) saveDialogView.findViewById(R.id.text_dialog_save);
        dialogText.setText(context.getString(R.string.save_object));
        editTextSaveMapName.setHint(type);
        saveDialogView.findViewById(R.id.button_save_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onSave(editTextSaveMapName.getText().toString());
            }
        });

        return new MaterialDialog.Builder(context).customView(saveDialogView, false).build();
    }

    public static MaterialDialog lostDataDialog(Context context, final DialogLostData listener) {
        View dialogLostDataView = LayoutInflater.from(context).inflate(R.layout.layout_data_lost_dialog, null);
        dialogLostDataView.findViewById(R.id.button_lost_data_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listener != null) listener.onCancal();
            }
        });

        dialogLostDataView.findViewById(R.id.button_lost_data_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onSave();

            }
        });

        dialogLostDataView.findViewById(R.id.button_lost_data_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDontSave();

            }
        });
        return new MaterialDialog.Builder(context).customView(dialogLostDataView, false).build();


    }

    public static MaterialDialog sureDialog(Context context, final DialogSure listener) {
        View dialogLostDataView = LayoutInflater.from(context).inflate(R.layout.layout_sure_dialog, null);
        dialogLostDataView.findViewById(R.id.button_sure_cancal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listener != null) listener.onDimmis();
            }
        });

        dialogLostDataView.findViewById(R.id.button_sure_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onSubmit();
            }
        });

        return new MaterialDialog.Builder(context).customView(dialogLostDataView, false).build();
    }

    public static MaterialDialog sureEraseDialog(Context context, int count, final DialogSure listener) {
        View dialogLostDataView = LayoutInflater.from(context).inflate(R.layout.layout_sure_erase_dialog, null);
        TextView textEraseContu = (TextView) dialogLostDataView.findViewById(R.id.text_erase_count);

        if (count > 1)
            textEraseContu.setText(context.getString(R.string.are_you_sure) + "\n" + count + " " + context.getString(R.string.items_has_been));
        else
            textEraseContu.setText(context.getString(R.string.are_you_sure) + "\n" + count + " " + context.getString(R.string.item_have_been));

        dialogLostDataView.findViewById(R.id.button_sure_cancal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDimmis();
            }
        });

        dialogLostDataView.findViewById(R.id.button_sure_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onSubmit();
            }
        });
        return new MaterialDialog.Builder(context).customView(dialogLostDataView, false).build();
    }

    public static MaterialDialog importDialog(Context context, final DialogSave listener) {
        View importDialogView = LayoutInflater.from(context).inflate(R.layout.layout_import_dialog, null);
        final EditText editTextImportJson = (EditText) importDialogView.findViewById(R.id.edittext_save_map_name);
        importDialogView.findViewById(R.id.button_import).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSave(editTextImportJson.getText().toString());
            }
        });
        importDialogView.findViewById(R.id.button_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextImportJson.setText("");
            }
        });

        return new MaterialDialog.Builder(context).customView(importDialogView, false).build();
    }

    public static MaterialDialog messageDialog(Context context, String tittle, String message) {
        View messageDialogView = LayoutInflater.from(context).inflate(R.layout.layout_message_dialog, null);

        TextView textViewTittle = (TextView) messageDialogView.findViewById(R.id.textview_tittle);
        TextView textViewMessage = (TextView) messageDialogView.findViewById(R.id.textview_message);

        textViewTittle.setText(tittle);
        textViewMessage.setText(message);

        return new MaterialDialog.Builder(context).customView(messageDialogView, false).build();
    }

    public static Dialog progressDialog(Context context){
        View progresseDialogView = LayoutInflater.from(context).inflate(R.layout.layout_progress_dialog, null);
        Dialog progressDialog = new Dialog(context);
        progressDialog.setContentView(progresseDialogView);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    public static ColorPickerDialog.Builder colorDialog(int id, int color) {

        if (id == ActivityGoogleMap.COLOR_DIALOG_ID_MAPCUSTOM)
            return ColorPickerDialog.newBuilder().setDialogId(id).setColor(color).setShowAlphaSlider(false);

        if (id == ActivityGoogleMap.COLOR_DIALOG_ID_POLYGON_FILL)
            return ColorPickerDialog.newBuilder().setDialogId(id).setColor(color).setShowAlphaSlider(true);

        if (id == ActivityGoogleMap.COLOR_DIALOG_ID_POLYGON_STROKE)
            return ColorPickerDialog.newBuilder().setDialogId(id).setColor(color).setShowAlphaSlider(true);

        if (id == ActivityGoogleMap.COLOR_DIALOG_ID_MAPCUSTOM_HUE)
            return ColorPickerDialog.newBuilder().setDialogId(id).setColor(color).setShowAlphaSlider(false);

        return ColorPickerDialog.newBuilder().setDialogId(id).setColor(color).setShowAlphaSlider(true);
    }


    public interface DialogDimmis {
        void onDimmis();
    }

    public interface MarkerNoteChaneg {
        void onChange(String note);
    }

    public interface DialogClose {
        void onDimmis();

        void onSubmit(int item);
    }

    public interface DialogSure {
        void onDimmis();

        void onSubmit();
    }

    public interface DialogLostData {
        void onSave();

        void onDontSave();

        void onCancal();
    }

    public interface DialogSave {
        void onSave(String text);
    }

    public interface DialogTheme {
        void onDimmis();

        void onSubmit(String json);
    }

    public interface DialogMapsSave {
        void onDimmis();

        void onSave(String saveText);
    }
}
