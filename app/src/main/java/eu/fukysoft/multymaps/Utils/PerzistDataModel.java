package eu.fukysoft.multymaps.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import eu.fukysoft.multymaps.Models.LibraryItem;


/**
 * Created by Funky on 10/20/2017.
 */

public class PerzistDataModel {

    /**
     *
     *
     * MAIN CONTOL APPLICATION PERZISTENT DATA MODEL
     *
     *
     */

    private static final String FIRST_START_APP = "app_first_start";

    public static void setFirstStartApp(Context context, boolean isFirstStart) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefSave.edit();
        editor.putBoolean(FIRST_START_APP, isFirstStart).apply();
    }

    public static boolean getFirstStartApp(Context context) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getBoolean(FIRST_START_APP, true);
    }
    /**
     *
     *
     * SAVING MAP PROPERYTIES PERZISTENT DATA MODEL
     *
     *
     */

    private static final String PREF_KEY = "SAVED_MAPS";
    private static final String SAVE_COUNT_KEY = "SAVE_MAPS_COUNTER";
    private static final String SAVE_NAME_KEY = "SAVE_MAP_NAME";
    private static final String SAVE_MAP_TYPE_KEY = "SAVE_MAP_TYPE";
    private static final String SAVE_JSON_STYLE_KEY = "SAVE_JSON_STYLE";
    private static final String SAVE_JSON_OBJECTS_KEY = "SAVE_JSON_OBJECTS";

    public static int getSavedMapsCounter(Context context) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getInt(SAVE_COUNT_KEY, 0);
    }

    public static String getSavedMapName(Context context, int position) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getString(SAVE_NAME_KEY + position, "");
    }

    public static String getSavedMapObjectJson(Context context, int position) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getString(SAVE_JSON_OBJECTS_KEY + position, "");
    }

    public static String getSavedMapStyleJson(Context context, int position) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getString(SAVE_JSON_STYLE_KEY + position, "");
    }

    public static String getSavedMapType(Context context, int position) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getString(SAVE_MAP_TYPE_KEY + position, "");
    }

    public static ArrayList<LibraryItem> refreshSavedMaps(Context context, ArrayList<Integer> removeLibIds) {

        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefSave.edit();
        int savedCounter = sharedPrefSave.getInt(SAVE_COUNT_KEY, 0);
        final ArrayList<LibraryItem> tempSave = new ArrayList<>();

        for (int i = 0; i < savedCounter; i++) {
            if (!removeLibIds.contains(i)) {
                LibraryItem libraryItem = new LibraryItem(sharedPrefSave.getString(SAVE_NAME_KEY + i, ""), false);
                libraryItem.setJson(sharedPrefSave.getString(SAVE_JSON_OBJECTS_KEY + i, ""));
                libraryItem.setJson(sharedPrefSave.getString(SAVE_MAP_TYPE_KEY + i, ""));
                tempSave.add(libraryItem);
            }
        }

        editor.putInt(SAVE_COUNT_KEY, tempSave.size()).apply();

        for (int i = 0; i < tempSave.size(); i++) {
            editor.putString(SAVE_MAP_TYPE_KEY + i, tempSave.get(i).getMapType()).apply();
            editor.putString(SAVE_NAME_KEY + i, tempSave.get(i).getName()).apply();
            editor.putString(SAVE_JSON_OBJECTS_KEY + i, tempSave.get(i).getJson()).apply();
        }

        return tempSave;
    }

    public static void saveMap(Context context, String name, String styleJson, String mapType, String objectJson) {

        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefSave.edit();

        int savedCounter = sharedPrefSave.getInt(SAVE_COUNT_KEY, 0);

        editor.putString(SAVE_MAP_TYPE_KEY + savedCounter, mapType).apply();
        editor.putString(SAVE_NAME_KEY + savedCounter, name).apply();
        editor.putString(SAVE_JSON_STYLE_KEY + savedCounter, styleJson).apply();
        editor.putString(SAVE_JSON_OBJECTS_KEY + savedCounter, objectJson).apply();
        editor.putInt(SAVE_COUNT_KEY, savedCounter + 1).apply();

    }

    /**
     *
     *
     * AUTO SAVING PERZISTENT DATA MODEL
     *
     *
     */

    private static final String SAVE_MAP_TYPE_KEY_AUTO = "AUTO_SAVE_MAP_TYPE";
    private static final String SAVE_JSON_STYLE_KEY_AUTO = "AUTO_SAVE_JSON_STYLE";
    private static final String SAVE_JSON_OBJECTS_KEY_AUTO = "AUTO_SAVE_JSON_OBJECTS";
    private static final String SAVE_LAT = "AUTO_SAVE_LAT";
    private static final String SAVE_LON = "AUTO_SAVE_LON";
    private static final String SAVE_TITL = "AUTO_SAVE_TITL";
    private static final String SAVE_ZOOM = "AUTO_SAVE_ZOOM";
    private static final String SAVE_BEARING = "AUTO_SAVE_BEARING";

    public static void AutoSaveMap(
            Context context,
            String styleJson,
            String objectJson,
            String mapType,
            double lat,
            double lon,
            float zoom,
            float titl,
            float bearing) {

        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefSave.edit();
        editor.putString(SAVE_MAP_TYPE_KEY_AUTO, mapType).apply();
        editor.putString(SAVE_JSON_STYLE_KEY_AUTO, styleJson).apply();
        editor.putString(SAVE_JSON_OBJECTS_KEY_AUTO, objectJson).apply();
        editor.putFloat(SAVE_LAT, (float) lat).apply();
        editor.putFloat(SAVE_LON, (float) lon).apply();
        editor.putFloat(SAVE_TITL, titl).apply();
        editor.putFloat(SAVE_ZOOM, zoom).apply();
        editor.putFloat(SAVE_BEARING, bearing).apply();
    }

    public static String[] AutoLoadMap(Context context) {
        String[] result = new String[8];
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);

        result[0] = sharedPrefSave.getString(SAVE_MAP_TYPE_KEY_AUTO, "");
        result[1] = sharedPrefSave.getString(SAVE_JSON_STYLE_KEY_AUTO, "");
        result[2] = sharedPrefSave.getString(SAVE_JSON_OBJECTS_KEY_AUTO, "");

        result[3] = (""+sharedPrefSave.getFloat(SAVE_LAT,0));
        result[4] = (""+sharedPrefSave.getFloat(SAVE_LON,0));
        result[5] = (""+sharedPrefSave.getFloat(SAVE_TITL,0));
        result[6] = (""+sharedPrefSave.getFloat(SAVE_ZOOM,0));
        result[7] = (""+sharedPrefSave.getFloat(SAVE_BEARING,0));

        return result;
    }

    /**
     *
     *
     * APPLICATION SETTINGS PERZISTENT DATA MODEL
     *
     *
     */

    private static final String SETTINGS_LENGHT = "sett_lenght";
    private static final String SETTINGS_AREA = "sett_area";
    private static final String SETTINGS_SCALE = "sett_scale";
    private static final String SETTINGS_AUTOSAVE_POSITION = "sett_autosave_position";
    private static final String SETTINGS_AUTOSAVE_STYLE = "sett_autosave_style";
    private static final String SETTINGS_AUTOSAVE_OBJECTS = "sett_autosave_objects";

    public static void initSettings(
            Context context,
            String lenghtUnits,
            String areaUnits,
            boolean isScaleShow,
            boolean isAutoSavePosition,
            boolean isAutoSaveStyle,
            boolean isAutoSaveObjects){
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefSave.edit();

        editor.putString(SETTINGS_LENGHT, lenghtUnits).apply();
        editor.putString(SETTINGS_AREA, areaUnits).apply();
        editor.putBoolean(SETTINGS_SCALE, isScaleShow).apply();
        editor.putBoolean(SETTINGS_AUTOSAVE_POSITION, isAutoSavePosition).apply();
        editor.putBoolean(SETTINGS_AUTOSAVE_STYLE, isAutoSaveStyle).apply();
        editor.putBoolean(SETTINGS_AUTOSAVE_OBJECTS, isAutoSaveObjects).apply();
    }

    public static String getLenghtUnits(Context context) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getString(SETTINGS_LENGHT, "");
    }

    public static String getAreaUnits(Context context) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getString(SETTINGS_AREA, "");
    }

    public static boolean isAutoSavePosition(Context context) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getBoolean(SETTINGS_AUTOSAVE_POSITION,true);
    }

    public static boolean isAutoSaveStyle(Context context) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getBoolean(SETTINGS_AUTOSAVE_STYLE,true);
    }

    public static boolean isAutoSaveObjects(Context context) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getBoolean(SETTINGS_AUTOSAVE_OBJECTS,true);
    }

    public static boolean isScaleShow(Context context) {
        SharedPreferences sharedPrefSave = context.getApplicationContext().getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPrefSave.getBoolean(SETTINGS_SCALE,true);
    }
}
