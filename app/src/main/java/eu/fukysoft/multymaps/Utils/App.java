package eu.fukysoft.multymaps.Utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by User on 30.11.2016.
 */

public class App extends Application {

    public static final String METRIC_UNITS = "metric";
    public static final String US_UNITS = "us";

    @Override
    public void onCreate() {
        super.onCreate();
        if (PerzistDataModel.getFirstStartApp(getApplicationContext())) {
            PerzistDataModel.setFirstStartApp(getApplicationContext(), false);
            PerzistDataModel.initSettings(
                    getApplicationContext(),
                    METRIC_UNITS,
                    METRIC_UNITS,
                    true,
                    true,
                    true,
                    true);
        }
    }
}