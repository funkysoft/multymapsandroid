package eu.fukysoft.multymaps.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.widget.ArrayAdapter;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import eu.fukysoft.multymaps.Activitys.ActivityGoogleMap;
import eu.fukysoft.multymaps.Models.ColorRGB;
import eu.fukysoft.multymaps.Models.GeoJson;
import eu.fukysoft.multymaps.Models.GraphicJson;
import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.R;


/**
 * Created by Majo on 4.6.2017.
 */

public class Tools {

    public static int getStatusBarHeight(Activity activity) {
        int result = 0;
        int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int[] getDisplayDimension(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int[] dimension = new int[2];
        dimension[0] = metrics.heightPixels;
        dimension[1] = metrics.widthPixels;

        return dimension;
    }

    public static String toUnitsQuad(double value, Context context) {
        String unit;
        double result;
        if (PerzistDataModel.getLenghtUnits(context).equals(App.US_UNITS)) {
            value = value * 10.7639104;
            unit = "sq ft";
            result = value;
            if (value > 43560) {
                unit = "acre";
                result = value / 43560;
            }
            if (value > 27878400) {
                unit = "sq mile";
                result = value / 27878400;
            }

        } else {
            unit = "m2";
            result = value;
            if (value > 1000000) {
                unit = "km2";
                result = value / 1000000;
            }
        }
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());

        if (result == 0) {
            formatter.applyPattern("#,##0");
        } else {
            formatter.applyPattern("#,##0.00");
        }
        return formatter.format(result) + " " + unit;

    }

    public static String toUnitsLenght(double value, Context context) {
        double result;
        String unit;
        if (PerzistDataModel.getLenghtUnits(context).equals(App.US_UNITS)) {
            value = value * 3.2808399;
            unit = "ft";
            result = value;
            if (value > 5280) {
                unit = "mile";
                result = value / 5280;
            }
        } else {
            unit = "m";
            result = value;
            if (value > 1000) {
                unit = "km";
                result = value / 1000;
            }
        }
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());

        if (result == 0) {
            formatter.applyPattern("#,##0");
        } else {
            formatter.applyPattern("#,##0.00");
        }
        return formatter.format(result) + " " + unit;

    }

    public static String toUnitsElevation(double value, Context context) {
        double result;
        String unit;
        if (PerzistDataModel.getLenghtUnits(context).equals(App.US_UNITS)) {
            value = value * 3.2808399;
            unit = "ft";
            result = value;
        } else {
            unit = "m";
            result = value;
        }
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());

        if (result == 0) {
            formatter.applyPattern("#,##0");
        } else {
            formatter.applyPattern("#,##0.00");
        }
        return formatter.format(result) + " " + unit;

    }

    public static ArrayList<Double> getLat(ArrayList<LatLng> latLngs) {
        ArrayList<Double> tempList = new ArrayList<>();

        for (int l = 0; l < latLngs.size(); l++) {
            tempList.add(latLngs.get(l).latitude);
        }

        return tempList;
    }

    public static ArrayList<Double> getLong(ArrayList<LatLng> latLngs) {
        ArrayList<Double> tempList = new ArrayList<>();

        for (int l = 0; l < latLngs.size(); l++) {
            tempList.add(latLngs.get(l).longitude);
        }

        return tempList;
    }

    public static ArrayList<LatLng> getLatLongList(ArrayList<Double> lan, ArrayList<Double> lon) {
        ArrayList<LatLng> tempList = new ArrayList<>();
        for (int l = 0; l < lan.size(); l++) {
            LatLng latLng = new LatLng(lan.get(l), lon.get(l));
            tempList.add(latLng);
        }
        return tempList;
    }

    public static CameraUpdate getZoomOfObject(MapObject mapObject, ActivityGoogleMap activity, int topBarSize) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        int height = getDisplayDimension(activity)[0] - topBarSize;
        int width = getDisplayDimension(activity)[1];

        for (int d = 0; d < mapObject.lonList.size(); d++) {
            builder.include(new LatLng(mapObject.latList.get(d), mapObject.lonList.get(d)));
        }
        LatLngBounds bounds = builder.build();
        int padding = (int) (width * 0.02);
        return CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

    }

    public static Spannable formatJsonString(String text, Context context) {

        StringBuilder json = new StringBuilder();
        String indentString = "";

        boolean inQuotes = false;
        boolean isEscaped = false;

        for (int i = 0; i < text.length(); i++) {
            char letter = text.charAt(i);

            switch (letter) {
                case '\\':
                    isEscaped = !isEscaped;
                    break;
                case '"':
                    if (!isEscaped) {
                        inQuotes = !inQuotes;
                    }
                    break;
                default:
                    isEscaped = false;
                    break;
            }

            if (!inQuotes && !isEscaped) {
                switch (letter) {
                    case '{':
                    case '[':
                        json.append("\n" + indentString + letter + "\n");
                        indentString = indentString + "\t";
                        json.append(indentString);
                        break;
                    case '}':
                    case ']':
                        indentString = indentString.replaceFirst("\t", "");
                        json.append("\n" + indentString + letter);
                        break;
                    case ',':
                        json.append(letter + "\n" + indentString);
                        break;
                    default:
                        json.append(letter);
                        break;
                }
            } else {
                json.append(letter);
            }
        }

        return changeCharsColor(json.toString(), context);
    }

    public static Spannable changeCharsColor(String json, Context context) {
        Spannable spannable = new SpannableString(json);

        for (int s = 0; s < json.length(); s++) {
            String substring = json.substring(s, s + 1);

            if (substring.contains("{") ||
                    substring.contains("}") ||
                    substring.contains("[") ||
                    substring.contains("]") ||
                    substring.contains(","))
                spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorPrimary)), s, s + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return spannable;
    }

    public static ArrayList<MapObject> geoJsonToMapObjects(String text) {
        Gson gson = new Gson();
        ArrayList<MapObject> tempMapObjects = new ArrayList<>();
        JsonObject jsonObject = gson.fromJson(text, new TypeToken<JsonObject>() {
        }.getType());
        GeoJson geoJson = new GeoJson();
        if (jsonObject.get("type").getAsString().equals("FeatureCollection")) {
            geoJson.setType("FeatureCollection");
            JsonArray jsonArray = jsonObject.getAsJsonArray("features");
            if (jsonArray != null) {
                geoJson.setFeatures(new ArrayList<GeoJson.Feature>());
                for (int a = 0; a < jsonArray.size(); a++) {
                    if (jsonArray.get(a).getAsJsonObject().get("geometry").getAsJsonObject().get("type").getAsString() != null) {
                        GeoJson.Feature feature = new GeoJson.Feature();
                        feature.setType("Feature");
                        feature.setGeometry(new GeoJson.Geometry());
                        switch (jsonArray.get(a).getAsJsonObject().get("geometry").getAsJsonObject().get("type").getAsString()) {
                            case "Point":
                                feature.getGeometry().setType("Point");
                                JsonArray cords = jsonArray.get(a).getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray();
                                if (cords != null && cords.size() == 2) {
                                    ArrayList<ArrayList<Double>> listCord = new ArrayList<>();
                                    ArrayList<Double> cordis = new ArrayList<>();
                                    cordis.add(cords.get(1).getAsDouble());
                                    cordis.add(cords.get(0).getAsDouble());
                                    listCord.add(cordis);
                                    feature.getGeometry().setCoordinates(listCord);
                                }
                                break;
                            case "LineString":
                                feature.getGeometry().setType("LineString");
                                JsonArray cor = jsonArray.get(a).getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray();
                                ArrayList<ArrayList<Double>> listCord = new ArrayList<>();
                                for (int x = 0; x < cor.size(); x++) {
                                    JsonArray cordsl = cor.get(x).getAsJsonArray();
                                    if (cordsl != null && cordsl.size() == 2) {
                                        ArrayList<Double> cordis = new ArrayList<>();
                                        cordis.add(cordsl.get(1).getAsDouble());
                                        cordis.add(cordsl.get(0).getAsDouble());
                                        listCord.add(cordis);
                                    }
                                }
                                feature.getGeometry().setCoordinates(listCord);
                                break;
                            case "Polygon":
                                feature.getGeometry().setType("Polygon");
                                JsonArray corp = jsonArray.get(a).getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray();
                                ArrayList<ArrayList<Double>> listCordp = new ArrayList<>();
                                for (int x = 0; x < corp.size(); x++) {
                                    JsonArray corpin = corp.get(x).getAsJsonArray();
                                    for (int t = 0; t < corpin.size(); t++) {
                                        JsonArray cordsp = corpin.get(t).getAsJsonArray();
                                        if (cordsp != null && cordsp.size() == 2) {
                                            ArrayList<Double> cordis = new ArrayList<>();
                                            cordis.add(cordsp.get(1).getAsDouble());
                                            cordis.add(cordsp.get(0).getAsDouble());
                                            listCordp.add(cordis);
                                        }
                                    }
                                }
                                feature.getGeometry().setCoordinates(listCordp);
                                break;
                        }

                        if (jsonArray.get(a).getAsJsonObject().get("geometry").getAsJsonObject().get("properties") != null) {
                            String name = jsonArray.get(a).
                                    getAsJsonObject().get("properties").
                                    getAsJsonObject().get("name").getAsString();
                            if (name != null) {
                                feature.setProperties(new GeoJson.Properties());
                                feature.getProperties().setName(name);
                            }
                        }
                        geoJson.getFeatures().add(feature);

                    }
                }
            }
        }

        if (geoJson.getType() != null && geoJson.getType().equals("FeatureCollection")) {
            if (geoJson.getFeatures() != null) {

                for (GeoJson.Feature feature : geoJson.getFeatures()) {

                    if (feature.getType() != null && feature.getType().equals("Feature")) {
                        MapObject mapObject = new MapObject();
                        mapObject.marker = "null";
                        mapObject.color = new ColorRGB(255, 145, 0, 255);
                        mapObject.colorFill = new ColorRGB(68, 132, 140, 128);
                        mapObject.weight = 5;

                        if (feature.getProperties() != null && feature.getProperties().getName() != null) {
                            mapObject.name = feature.getProperties().getName();
                        }
                        switch (feature.getGeometry().getType()) {
                            case "Point":
                                mapObject.type = ActivityGoogleMap.TYPE_POINT;
                                mapObject.marker = "a1";
                                break;
                            case "LineString":
                                mapObject.type = ActivityGoogleMap.TYPE_LINE;
                                break;
                            case "Polygon":
                                mapObject.type = ActivityGoogleMap.TYPE_POLYGON;
                                break;
                        }

                        if (mapObject.name == null) mapObject.name = mapObject.type;

                        if (feature.getGeometry().getCoordinates() != null) {
                            mapObject.latList = new ArrayList<>();
                            mapObject.lonList = new ArrayList<>();

                            for (ArrayList<Double> cord : feature.getGeometry().getCoordinates()) {
                                if (cord != null && cord.size() == 2) {
                                    mapObject.latList.add(cord.get(0));
                                    mapObject.lonList.add(cord.get(1));
                                }
                            }
                        }
                        tempMapObjects.add(mapObject);
                    }
                }
            }
        }

        return tempMapObjects;
    }

    public static GeoJson mapObjectsToGeoJson(ArrayList<MapObject> mapObjects) {
        GeoJson geoJson = new GeoJson();
        ArrayList<GeoJson.Feature> features = new ArrayList<>();
        for (int g = 0; g < mapObjects.size(); g++) {
            if (!mapObjects.get(g).type.equals(ActivityGoogleMap.TYPE_TEXT)) {
                GeoJson.Feature feature = new GeoJson.Feature();
                feature.setType("Feature");
                GeoJson.Geometry geometry = new GeoJson.Geometry();

                ArrayList<ArrayList<Double>> cordinates = new ArrayList<>();
                for (int l = 0; l < mapObjects.get(g).lonList.size(); l++) {
                    ArrayList<Double> cordObject = new ArrayList<>();
                    cordObject.add(mapObjects.get(g).lonList.get(l));
                    cordObject.add(mapObjects.get(g).latList.get(l));
                    cordinates.add(cordObject);
                }

                switch (mapObjects.get(g).type) {
                    case ActivityGoogleMap.TYPE_POINT:
                        geometry.setType("Point");
                        ArrayList<Double> cordObject = new ArrayList<>();
                        cordObject.add(mapObjects.get(g).lonList.get(0));
                        cordObject.add(mapObjects.get(g).latList.get(0));
                        geometry.setPointcoordinates(cordObject);
                        break;
                    case ActivityGoogleMap.TYPE_LINE:
                        geometry.setType("LineString");
                        geometry.setCoordinates(cordinates);
                        break;
                    case ActivityGoogleMap.TYPE_POLYGON:
                        geometry.setType("Polygon");
                        ArrayList<Double> cordLastObject = new ArrayList<>();
                        cordLastObject.add(mapObjects.get(g).lonList.get(0));
                        cordLastObject.add(mapObjects.get(g).latList.get(0));
                        cordinates.add(cordLastObject);
                        ArrayList<ArrayList<ArrayList<Double>>> polygonCord = new ArrayList<>();
                        polygonCord.add(cordinates);
                        geometry.setPolygoncoordinates(polygonCord);
                        break;
                }

                feature.setGeometry(geometry);
                GeoJson.Properties properties = new GeoJson.Properties();
                properties.setName(mapObjects.get(g).name);
                feature.setProperties(properties);
                features.add(feature);
            }
        }
        geoJson.setType("FeatureCollection");
        geoJson.setFeatures(features);
        if (features.isEmpty())
            return null;
        else
            return geoJson;
    }

    public static ArrayList<GraphicJson> getJsonFromString(String jsonString) {
        Gson gson = new Gson();
        GraphicJson[] json = gson.fromJson(jsonString, GraphicJson[].class);
        ArrayList<GraphicJson> jsonList = new ArrayList<>();

        for (int i = 0; i < json.length; i++) {
            if (json[i].getFeatureType() == null)
                json[i].setFeatureType("");
            if (json[i].getElementType() == null)
                json[i].setElementType("");
            jsonList.add(json[i]);
        }

        return jsonList;
    }

    public static ArrayList<GraphicJson> getJsonFromRawResources(InputStream is) {

        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Gson gson = new Gson();
        GraphicJson[] json = gson.fromJson(writer.toString(), GraphicJson[].class);
        ArrayList<GraphicJson> jsonList = new ArrayList<>();

        for (int i = 0; i < json.length; i++) {
            if (json[i].getFeatureType() == null)
                json[i].setFeatureType("");
            if (json[i].getElementType() == null)
                json[i].setElementType("");
            jsonList.add(json[i]);
        }
        return jsonList;
    }
}
