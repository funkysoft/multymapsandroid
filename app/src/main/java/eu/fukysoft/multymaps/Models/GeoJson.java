package eu.fukysoft.multymaps.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Funky on 10/21/2017.
 */

public class GeoJson {
    public static class Geometry {
        @SerializedName("type")
        private String type;

        public String getType() {
            return this.type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @SerializedName("coordinates")
        private ArrayList<ArrayList<Double>> coordinates;

        public ArrayList<ArrayList<Double>> getCoordinates() {
            return this.coordinates;
        }

        public void setCoordinates(ArrayList<ArrayList<Double>> coordinates) {
            this.coordinates = coordinates;
        }

        @SerializedName("polygoncoordinates")
        private ArrayList<ArrayList<ArrayList<Double>>> polygoncoordinates;

        public ArrayList<ArrayList<ArrayList<Double>>> getPolygoncoordinates() {
            return this.polygoncoordinates;
        }

        public void setPolygoncoordinates(ArrayList<ArrayList<ArrayList<Double>>> polygoncoordinates) {
            this.polygoncoordinates = polygoncoordinates;
        }

        @SerializedName("pointcoordinates")
        private ArrayList<Double> pointcoordinates;

        public ArrayList<Double> getPointcoordinates() {
            return this.pointcoordinates;
        }

        public void setPointcoordinates(ArrayList<Double> pointcoordinates) {
            this.pointcoordinates = pointcoordinates;
        }
    }

    public static class Properties {
        @SerializedName("name")
        private String name;

        public String getName() {
            return this.name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public static class Feature {
        @SerializedName("type")
        private String type;

        public String getType() {
            return this.type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @SerializedName("geometry")
        private Geometry geometry;

        public Geometry getGeometry() {
            return this.geometry;
        }

        public void setGeometry(Geometry geometry) {
            this.geometry = geometry;
        }

        @SerializedName("properties")
        private Properties properties;

        public Properties getProperties() {
            return this.properties;
        }

        public void setProperties(Properties properties) {
            this.properties = properties;
        }
    }

    @SerializedName("features")
    private ArrayList<Feature> features;

    public ArrayList<Feature> getFeatures() {
        return this.features;
    }

    public void setFeatures(ArrayList<Feature> features) {
        this.features = features;
    }

    @SerializedName("type")
    private String type;

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }


}


