package eu.fukysoft.multymaps.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mazof on 29.1.2017.
 */



public class GraphicJson implements Serializable
{
    @SerializedName("featureType")
    private String featureType;

    public String getFeatureType() { return this.featureType; }

    public void setFeatureType(String featureType) { this.featureType = featureType; }
    @SerializedName("elementType")
    private String elementType;

    public String getElementType() { return this.elementType; }

    public void setElementType(String elementType) { this.elementType = elementType; }
    @SerializedName("stylers")
    private ArrayList<Styler> stylers;

    public ArrayList<Styler> getStylers() { return this.stylers; }

    public void setStylers(ArrayList<Styler> stylers) { this.stylers = stylers; }
}
