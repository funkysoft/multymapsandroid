package eu.fukysoft.multymaps.Models;

/**
 * Created by mazof on 30.1.2017.
 */

public class ItemListEdits {
    private String description;
    private String jSonValue;
    private int padding;

    public ItemListEdits(String description, int padding,String jSonValue) {
        this.padding = padding;
        this.jSonValue = jSonValue;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getjSonValue() {
        return jSonValue;
    }

    public int getPadding() {
        return padding;
    }
}
