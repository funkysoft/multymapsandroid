package eu.fukysoft.multymaps.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Majo on 24.10.2017.
 */

public class MapStudioJson implements Serializable {

    @SerializedName("styleoptions")
    private ArrayList<GraphicJson> styleoptions;
    @SerializedName("mapobjects")
    private ArrayList<MapObject> mapobjects;

    public ArrayList<GraphicJson> getStyleoptions() {
        return styleoptions;
    }

    public void setStyleoptions(ArrayList<GraphicJson> styleoptions) {
        this.styleoptions = styleoptions;
    }

    public ArrayList<MapObject> getMapobjects() {
        return mapobjects;
    }

    public void setMapobjects(ArrayList<MapObject> mapobjects) {
        this.mapobjects = mapobjects;
    }
}
