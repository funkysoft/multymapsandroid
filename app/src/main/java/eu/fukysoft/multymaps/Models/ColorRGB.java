package eu.fukysoft.multymaps.Models;

import android.graphics.Color;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Majo on 19.7.2017.
 */

public class ColorRGB implements Serializable{
    @SerializedName("r")
    public int r;
    @SerializedName("g")
    public int g;
    @SerializedName("b")
    public int b;
    @SerializedName("a")
    public int a;

    public ColorRGB(int r, int g, int b, int a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public int parseColor(){
        return Color.argb(a,r,g,b);
    }

    public void setFromInt(int color){
        r = Color.red(color);
        g = Color.green(color);
        b = Color.blue(color);
        a = Color.alpha(color);
    }
}
