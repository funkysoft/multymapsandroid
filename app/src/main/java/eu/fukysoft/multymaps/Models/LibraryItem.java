package eu.fukysoft.multymaps.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Funky on 10/18/2017.
 */

public class LibraryItem {
    @SerializedName("name")
    private String name;
    @SerializedName("json")
    private String json;
    @SerializedName("mapType")
    private String mapType;
    @SerializedName("isChecked")
    private boolean isChecked;

    public LibraryItem(String name, boolean isChecked) {
        this.name = name;
        this.isChecked = isChecked;
    }

    public String getName() {
        return name;
    }

    public String getMapType() {
        return mapType;
    }

    public void setMapType(String mapType) {
        this.mapType = mapType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
