package eu.fukysoft.multymaps.Models;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Majo on 18.7.2017.
 */

public class MapObject implements Serializable{
    @SerializedName("latList")
    public ArrayList<Double> latList;
    @SerializedName("lonList")
    public ArrayList<Double> lonList;
    @SerializedName("color")
    public ColorRGB color;
    @SerializedName("colorFill")
    public ColorRGB colorFill;
    @SerializedName("weight")
    public int weight;
    @SerializedName("type")
    public String type;
    @SerializedName("marker")
    public String marker;
    @SerializedName("note")
    public String note;
    @SerializedName("name")
    public String name;


    public MapObject(ArrayList<Double> latList,ArrayList<Double> lonList, ColorRGB color, ColorRGB colorFill, int weight,String marker, String type) {
        this.latList = latList;
        this.lonList = lonList;
        this.color = color;
        this.colorFill = colorFill;
        this.weight = weight;
        this.type = type;
        this.marker = marker;
    }
    public MapObject(){

    }
}
