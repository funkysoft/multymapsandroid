package eu.fukysoft.multymaps.Models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by mazof on 29.1.2017.
 */

public class Styler implements Serializable
{
    @SerializedName("color")
    private String color;

    public String getColor() { return this.color; }

    public void setColor(String color) { this.color = color; }

    @SerializedName("hue")
    private String hue;

    public String getHue() { return this.hue; }

    public void setHue(String hue) { this.hue = hue; }

    @SerializedName("visibility")
    private String visibility;

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    @SerializedName("saturation")
    private Integer saturation;

    public Integer getSaturation() { return this.saturation; }

    public void setSaturation(Integer saturation) { this.saturation = saturation; }

    @SerializedName("lightness")
    private Integer lightness;

    public Integer getLightness() { return this.lightness; }

    public void setLightness(Integer lightness) { this.lightness = lightness; }

    @SerializedName("weight")
    private Double weight;

    public Double getWeight() { return this.weight; }

    public void setWeight(Double weight) { this.weight = weight; }

    @SerializedName("gamma")
    private Double gamma;

    public Double getGamma() { return this.gamma; }

    public void setGamma(Double gamma) { this.gamma = gamma; }

    @SerializedName("invert_lightness")
    private Boolean invert_lightness;

    public Boolean isInvert_lightness() {
        return invert_lightness;
    }

    public void setInvert_lightness(Boolean invert_lightness) {
        this.invert_lightness = invert_lightness;
    }
}