package eu.fukysoft.multymaps.Design;

import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.vision.text.Line;

import eu.fukysoft.multymaps.Activitys.ActivityGoogleMap;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Utils.Tools;

/**
 * Created by mazof on 30.1.2017.
 */

public class Menus {

    private Activity activity;
    private int screenHeight;
    private int screenWidth;
    private int statusBarHeight;
    private LinearLayout customLayout;
    private LinearLayout featureLayout;
    private LinearLayout elementLayout;
    private LinearLayout libraryLayout;
    private LinearLayout objectLayout;
    private LinearLayout rightDrawerEdits;
    private DrawerLayout drawerLayout;

    private LinearLayout topDrawerLayout;
    private Button buttonBack;
    private int numberOfOpenLayout = 0;

    public boolean isRightDrawerEditsOpen = false;
    public boolean isTopDrawerOpen = false;

    public int topBarSize;
    private int rightDrawerSize;

    private final int ANIM_DURATION = 500;


    public Menus(Activity activity) {

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        this.activity = activity;
        this.statusBarHeight = Tools.getStatusBarHeight(activity);
        this.screenHeight = metrics.heightPixels;
        this.screenWidth = metrics.widthPixels;


        this.topBarSize = activity.findViewById(R.id.layout_top_bar).getLayoutParams().height +
                activity.findViewById(R.id.view_top_bar_line).getLayoutParams().height;
        this.rightDrawerSize = activity.findViewById(R.id.layout_right_drawer_root).getLayoutParams().width;
    }

    public void createTopDrawer() {
        topDrawerLayout = (LinearLayout) activity.findViewById(R.id.top_drawer_layout);
        ViewGroup.LayoutParams params = topDrawerLayout.getLayoutParams();
        params.height = screenHeight - topBarSize;
        topDrawerLayout.setLayoutParams(params);
        topDrawerLayout.setY(-screenHeight);

        activity.findViewById(R.id.top_close_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topDrawerAction();
            }
        });

    }

    public void createRightDrawer() {
        rightDrawerEdits = (LinearLayout) activity.findViewById(R.id.right_drawer_layout_edit);
        ViewGroup.LayoutParams params = rightDrawerEdits.getLayoutParams();
        params.height = screenHeight - topBarSize ;
        rightDrawerEdits.setLayoutParams(params);
        rightDrawerEdits.setX(screenWidth);
        rightDrawerEdits.setY(topBarSize);


        elementLayout = (LinearLayout) activity.findViewById(R.id.element_layout);
        featureLayout = (LinearLayout) activity.findViewById(R.id.feature_layout);
        customLayout = (LinearLayout) activity.findViewById(R.id.custom_layout);
        libraryLayout = (LinearLayout) activity.findViewById(R.id.library_layout);
        objectLayout = (LinearLayout) activity.findViewById(R.id.object_layout);

        buttonBack = (Button) activity.findViewById(R.id.button_back);
        buttonBack.setVisibility(View.GONE);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityGoogleMap.isStylerOpen = false;
                switch (numberOfOpenLayout) {
                    case 1:
                        customLayout.animate().translationX(0);
                        buttonBack.setVisibility(View.GONE);
                        numberOfOpenLayout = 0;
                        break;
                    case 2:
                        featureLayout.animate().translationX(0);
                        numberOfOpenLayout = 1;
                        break;
                    case 3:
                        elementLayout.animate().translationX(0);
                        numberOfOpenLayout = 2;
                        break;
                }
            }
        });


        activity.findViewById(R.id.right_drawer_close_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rightDrawerAction();
                ActivityGoogleMap.isStylerOpen = false;
            }
        });

    }


    public void createLeftDrawer() {
        drawerLayout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
    }

    public void rightDrawerAction() {
        if (isRightDrawerEditsOpen) {
            isRightDrawerEditsOpen = false;
            rightDrawerEdits.animate().setDuration(ANIM_DURATION).translationX(screenWidth);
        } else {
            resetRightDrawer();
            isRightDrawerEditsOpen = true;
            rightDrawerEdits.animate().translationX(0);
            drawerLayout.closeDrawer(Gravity.START);

            isTopDrawerOpen = false;
            topDrawerLayout.animate().setDuration(ANIM_DURATION).translationY(-screenHeight);
        }
    }


    public void leftDrawerAction() {

        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            //isLeftDrawerGoogleopen = false;
            drawerLayout.closeDrawer(Gravity.START);
        } else {

            //isLeftDrawerGoogleopen = true;
            drawerLayout.openDrawer(Gravity.START);
            if (rightDrawerEdits != null) {
                isRightDrawerEditsOpen = false;
                rightDrawerEdits.animate().setDuration(ANIM_DURATION).translationX(screenWidth);
            }

            if (topDrawerLayout != null) {
                isTopDrawerOpen = false;
                topDrawerLayout.animate().setDuration(ANIM_DURATION).translationY(-screenHeight);
            }

        }
    }

    public boolean topDrawerAction() {

        if (isTopDrawerOpen) {
            isTopDrawerOpen = false;
            topDrawerLayout.animate().setDuration(ANIM_DURATION).translationY(-screenHeight);
        } else {

            isTopDrawerOpen = true;
            topDrawerLayout.animate().setDuration(ANIM_DURATION).translationY(topBarSize);
            //isLeftDrawerGoogleopen = false;
            drawerLayout.closeDrawer(Gravity.START);
            isRightDrawerEditsOpen = false;
            rightDrawerEdits.animate().setDuration(ANIM_DURATION).translationX(screenWidth);

        }
        return isTopDrawerOpen;
    }

    public void resetRightDrawer() {
        buttonBack.setVisibility(View.GONE);
        customLayout.animate().setDuration(ANIM_DURATION).translationX(0);
        featureLayout.animate().setDuration(ANIM_DURATION).translationX(0);
        elementLayout.animate().setDuration(ANIM_DURATION).translationX(0);

    }

    public void closeLibraryList() {
        libraryLayout.animate().translationX(this.rightDrawerSize);
    }

    public void closeObjectList() {
        objectLayout.animate().setDuration(ANIM_DURATION).translationX(this.rightDrawerSize);
    }

    public void resetLibraryList() {
        libraryLayout.setX(0);
    }

    public void resetObjectList() {
        objectLayout.setX(0);
    }

    public void closeCustomList() {
        customLayout.animate().setDuration(ANIM_DURATION).translationX(this.rightDrawerSize);
        buttonBack.setVisibility(View.VISIBLE);
        numberOfOpenLayout = 1;
    }

    public void closeFeatureList() {
        featureLayout.animate().setDuration(ANIM_DURATION).translationX(this.rightDrawerSize);
        numberOfOpenLayout = 2;
    }

    public void closeElementList() {
        elementLayout.animate().setDuration(ANIM_DURATION).translationX(this.rightDrawerSize);
        numberOfOpenLayout = 3;
    }
}
