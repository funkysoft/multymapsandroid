package eu.fukysoft.multymaps.Design;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.jrummyapps.android.colorpicker.ColorPickerDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import eu.fukysoft.multymaps.Activitys.ActivityGoogleMap;
import eu.fukysoft.multymaps.DrawUtils.MapWrapperLayout;
import eu.fukysoft.multymaps.DrawUtils.MySupportMapFragment;
import eu.fukysoft.multymaps.Models.GraphicJson;
import eu.fukysoft.multymaps.Models.Styler;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.StaticData.StaticData;

/**
 * Created by mazof on 7.2.2017.
 */

public class StylerMenu {
    private LinearLayout pickColorImage;
    private CheckBox checkBoxColor;
    private LinearLayout pickHueImage;
    private CheckBox checkBoxHue;
    private CheckBox checkBoxLightInvert;
    private ArrayList<GraphicJson> graphicJsons = new ArrayList<>();
    private GraphicJson graphicJson;
    private boolean exist;
    private RadioGroup radioGroup;
    private SeekBar seekBarWeight;
    private TextView textWeight;
    private SeekBar seekBarSaturation;
    private TextView textSaturation;
    private SeekBar seekBarLightness;
    private TextView textLightness;
    private SeekBar seekBarGamma;
    private TextView textGamma;
    private GoogleMap googleMap;

    public String jSonString = "";

    private int featurePos = -1;
    private int stylerPos = -1;

    private ActivityGoogleMap activity;

    public StylerMenu(final ActivityGoogleMap activity, GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.activity = activity;
        radioGroup = (RadioGroup) activity.findViewById(R.id.radio_gruop_visibility);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int position = 0;
                switch (checkedId) {
                    case R.id.radio_inherit:
                        position = 1;
                        break;
                    case R.id.radio_hidden:
                        position = 2;
                        break;
                    case R.id.radio_simplifield:
                        position = 3;
                        break;
                    case R.id.radio_shown:
                        position = 4;
                        break;
                }
                if (ActivityGoogleMap.isStylerOpen) {

                    if (graphicJson.getStylers().isEmpty()) {
                        Styler temp = new Styler();
                        temp.setVisibility(StaticData.visibility[position]);
                        graphicJson.getStylers().add(temp);
                    } else {
                        for (int x = 0; x < graphicJson.getStylers().size(); x++) {
                            if (graphicJson.getStylers().get(x).getVisibility() != null) {
                                if (position < 2) graphicJson.getStylers().remove(x);
                                else
                                    graphicJson.getStylers().get(x).setVisibility(StaticData.visibility[position]);

                                break;
                            }
                            if (x == graphicJson.getStylers().size() - 1) {
                                Styler temp = new Styler();
                                temp.setVisibility(StaticData.visibility[position]);
                                graphicJson.getStylers().add(temp);
                            }
                        }
                    }
                    if (!exist) {
                        exist = true;
                        graphicJsons.add(graphicJson);
                    }
                    putChangeToGoogleMap();
                }

            }
        });

        pickColorImage = (LinearLayout) activity.findViewById(R.id.color_pick_image);
        pickColorImage.setBackgroundColor(Color.RED);
        pickColorImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxColor.isChecked()) {
                    int color = Color.WHITE;
                    ColorPickerDialog.newBuilder().setDialogId(ActivityGoogleMap.COLOR_DIALOG_ID_MAPCUSTOM).setColor(color).show(activity);
                }
            }
        });

        checkBoxColor = (CheckBox) activity.findViewById(R.id.checkbox_color);
        checkBoxColor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ActivityGoogleMap.isStylerOpen) {

                    ColorDrawable viewColor = (ColorDrawable) pickColorImage.getBackground();
                    String colorHex = "#" + Integer.toHexString(viewColor.getColor()).substring(2);

                    if (!isChecked) {
                        for (int x = 0; x < graphicJson.getStylers().size(); x++) {
                            if (graphicJson.getStylers().get(x).getColor() != null) {
                                graphicJson.getStylers().remove(x);
                                putChangeToGoogleMap();
                                break;
                            }
                        }
                    } else {
                        if (exist) {

                            Styler temp = new Styler();
                            temp.setColor(colorHex);
                            setStylerIsExist(temp);
                        } else {
                            Styler temp = new Styler();
                            temp.setColor(colorHex);
                            setStylerNoExist(temp);
                        }
                    }
                }
            }
        });

        pickHueImage = (LinearLayout) activity.findViewById(R.id.hue_pick_image);
        pickHueImage.setBackgroundColor(Color.RED);
        pickHueImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxHue.isChecked()) {
                    int color = Color.WHITE;
                    ColorPickerDialog.newBuilder().setDialogId(ActivityGoogleMap.COLOR_DIALOG_ID_MAPCUSTOM_HUE).setColor(color).show(activity);
                }
            }
        });

        checkBoxHue = (CheckBox) activity.findViewById(R.id.checkbox_hue);
        checkBoxHue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ActivityGoogleMap.isStylerOpen) {

                    ColorDrawable viewColor = (ColorDrawable) pickHueImage.getBackground();
                    String colorHex = "#" + Integer.toHexString(viewColor.getColor()).substring(2);

                    if (!isChecked) {
                        for (int x = 0; x < graphicJson.getStylers().size(); x++) {
                            if (graphicJson.getStylers().get(x).getHue() != null) {
                                graphicJson.getStylers().remove(x);
                                putChangeToGoogleMap();
                                break;
                            }
                        }
                    } else {
                        if (exist) {

                            Styler temp = new Styler();
                            temp.setHue(colorHex);
                            setStylerIsExist(temp);
                        } else {
                            Styler temp = new Styler();
                            temp.setHue(colorHex);
                            setStylerNoExist(temp);
                        }
                    }
                }
            }
        });

        checkBoxLightInvert = (CheckBox) activity.findViewById(R.id.checkbox_lightinvert);
        checkBoxLightInvert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ActivityGoogleMap.isStylerOpen) {
                    if (!isChecked) {
                        for (int x = 0; x < graphicJson.getStylers().size(); x++) {
                            if (graphicJson.getStylers().get(x).isInvert_lightness() != null) {
                                graphicJson.getStylers().remove(x);
                                putChangeToGoogleMap();
                                break;
                            }
                        }
                    } else {
                        if (exist) {

                            Styler temp = new Styler();
                            temp.setInvert_lightness(true);
                            setStylerIsExist(temp);
                        } else {
                            Styler temp = new Styler();
                            temp.setInvert_lightness(true);
                            setStylerNoExist(temp);
                        }
                    }
                }
            }
        });

        textWeight = (TextView) activity.findViewById(R.id.text_weight_styler);
        seekBarWeight = (SeekBar) activity.findViewById(R.id.seek_bar_weight);
        seekBarWeight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double prog = progress;
                if (ActivityGoogleMap.isStylerOpen && fromUser) {
                    textWeight.setText(String.valueOf(progress)+".0");
                    if (exist) {
                        for (int x = 0; x < graphicJson.getStylers().size(); x++) {
                            if (graphicJson.getStylers().get(x).getWeight() != null) {
                                if (prog == 1.0) {
                                    graphicJson.getStylers().get(x).setWeight(null);
                                } else {
                                    graphicJson.getStylers().get(x).setWeight(prog);
                                }
                                putChangeToGoogleMap();
                                break;
                            }
                            if (x == graphicJson.getStylers().size() - 1) {
                                Styler temp = new Styler();
                                if (prog == 1.0) {
                                    temp.setWeight(null);
                                }else{
                                    temp.setWeight(prog);
                                }
                                setStylerIsExist(temp);
                               break;
                            }
                        }

                    } else {
                        Styler temp = new Styler();
                        if (prog == 1.0) {
                            temp.setWeight(null);
                        }else{
                            temp.setWeight(prog);
                        }
                        setStylerNoExist(temp);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        textGamma = (TextView) activity.findViewById(R.id.text_gamma);
        seekBarGamma = (SeekBar) activity.findViewById(R.id.seek_bar_gamma);
        seekBarGamma.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double prog = ((double) progress) / 10;
                if (ActivityGoogleMap.isStylerOpen) {
                    textGamma.setText("" + prog);
                    if (exist) {
                        for (int x = 0; x < graphicJson.getStylers().size(); x++) {
                            if (graphicJson.getStylers().get(x).getGamma() != null) {
                                graphicJson.getStylers().get(x).setGamma(prog);
                                putChangeToGoogleMap();
                                break;
                            }
                            if (x == graphicJson.getStylers().size() - 1) {
                                Styler temp = new Styler();
                                temp.setGamma(prog);
                                setStylerIsExist(temp);
                            }
                        }

                    } else {
                        Styler temp = new Styler();
                        temp.setGamma(prog);
                        setStylerNoExist(temp);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        textSaturation = (TextView) activity.findViewById(R.id.text_saturation);

        seekBarSaturation = (SeekBar) activity.findViewById(R.id.seek_bar_saturation);
        seekBarSaturation.setProgress(20);
        seekBarSaturation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int value = 0;
                if (progress == 20) value = 0;
                if (progress > 20) value = (progress - 20) * 5;
                if (progress < 20) value = -((20 - progress) * 5);
                if (ActivityGoogleMap.isStylerOpen) {
                    textSaturation.setText("" + value);
                    if (exist) {
                        for (int x = 0; x < graphicJson.getStylers().size(); x++) {
                            if (graphicJson.getStylers().get(x).getSaturation() != null) {
                                graphicJson.getStylers().get(x).setSaturation(value);
                                putChangeToGoogleMap();
                                break;
                            }
                            if (x == graphicJson.getStylers().size() - 1) {
                                Styler temp = new Styler();
                                temp.setSaturation(value);
                                setStylerIsExist(temp);

                            }
                        }

                    } else {
                        Styler temp = new Styler();
                        temp.setSaturation(value);
                        setStylerNoExist(temp);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        textLightness = (TextView) activity.findViewById(R.id.text_lightness);

        seekBarLightness = (SeekBar) activity.findViewById(R.id.seek_bar_lightness);
        seekBarLightness.setProgress(20);
        seekBarLightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int value = 0;
                if (progress == 20) value = 0;
                if (progress > 20) value = (progress - 20) * 5;
                if (progress < 20) value = -((20 - progress) * 5);
                if (ActivityGoogleMap.isStylerOpen) {
                    textLightness.setText("" + value);
                    if (exist) {
                        for (int x = 0; x < graphicJson.getStylers().size(); x++) {
                            if (graphicJson.getStylers().get(x).getLightness() != null) {
                                graphicJson.getStylers().get(x).setLightness(value);
                                putChangeToGoogleMap();
                                break;
                            }
                            if (x == graphicJson.getStylers().size() - 1) {
                                Styler temp = new Styler();
                                temp.setLightness(value);
                                setStylerIsExist(temp);
                            }
                        }

                    } else {
                        Styler temp = new Styler();
                        temp.setLightness(value);
                        setStylerNoExist(temp);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        activity.findViewById(R.id.layout_clear_stylers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                graphicJson = new GraphicJson();
                graphicJson.setStylers(new ArrayList<Styler>());

                for (int j = 0; j < graphicJsons.size(); j++) {
                    if (graphicJsons.get(j).getFeatureType().toLowerCase().equals(StaticData.listItemFeatureType.get(featurePos).getjSonValue().toLowerCase()))
                        if (graphicJsons.get(j).getElementType().toLowerCase().equals(StaticData.listItemElementType.get(stylerPos).getjSonValue().toLowerCase())) {
                            graphicJsons.remove(j);
                            break;
                        }
                }

                resetStyler();
                putChangeToGoogleMap();
            }
        });

    }

    public void init(int featurePosition, int elementPosition) {
        exist = false;

        featurePos = featurePosition;
        stylerPos = elementPosition;

        ActivityGoogleMap.isStylerOpen = false;
        for (int j = 0; j < graphicJsons.size(); j++) {
            if (graphicJsons.get(j).getFeatureType().toLowerCase().equals(StaticData.listItemFeatureType.get(featurePosition).getjSonValue().toLowerCase()))
                if (graphicJsons.get(j).getElementType().toLowerCase().equals(StaticData.listItemElementType.get(elementPosition).getjSonValue().toLowerCase())) {
                    graphicJson = graphicJsons.get(j);
                    exist = true;
                    break;
                }
        }

        if (!exist) {
            graphicJson = new GraphicJson();
            graphicJson.setStylers(new ArrayList<Styler>());
            graphicJson.setFeatureType(StaticData.listItemFeatureType.get(featurePosition).getjSonValue());
            graphicJson.setElementType(StaticData.listItemElementType.get(elementPosition).getjSonValue());
           resetStyler();

        } else {
            resetStyler();
            for (int x = 0; x < graphicJson.getStylers().size(); x++) {
                if (graphicJson.getStylers().get(x).getColor() != null) {
                    checkBoxColor.setChecked(true);
                    pickColorImage.setBackgroundColor(Color.parseColor(graphicJson.getStylers().get(x).getColor()));

                }
                if (graphicJson.getStylers().get(x).getHue() != null) {
                    checkBoxHue.setChecked(true);
                    pickHueImage.setBackgroundColor(Color.parseColor(graphicJson.getStylers().get(x).getHue()));

                }
                if (graphicJson.getStylers().get(x).getVisibility() != null) {
                    switch (graphicJson.getStylers().get(x).getVisibility()) {
                        case "":
                            radioGroup.check(R.id.radio_inherit);
                            break;
                        case "off":
                            radioGroup.check(R.id.radio_hidden);
                            break;
                        case "simplified":
                            radioGroup.check(R.id.radio_simplifield);
                            break;
                        case "on":
                            radioGroup.check(R.id.radio_shown);
                            break;
                    }

                }

                if (graphicJson.getStylers().get(x).isInvert_lightness() != null)
                    if (graphicJson.getStylers().get(x).isInvert_lightness()) {
                        checkBoxLightInvert.setChecked(true);
                    }

                if (graphicJson.getStylers().get(x).getWeight() != null)
                    if (graphicJson.getStylers().get(x).getWeight() != 0) {
                        seekBarWeight.setProgress(graphicJson.getStylers().get(x).getWeight().intValue());
                        textWeight.setText("" + (graphicJson.getStylers().get(x).getWeight()));
                    }

                if (graphicJson.getStylers().get(x).getGamma() != null)
                    if (graphicJson.getStylers().get(x).getGamma() != 0) {
                        int progressGamma = (int) (graphicJson.getStylers().get(x).getGamma() * 10);
                        seekBarGamma.setProgress(progressGamma);
                        textGamma.setText("" + (graphicJson.getStylers().get(x).getGamma()));
                    }

                if (graphicJson.getStylers().get(x).getSaturation() != null) {
                    int value = 20;
                    if (graphicJson.getStylers().get(x).getSaturation() > 0)
                        value = value + graphicJson.getStylers().get(x).getSaturation() / 5;
                    if (graphicJson.getStylers().get(x).getSaturation() < 0)
                        value = value + graphicJson.getStylers().get(x).getSaturation() / 5;
                    seekBarSaturation.setProgress(value);
                    textSaturation.setText("" + graphicJson.getStylers().get(x).getSaturation());
                }

                if (graphicJson.getStylers().get(x).getLightness() != null) {
                    int value = 20;
                    if (graphicJson.getStylers().get(x).getLightness() > 0)
                        value = value + graphicJson.getStylers().get(x).getLightness() / 5;
                    if (graphicJson.getStylers().get(x).getLightness() < 0)
                        value = value + graphicJson.getStylers().get(x).getLightness() / 5;
                    seekBarLightness.setProgress(value);
                    textLightness.setText("" + graphicJson.getStylers().get(x).getLightness());
                }

            }
        }
        ActivityGoogleMap.isStylerOpen = true;
    }


    public void onColorSelected(int color) {
        pickColorImage.setBackgroundColor(color);
        String colorHex = "#" + Integer.toHexString(color).substring(2);
        for (int s = 0; s < graphicJson.getStylers().size(); s++) {
            if (graphicJson.getStylers().get(s).getColor() != null) {
                graphicJson.getStylers().get(s).setColor(colorHex);
            }
        }
        putChangeToGoogleMap();
    }

    public void onHueSelected(int color) {
        pickHueImage.setBackgroundColor(color);
        String colorHex = "#" + Integer.toHexString(color).substring(2);
        for (int s = 0; s < graphicJson.getStylers().size(); s++) {
            if (graphicJson.getStylers().get(s).getHue() != null) {
                graphicJson.getStylers().get(s).setHue(colorHex);
            }
        }
        putChangeToGoogleMap();
    }

    public void putChangeToGoogleMap() {
        Gson gson = new Gson();
        String json = "";

        for (int i = 0; i < graphicJsons.size(); i++) {
            if (i == graphicJsons.size() - 1)
                json = json + gson.toJson(graphicJsons.get(i));
            else
                json = json + gson.toJson(graphicJsons.get(i)) + ",";
        }

        json = "[ " + json + " ]";

        this.jSonString = json;

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        MapStyleOptions mapStyleOptions = new MapStyleOptions(json);
        googleMap.setMapStyle(mapStyleOptions);
    }

    public void putJsonToGoogleMap(String json) {
        Gson gson = new Gson();
        graphicJsons = gson.fromJson(json, new TypeToken<List<GraphicJson>>() {
        }.getType());
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        MapStyleOptions mapStyleOptions = new MapStyleOptions(json);
        googleMap.setMapStyle(mapStyleOptions);
    }

    public void setGraphicJsons(ArrayList<GraphicJson> graphicJsonsList) {
            activity.mapType = "";
            this.graphicJsons = new ArrayList<>(graphicJsonsList);
            Gson gson = new Gson();

            String json = "";

            for (int i = 0; i < this.graphicJsons.size(); i++) {
                if (i == this.graphicJsons.size() - 1)
                    json = json + gson.toJson(this.graphicJsons.get(i));
                else
                    json = json + gson.toJson(this.graphicJsons.get(i)) + ",";
            }

            json = "[ " + json + " ]";

            this.jSonString = json;

    }

    public void resetGraphicJson(){
        this.graphicJsons = new ArrayList<>();
        this.jSonString = "";
    }

    public void setStylerIsExist(Styler temp) {
        graphicJson.getStylers().add(temp);
        putChangeToGoogleMap();
    }

    public void setStylerNoExist(Styler temp) {
        graphicJson.getStylers().add(temp);
        graphicJsons.add(graphicJson);
        exist = true;
        putChangeToGoogleMap();
    }

    private void resetStyler(){


        pickColorImage.setBackgroundColor(Color.RED);
        pickHueImage.setBackgroundColor(Color.RED);
        radioGroup.clearCheck();
        checkBoxColor.setChecked(false);
        checkBoxHue.setChecked(false);
        checkBoxLightInvert.setChecked(false);

        seekBarWeight.setProgress(0);
        seekBarGamma.setProgress(10);
        seekBarSaturation.setProgress(20);
        seekBarLightness.setProgress(20);

        textGamma.setText("1.0");
        textWeight.setText("0");
        textSaturation.setText("0");
        textLightness.setText("0");
    }


}
