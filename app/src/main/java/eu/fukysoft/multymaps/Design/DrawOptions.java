package eu.fukysoft.multymaps.Design;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import eu.fukysoft.multymaps.Activitys.ActivityGoogleMap;
import eu.fukysoft.multymaps.Activitys.ActivityObjectList;
import eu.fukysoft.multymaps.Adpaters.AdapterMenu;
import eu.fukysoft.multymaps.DrawUtils.MapWrapperLayout;
import eu.fukysoft.multymaps.DrawUtils.MySupportMapFragment;
import eu.fukysoft.multymaps.Models.ColorRGB;
import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.StaticData.StaticData;
import eu.fukysoft.multymaps.Utils.Dialogs;
import eu.fukysoft.multymaps.Utils.Tools;

/**
 * Created by Majo on 23.7.2017.
 */

public class DrawOptions {

    private final String BLOCK_OK = "BLOCK_OK";
    private final String BLOCK_ERASE = "BLOCK_ERASE";
    private final String BLOCK_BACK = "BLOCK_BACK";
    private final String BLOCK_STORKE = "BLOCK_STROKE";
    private final String BLOCK_FILL = "BLOCK_FILL";
    private final String BLOCK_WEIGHT = "BLOCK_WEIGHT";
    private final String BLOCK_MARKER = "BLOCK_MARKER";
    private final String BLOCK_INFO = "BLOCK_INFO";
    private final String BLOCK_CLOSE = "BLOCK_CLOSE";
    private final String BLOCK_TEXT = "BLOCK_TEXT";


    private String[] menuOptions = new String[]{"OBJECT LIST"};
    private String typeSet;
    private int strokeWidth;
    private Activity activity;

    private ArrayList<LatLng> latLngList = new ArrayList<>();
    public ArrayList<MapObject> mapObjectList = new ArrayList<>();

    private LinearLayout layoutDrawStyle;
    private TextView textViewWeight;
    public View colorFillView;
    public View colorStrokeView;
    private ImageView markerImage;

    public ColorRGB colorFill;
    public ColorRGB colorStroke;

    private MarkerOptions markerOptions;
    private GoogleMap googleMapView;
    private MaterialDialog infoDialog;
    private MaterialDialog weightDialog;
    private MaterialDialog markerDialog;
    private MaterialDialog saveDialog;
    private MaterialDialog textInputDialog;
    private String marker;
    private String note;

    private boolean isDrawText = true;
    private boolean isDrawPoint = true;
    private boolean isDrawPolyline = true;
    private boolean isDrawPolygon = true;
    private boolean isDrawing = true;

    private Menus menus;
    private Polygon polygon;

    private HashMap<String, LinearLayout> drawBlocks = new HashMap<>();
    private MySupportMapFragment mapFragment;
    private ArrayList<LatLng> drawedLats = new ArrayList<>();

    public DrawOptions(final Activity activity, final GoogleMap googleMapView, final Menus menu, MySupportMapFragment mapFragment) {
        this.googleMapView = googleMapView;
        this.activity = activity;
        this.mapFragment = mapFragment;
        this.menus = menu;

        strokeWidth = 5;
        colorFill = new ColorRGB(68, 132, 140, 128);
        colorStroke = new ColorRGB(255, 145, 0, 255);
        marker = "a1";
        markerOptions = new MarkerOptions();

        googleMapView.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        Bitmap bm = BitmapFactory.decodeResource(activity.getResources(), R.drawable.marker_a1);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bm));

        layoutDrawStyle = (LinearLayout) activity.findViewById(R.id.layout_draw);
        colorFillView = activity.findViewById(R.id.color_view_fill);
        colorFillView.setBackgroundColor(colorFill.parseColor());

        colorStrokeView = activity.findViewById(R.id.color_view_stroke);
        colorStrokeView.setBackgroundColor(colorStroke.parseColor());

        textViewWeight = (TextView) activity.findViewById(R.id.text_weight);
        textViewWeight.setText("" + strokeWidth);
        markerImage = (ImageView) activity.findViewById(R.id.marker_imageview);


        ListView rightDrawerOptionsListview = (ListView) activity.findViewById(R.id.right_drawer_listview_google_objects_options);
        AdapterMenu rightDrawerOptionsAdapter = new AdapterMenu(activity, menuOptions, StaticData.drawablesDrawOptions);
        rightDrawerOptionsListview.setAdapter(rightDrawerOptionsAdapter);
        rightDrawerOptionsListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        Intent intent = new Intent(activity, ActivityObjectList.class);
                        intent.putExtra(ActivityObjectList.KEY_OBJECT_LIST, mapObjectList);
                        activity.startActivityForResult(intent, ActivityGoogleMap.OBJECT_LIST_REQUEST_CODE);
                        break;
                }
            }
        });

        NonScrollListView rightDrawerListview = activity.findViewById(R.id.right_drawer_listview_google_objects);
        AdapterMenu rightDrawerAdapter = new AdapterMenu(activity, activity.getResources().getStringArray(R.array.menu_google_map_object_draw), StaticData.drawablesDraw);
        rightDrawerListview.setAdapter(rightDrawerAdapter);
        rightDrawerListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                menus.rightDrawerAction();
                layoutDrawStyle.setVisibility(View.VISIBLE);
                switch (position) {
                    case 0:
                        prepareDrawText();
                        break;
                    case 1:
                        prepareDrawPoint();
                        break;
                    case 2:
                        preparePinPolyline();
                        break;
                    case 3:
                        prepareDrawPolyline();
                        break;
                    case 4:
                        preparePinPolygon();
                        break;
                    case 5:
                        prepareDrawPolygon();
                        break;
                }

            }
        });


        drawBlocks.put(BLOCK_OK, (LinearLayout) activity.findViewById(R.id.ok_block));
        drawBlocks.get(BLOCK_OK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameText;
                if (typeSet.equals(ActivityGoogleMap.TYPE_TEXT)) {
                    nameText = note;
                } else {
                    nameText = typeSet;
                }
                saveDialog = Dialogs.saveDialog(activity, nameText, new Dialogs.DialogSave() {
                    @Override
                    public void onSave(String text) {
                        if (!latLngList.isEmpty()) {
                            ColorRGB stroke = new ColorRGB(colorStroke.r, colorStroke.g, colorStroke.b, colorStroke.a);
                            ColorRGB fill = new ColorRGB(colorFill.r, colorFill.g, colorFill.b, colorFill.a);

                            MapObject mapObject = new MapObject(Tools.getLat(latLngList), Tools.getLong(latLngList), stroke, fill, strokeWidth, marker, typeSet);
                            mapObject.note = note;
                            if (text.equals("")) {
                                mapObject.name = typeSet;
                            } else {
                                mapObject.name = text;
                            }
                            mapObjectList.add(mapObject);
                            latLngList = new ArrayList<>();

                        }
                        saveDialog.dismiss();
                    }
                });
                saveDialog.show();


            }
        });

        drawBlocks.put(BLOCK_ERASE, (LinearLayout) activity.findViewById(R.id.erase_block));
        drawBlocks.get(BLOCK_ERASE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                latLngList.clear();
                redrawObjects();
            }
        });

        drawBlocks.put(BLOCK_BACK, (LinearLayout) activity.findViewById(R.id.back_block));
        drawBlocks.get(BLOCK_BACK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!latLngList.isEmpty()) {
                    latLngList.remove(latLngList.size() - 1);
                    redrawObjects();
                    drawObjectToMap();
                }
            }
        });

        drawBlocks.put(BLOCK_TEXT, (LinearLayout) activity.findViewById(R.id.text_block));
        drawBlocks.get(BLOCK_TEXT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textInputDialog = Dialogs.textInputDialog(activity, note, new Dialogs.DialogDimmis() {
                    @Override
                    public void onDimmis() {
                        textInputDialog.dismiss();
                    }
                }, new Dialogs.MarkerNoteChaneg() {
                    @Override
                    public void onChange(String noteText) {
                        note = noteText;
                        drawObjectToMap();
                    }
                });
                textInputDialog.show();
            }
        });

        drawBlocks.put(BLOCK_FILL, (LinearLayout) activity.findViewById(R.id.fill_block));
        drawBlocks.get(BLOCK_FILL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialogs.colorDialog(ActivityGoogleMap.COLOR_DIALOG_ID_POLYGON_FILL,
                        colorFill.parseColor()).show(activity);
            }
        });

        drawBlocks.put(BLOCK_STORKE, (LinearLayout) activity.findViewById(R.id.color_block));
        drawBlocks.get(BLOCK_STORKE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialogs.colorDialog(ActivityGoogleMap.COLOR_DIALOG_ID_POLYGON_STROKE,
                        colorStroke.parseColor()).show(activity);
            }
        });

        drawBlocks.put(BLOCK_WEIGHT, (LinearLayout) activity.findViewById(R.id.weight_block));
        drawBlocks.get(BLOCK_WEIGHT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weightDialog = Dialogs.weightDialog(activity, strokeWidth, new Dialogs.DialogClose() {
                    @Override
                    public void onDimmis() {
                        weightDialog.dismiss();
                    }

                    @Override
                    public void onSubmit(int item) {
                        weightDialog.dismiss();
                        textViewWeight.setText("" + item);
                        strokeWidth = item;
                        drawObjectToMap();
                    }
                });
                weightDialog.show();
            }
        });

        drawBlocks.put(BLOCK_MARKER, (LinearLayout) activity.findViewById(R.id.marker_block));
        drawBlocks.get(BLOCK_MARKER).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markerDialog = Dialogs.markerDialog(activity, new Dialogs.DialogClose() {
                    @Override
                    public void onDimmis() {
                        markerDialog.dismiss();
                    }

                    @Override
                    public void onSubmit(int item) {
                        markerDialog.dismiss();

                        drawObjectToMap();
                    }
                });
                markerDialog.show();
            }
        });

        drawBlocks.put(BLOCK_INFO, (LinearLayout) activity.findViewById(R.id.info_block));
        drawBlocks.get(BLOCK_INFO).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (typeSet) {
                    case ActivityGoogleMap.TYPE_POINT:
                        infoDialog = Dialogs.markerInfoDialog(activity, latLngList, note, new Dialogs.DialogDimmis() {
                            @Override
                            public void onDimmis() {
                                infoDialog.dismiss();
                            }
                        }, new Dialogs.MarkerNoteChaneg() {
                            @Override
                            public void onChange(String noteChange) {
                                note = noteChange;
                            }
                        });
                        infoDialog.show();
                        break;

                    case ActivityGoogleMap.TYPE_POLYGON:
                        infoDialog = Dialogs.infoDialog(activity, latLngList, new Dialogs.DialogDimmis() {
                            @Override
                            public void onDimmis() {
                                infoDialog.dismiss();
                            }
                        });
                        infoDialog.show();
                        break;
                    case ActivityGoogleMap.TYPE_LINE:
                        infoDialog = Dialogs.infoDialog(activity, latLngList, new Dialogs.DialogDimmis() {
                            @Override
                            public void onDimmis() {
                                infoDialog.dismiss();
                            }
                        });
                        infoDialog.show();
                        break;


                }
            }
        });

        drawBlocks.put(BLOCK_CLOSE, (LinearLayout) activity.findViewById(R.id.close_block));
        drawBlocks.get(BLOCK_CLOSE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                latLngList.clear();
                redrawObjects();
                layoutDrawStyle.setVisibility(View.GONE);
                googleMapView.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                    }
                });
                disableDrawing();

            }
        });

        CheckBox checkBoxText = (CheckBox) activity.findViewById(R.id.checkbox_texts_visibility);
        CheckBox checkBoxPoints = (CheckBox) activity.findViewById(R.id.checkbox_markers_visibility);
        CheckBox checkBoxPolylines = (CheckBox) activity.findViewById(R.id.checkbox_polylines_visibility);
        CheckBox checkBoxPolygons = (CheckBox) activity.findViewById(R.id.checkbox_polygons_visibility);

        checkBoxText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isDrawText = isChecked;
                redrawObjects();
            }
        });

        checkBoxPoints.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isDrawPoint = isChecked;
                redrawObjects();
            }
        });

        checkBoxPolylines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isDrawPolyline = isChecked;
                redrawObjects();
            }
        });

        checkBoxPolygons.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isDrawPolygon = isChecked;
                redrawObjects();
            }
        });

        googleMapView.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                isDrawing = false;
            }
        });
        googleMapView.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                isDrawing = true;
                drawObjectToMap();
            }
        });
    }

    public void drawObjectEnd() {
        layoutDrawStyle.setVisibility(View.GONE);
        redrawObjects();
        latLngList.clear();
        disableDrawing();
        googleMapView.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
            }
        });
    }

    public void drawObjectToMap() {
        if (!latLngList.isEmpty()) {
            redrawObjects();
            switch (typeSet) {

                case ActivityGoogleMap.TYPE_TEXT:
                    MapObject object = new MapObject(Tools.getLat(latLngList), Tools.getLong(latLngList), colorStroke, colorFill, strokeWidth, marker, typeSet);
                    object.note = note;
                    drawText(object);
                    break;

                case ActivityGoogleMap.TYPE_POINT:
                    drawPoint(new MapObject(Tools.getLat(latLngList), Tools.getLong(latLngList), null, null, 0, marker, typeSet));
                    break;

                case ActivityGoogleMap.TYPE_LINE:
                    drawPolyline(new MapObject(Tools.getLat(latLngList), Tools.getLong(latLngList), colorStroke, null, strokeWidth, marker, typeSet));
                    break;

                case ActivityGoogleMap.TYPE_POLYGON:
                    drawPolygon(new MapObject(Tools.getLat(latLngList), Tools.getLong(latLngList), colorStroke, colorFill, strokeWidth, marker, typeSet));
                    break;

            }

        }
    }

    public void redrawObjects() {
        googleMapView.clear();
        for (int o = 0; o < mapObjectList.size(); o++) {
            switch (mapObjectList.get(o).type) {
                case ActivityGoogleMap.TYPE_TEXT:
                    if (isDrawText)
                        drawText(mapObjectList.get(o));
                    break;
                case ActivityGoogleMap.TYPE_POINT:
                    if (isDrawPoint)
                        drawPoint(mapObjectList.get(o));
                    break;
                case ActivityGoogleMap.TYPE_LINE:
                    if (isDrawPolyline)
                        drawPolyline(mapObjectList.get(o));
                    break;
                case ActivityGoogleMap.TYPE_POLYGON:
                    if (isDrawPolygon)
                        drawPolygon(mapObjectList.get(o));
                    break;
            }
        }

    }

    private void drawText(MapObject object) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap((getTextBitMap(object))));
        markerOptions.position(Tools.getLatLongList(object.latList, object.lonList).get(0));
        googleMapView.addMarker(markerOptions);
    }

    private void drawPoint(MapObject object) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(((BitmapDrawable) StaticData.markers.get(object.marker)).getBitmap()));

        markerOptions.position(Tools.getLatLongList(object.latList, object.lonList).get(0));


        googleMapView.addMarker(markerOptions);
    }

    private void drawPolyline(MapObject object) {
        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.color(object.color.parseColor());
        lineOptions.width(object.weight);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(((BitmapDrawable) StaticData.markers.get(object.marker)).getBitmap()));
        for (int d = 0; d < Tools.getLatLongList(object.latList, object.lonList).size(); d++) {
            lineOptions.add(Tools.getLatLongList(object.latList, object.lonList).get(d));
            if (!object.marker.equals("null"))
                googleMapView.addMarker(markerOptions.position(Tools.getLatLongList(object.latList, object.lonList).get(d)).draggable(false));
        }
        googleMapView.addPolyline(lineOptions);
    }


    private void drawPolygon(MapObject object) {
        PolygonOptions rectOptions = new PolygonOptions();
        rectOptions.fillColor(object.colorFill.parseColor());
        rectOptions.strokeColor(object.color.parseColor());
        rectOptions.strokeWidth(object.weight);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(((BitmapDrawable) StaticData.markers.get(object.marker)).getBitmap()));
        for (int l = 0; l < Tools.getLatLongList(object.latList, object.lonList).size(); l++) {
            rectOptions.add(Tools.getLatLongList(object.latList, object.lonList).get(l));
            if (!object.marker.equals("null")) {
                googleMapView.addMarker(markerOptions
                        .position(Tools.getLatLongList(object.latList, object.lonList).get(l))
                        .draggable(false));
            }
        }
        googleMapView.addPolygon(rectOptions);
    }

    public void editObject(int positionOfObject) {
        MapObject editObject = mapObjectList.get(positionOfObject);
        mapObjectList.remove(positionOfObject);

        latLngList = Tools.getLatLongList(editObject.latList, editObject.lonList);
        colorStroke = editObject.color;
        colorFill = editObject.colorFill;
        strokeWidth = editObject.weight;
        marker = editObject.marker;
        typeSet = editObject.type;
        note = editObject.note;

        colorFillView.setBackgroundColor(colorFill.parseColor());
        colorStrokeView.setBackgroundColor(colorStroke.parseColor());
        textViewWeight.setText("" + strokeWidth);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(((BitmapDrawable) StaticData.markers.get(marker)).getBitmap()));
        markerImage.setImageDrawable(StaticData.markers.get(marker));

        switch (typeSet) {
            case ActivityGoogleMap.TYPE_TEXT:
                prepareDrawText();
                break;
            case ActivityGoogleMap.TYPE_POINT:
                prepareDrawPoint();
                break;
            case ActivityGoogleMap.TYPE_LINE:
                preparePinPolyline();
                break;
            case ActivityGoogleMap.TYPE_POLYGON:
                preparePinPolygon();
                break;
        }
        if(menus.isRightDrawerEditsOpen)menus.rightDrawerAction();
        layoutDrawStyle.setVisibility(View.VISIBLE);


    }

    public void setMarker(String tag) {
        if (tag.equals("null")) {
            marker = "null";
        } else {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(((BitmapDrawable) StaticData.markers.get(tag)).getBitmap()));
            marker = tag;
        }
        markerImage.setImageDrawable(StaticData.markers.get(tag));
        drawObjectToMap();
        markerDialog.dismiss();
    }

    public void setObjectsFromJson(String json) {
        Gson gson = new Gson();
        mapObjectList = gson.fromJson(json, new TypeToken<ArrayList<MapObject>>() {
        }.getType());
        if (mapObjectList == null) mapObjectList = new ArrayList<>();
        redrawObjects();
    }

    public Bitmap getTextBitMap(MapObject object) {

        final TextView textView = new TextView(activity);
        textView.setText(object.note);
        textView.setTextSize(object.weight);

        final Paint paintText = textView.getPaint();
        final Paint strokeText = new Paint();

        final Rect boundsText = new Rect();
        paintText.getTextBounds(object.note, 0, textView.length(), boundsText);
        paintText.setTextAlign(Paint.Align.CENTER);

        int lineCount = getLineCount(object.note);

        final Bitmap bmpText = Bitmap.createBitmap(boundsText.width() + 2, (boundsText.height() + 2) * lineCount, Bitmap.Config.ARGB_8888);

        final Canvas canvasText = new Canvas(bmpText);

        paintText.setColor(object.colorFill.parseColor());
        strokeText.setStyle(Paint.Style.STROKE);
        strokeText.setStrokeWidth(3);
        strokeText.setColor(object.color.parseColor());
        strokeText.setTextSize(textView.getTextSize());
        strokeText.setTextAlign(Paint.Align.CENTER);
        strokeText.setShadowLayer(7, 0, 0, object.color.parseColor());

        int y = (canvasText.getHeight() - boundsText.bottom) / lineCount;
        for (String line : object.note.split("\n")) {
            canvasText.drawText(line, canvasText.getWidth() / 2, y, strokeText);
            canvasText.drawText(line, canvasText.getWidth() / 2, y, paintText);
            y += (canvasText.getHeight() - boundsText.bottom) / lineCount;
        }


        return bmpText;
    }

    private void prepareDrawText() {
        typeSet = ActivityGoogleMap.TYPE_TEXT;
        note = "Text";
        strokeWidth = 10;
        colorFill = new ColorRGB(255, 145, 0, 255);
        colorStroke = new ColorRGB(68, 132, 140, 255);

        textViewWeight.setText("" + strokeWidth);

        colorFillView.setBackgroundColor(colorFill.parseColor());
        colorStrokeView.setBackgroundColor(colorStroke.parseColor());

        drawBlocks.get(BLOCK_INFO).setVisibility(View.GONE);
        drawBlocks.get(BLOCK_MARKER).setVisibility(View.GONE);
        drawBlocks.get(BLOCK_BACK).setVisibility(View.GONE);
        drawBlocks.get(BLOCK_FILL).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_STORKE).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_TEXT).setVisibility(View.VISIBLE);
        googleMapView.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                latLngList.clear();
                latLngList.add(latLng);
                drawObjectToMap();
            }
        });
    }

    private void prepareDrawPoint() {
        typeSet = ActivityGoogleMap.TYPE_POINT;
        note = "";
        drawBlocks.get(BLOCK_FILL).setVisibility(View.GONE);
        drawBlocks.get(BLOCK_STORKE).setVisibility(View.GONE);
        drawBlocks.get(BLOCK_WEIGHT).setVisibility(View.GONE);
        drawBlocks.get(BLOCK_BACK).setVisibility(View.GONE);
        drawBlocks.get(BLOCK_MARKER).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_INFO).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_TEXT).setVisibility(View.GONE);
        googleMapView.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                latLngList.clear();
                latLngList.add(latLng);
                drawObjectToMap();
            }
        });
    }

    private void preparePinPolyline() {
        typeSet = ActivityGoogleMap.TYPE_LINE;
        drawBlocks.get(BLOCK_FILL).setVisibility(View.GONE);
        drawBlocks.get(BLOCK_BACK).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_STORKE).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_WEIGHT).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_MARKER).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_INFO).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_TEXT).setVisibility(View.GONE);
        googleMapView.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                latLngList.add(latLng);
                drawObjectToMap();
            }
        });
    }

    private void prepareDrawPolyline() {
        typeSet = ActivityGoogleMap.TYPE_LINE;
        drawBlocks.get(BLOCK_FILL).setVisibility(View.GONE);
        drawBlocks.get(BLOCK_BACK).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_STORKE).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_WEIGHT).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_MARKER).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_INFO).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_TEXT).setVisibility(View.GONE);
        setDrawIng();
    }


    private void preparePinPolygon() {
        typeSet = ActivityGoogleMap.TYPE_POLYGON;
        drawBlocks.get(BLOCK_FILL).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_BACK).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_STORKE).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_WEIGHT).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_MARKER).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_INFO).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_TEXT).setVisibility(View.GONE);
        googleMapView.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                latLngList.add(latLng);
                drawObjectToMap();
            }
        });
    }

    private void prepareDrawPolygon() {
        typeSet = ActivityGoogleMap.TYPE_POLYGON;
        drawBlocks.get(BLOCK_FILL).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_BACK).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_STORKE).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_WEIGHT).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_MARKER).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_INFO).setVisibility(View.VISIBLE);
        drawBlocks.get(BLOCK_TEXT).setVisibility(View.GONE);
        setDrawIng();
    }

    private int getLineCount(String text) {
        int count = 1;
        for (String line : text.split("\n")) {
            count++;
        }
        return count;
    }

    private void setDrawIng() {
        mapFragment.setOnDragListener(new MapWrapperLayout.OnDragListener() {
            @Override
            public void onDrag(MotionEvent motionEvent) {
                if (isDrawing) {
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    int x_co = Integer.parseInt(String.valueOf(Math.round(x)));
                    int y_co = Integer.parseInt(String.valueOf(Math.round(y)));
                    Point x_y_points = new Point(x_co, y_co);
                    LatLng latLng = googleMapView.getProjection().fromScreenLocation(x_y_points);

                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            latLngList.clear();
                            googleMapView.getUiSettings().setScrollGesturesEnabled(false);

                            break;
                        case MotionEvent.ACTION_UP:
                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            googleMapView.getUiSettings().setScrollGesturesEnabled(true);
                                            drawObjectToMap();
                                        }
                                    });
                                }
                            }, 200);
                           //
                            break;
                    }
                    googleMapView.clear();
                    latLngList.add(latLng);

                    switch (typeSet) {
                        case ActivityGoogleMap.TYPE_POLYGON:
                            drawPolygon(new MapObject(Tools.getLat(latLngList), Tools.getLong(latLngList), colorStroke, colorFill, strokeWidth, marker, typeSet));
                            break;
                        case ActivityGoogleMap.TYPE_LINE:
                            drawPolyline(new MapObject(Tools.getLat(latLngList), Tools.getLong(latLngList), colorStroke, null, strokeWidth, marker, typeSet));
                            break;

                    }
                }
            }
        });
    }


    private void disableDrawing() {
        mapFragment.setOnDragListener(null);
        googleMapView.getUiSettings().setScrollGesturesEnabled(true);
    }

}
