package eu.fukysoft.multymaps.Design;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import eu.fukysoft.multymaps.Api.ApiClient;
import eu.fukysoft.multymaps.Api.MapApi;
import eu.fukysoft.multymaps.Api.Models.Elevation;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Utils.Tools;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mazof on 9.2.2017.
 */

public class SearchBarGoogle {

    private Activity activity;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private EditText editTextAdress;
    private EditText editTextCity;
    private EditText editTextElevation;
    private EditText editTextState;
    private EditText editTextLat;
    private EditText editTextLon;
    private ImageView imageViewSearchCord;
    private GoogleMap googleMapView;

    public SearchBarGoogle(final Activity activity, final Menus menus) {
        this.activity = activity;
        editTextAdress = (EditText) activity.findViewById(R.id.edittext_adress);
        editTextState = (EditText) activity.findViewById(R.id.edittext_state);
        editTextCity = (EditText) activity.findViewById(R.id.edittext_city);
        editTextElevation = (EditText) activity.findViewById(R.id.edittext_elevation);
        editTextLat = (EditText) activity.findViewById(R.id.edittetx_lat);
        editTextLon = (EditText) activity.findViewById(R.id.edittetx_lon);

        imageViewSearchCord = (ImageView) activity.findViewById(R.id.button_search_cordination);

        imageViewSearchCord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double latitude;
                double longitude;

                latitude = Double.parseDouble(editTextLat.getText().toString());
                longitude = Double.parseDouble(editTextLon.getText().toString());
                LatLng myLocation = new LatLng(latitude, longitude);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(myLocation)      // Sets the center of the map to Mountain View
                        .zoom(17)                   // Sets the zoom
                        .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                googleMapView.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                menus.topDrawerAction();
            }
        });

        editTextAdress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    try {
                        Intent intent =
                                new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                        .build(activity);
                        activity.startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                    } catch (GooglePlayServicesRepairableException e) {
                        // TODO: Handle the error.
                    } catch (GooglePlayServicesNotAvailableException e) {
                        // TODO: Handle the error.
                    }
                }
            }
        });


    }

    public void setAtributesFromMap(double lat, double lon, List<Address> addresses) {
        clearText();
        String unknown = activity.getString(R.string.unknown);

        if (!addresses.isEmpty()) {
            editTextAdress.setText(addresses.get(0).getAddressLine(0));


            if (addresses.get(0).getLocality() == null) {
                if (addresses.get(0).getSubAdminArea() == null) {
                    editTextCity.setText(unknown);
                } else {
                    editTextCity.setText(addresses.get(0).getSubAdminArea());
                }

            } else {
                editTextCity.setText(addresses.get(0).getLocality());
            }
            editTextState.setText(addresses.get(0).getCountryName());

        } else {
            editTextAdress.setText(unknown);
            editTextCity.setText(unknown);
            editTextState.setText(unknown);
        }
        editTextLat.setText("" + lat);
        editTextLon.setText("" + lon);

        ApiClient.getClient(MapApi.GOOGLE_URL).create(MapApi.class).getElevation((lat + "," + lon), true).enqueue(new Callback<Elevation>() {
            @Override
            public void onResponse(Call<Elevation> call, Response<Elevation> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null &&
                            response.body().getResults() != null &&
                            !response.body().getResults().isEmpty())
                        editTextElevation.setText(Tools.toUnitsElevation(response.body().getResults().get(0).getElevation(), activity) + "");
                }
            }

            @Override
            public void onFailure(Call<Elevation> call, Throwable t) {

            }
        });
    }

    public void setMap(GoogleMap googleMapView) {
        this.googleMapView = googleMapView;
    }

    public void clearText() {
        editTextAdress.setText("");
        editTextState.setText("");
        editTextCity.setText("");
        editTextLat.setText("");
        editTextLon.setText("");
        editTextElevation.setText("");
    }
}
