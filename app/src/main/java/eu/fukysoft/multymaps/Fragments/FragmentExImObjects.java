package eu.fukysoft.multymaps.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import eu.fukysoft.multymaps.Activitys.ActivityExport;
import eu.fukysoft.multymaps.Activitys.ActivityGoogleMap;
import eu.fukysoft.multymaps.Adpaters.AdapterSpinnerExtension;
import eu.fukysoft.multymaps.Models.GeoJson;
import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Interfaces.ChangeJsonState;
import eu.fukysoft.multymaps.Utils.Tools;

/**
 * Created by Funky on 10/20/2017.
 */

public class FragmentExImObjects extends Fragment {

    private TextView textViewJson;
    private TextView textViewNoObjects;
    private ScrollView scrollViewJson;

    private CheckBox checkBoxText;
    private CheckBox checkBoxPoint;
    private CheckBox checkBoxPolyline;
    private CheckBox checkBoxPolygon;

    private ArrayList<MapObject> mapObjects;

    private boolean isGeoJson;
    private boolean isJsonVisible;
    private String jsnoStyle;
    private int extension = 0;

    private ChangeJsonState listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ChangeJsonState) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_imex_objects, container, false);

        textViewJson = (TextView) rootView.findViewById(R.id.textview_json);
        textViewNoObjects = (TextView) rootView.findViewById(R.id.textview_noobjects);
        scrollViewJson = (ScrollView) rootView.findViewById(R.id.scroll_view_json);

        checkBoxText = (CheckBox) rootView.findViewById(R.id.checkbox_texts_visibility);
        checkBoxPoint = (CheckBox) rootView.findViewById(R.id.checkbox_markers_visibility);
        checkBoxPolyline = (CheckBox) rootView.findViewById(R.id.checkbox_polylines_visibility);
        checkBoxPolygon = (CheckBox) rootView.findViewById(R.id.checkbox_polygons_visibility);

        LinearLayout layoutTextVisibility = (LinearLayout) rootView.findViewById(R.id.layout_text_visibility);
        LinearLayout layoutGeoJsonSettings = (LinearLayout) rootView.findViewById(R.id.layout_geo_json_setting);

        mapObjects = (ArrayList<MapObject>) getArguments().getSerializable(ActivityExport.KEY_OBJECT_LIST);
        isGeoJson = getArguments().getBoolean(ActivityExport.KEY_IS_GEOJSON);

        if(isGeoJson){
            Spinner spinnerGeoJson = (Spinner) rootView.findViewById(R.id.spinner_geo_json);
            AdapterSpinnerExtension adapterSpinnerExtension = new AdapterSpinnerExtension(getContext(),
                    getContext().getResources().getStringArray(R.array.files_extensions));
            spinnerGeoJson.setAdapter(adapterSpinnerExtension);
            spinnerGeoJson.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    extension = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            layoutTextVisibility.setVisibility(View.GONE);
            layoutGeoJsonSettings.setVisibility(View.VISIBLE);
        }

        if (mapObjects != null) {
            if (!mapObjects.isEmpty()) {
                setShowObjects();

                checkBoxText.setEnabled(true);
                checkBoxPoint.setEnabled(true);
                checkBoxPolyline.setEnabled(true);
                checkBoxPolygon.setEnabled(true);

                checkBoxText.setChecked(true);
                checkBoxPoint.setChecked(true);
                checkBoxPolyline.setChecked(true);
                checkBoxPolygon.setChecked(true);

                checkBoxText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        filterObjects();
                    }
                });

                checkBoxPoint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        filterObjects();
                    }
                });

                checkBoxPolyline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        filterObjects();
                    }
                });

                checkBoxPolygon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        filterObjects();
                    }
                });


                setJsonToTextView(mapObjects);
            }
        }

        return rootView;
    }

    private void setNullObjects() {
        isJsonVisible = false;
        scrollViewJson.setVisibility(View.GONE);
        textViewNoObjects.setVisibility(View.VISIBLE);

        if (listener != null) {
            if (isGeoJson)
                listener.jsonChange(2, false);
            else
                listener.jsonChange(1, false);
        }
    }

    private void setShowObjects() {
        isJsonVisible = true;
        scrollViewJson.setVisibility(View.VISIBLE);
        textViewNoObjects.setVisibility(View.GONE);

        if (listener != null) {
            if (isGeoJson)
                listener.jsonChange(2, true);
            else
                listener.jsonChange(1, true);
        }

    }

    private void setJsonToTextView(ArrayList<MapObject> mapObjects) {
        if (mapObjects.isEmpty()) {
            setNullObjects();
        } else {
            setShowObjects();
            Gson gson = new Gson();
            if (isGeoJson) {
                jsnoStyle = gson.toJson(Tools.mapObjectsToGeoJson(mapObjects), new TypeToken<GeoJson>() {
                }.getType()).replaceAll("pointcoordinates", "coordinates").replaceAll("polygoncoordinates", "coordinates");
            } else {
                jsnoStyle = gson.toJson(mapObjects, new TypeToken<ArrayList<MapObject>>() {
                }.getType());
            }
            if (jsnoStyle.equals("null")) setNullObjects();
            else
                textViewJson.setText(Tools.formatJsonString(jsnoStyle, getContext()));
        }
    }

    private void filterObjects() {
        ArrayList<MapObject> tempMapObjects = new ArrayList<>();

        for (int o = 0; o < mapObjects.size(); o++) {
            if (mapObjects.get(o).type.equals(ActivityGoogleMap.TYPE_TEXT) && checkBoxText.isChecked())
                tempMapObjects.add(mapObjects.get(o));
            if (mapObjects.get(o).type.equals(ActivityGoogleMap.TYPE_POINT) && checkBoxPoint.isChecked())
                tempMapObjects.add(mapObjects.get(o));
            if (mapObjects.get(o).type.equals(ActivityGoogleMap.TYPE_LINE) && checkBoxPolyline.isChecked())
                tempMapObjects.add(mapObjects.get(o));
            if (mapObjects.get(o).type.equals(ActivityGoogleMap.TYPE_POLYGON) && checkBoxPolygon.isChecked())
                tempMapObjects.add(mapObjects.get(o));
        }
        setJsonToTextView(tempMapObjects);
    }

    public boolean isJsonVisible() {
        return isJsonVisible;
    }

    public String getJsnoStyle() {
        return jsnoStyle;
    }

    public int getExtension(){return extension;}
}
