package eu.fukysoft.multymaps.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import eu.fukysoft.multymaps.Activitys.ActivityExport;
import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Interfaces.ChangeJsonState;
import eu.fukysoft.multymaps.Utils.Tools;

/**
 * Created by Funky on 10/20/2017.
 */

public class FragmentExImMapStudio extends Fragment {

    private String finalJson;
    private boolean isJsonVisible;
    private ChangeJsonState listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ChangeJsonState) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_imex_style, container, false);

        TextView textViewJson = (TextView) rootView.findViewById(R.id.textview_json);
        TextView textViewNoObjects = (TextView) rootView.findViewById(R.id.textview_noobjects);
        ScrollView scrollViewJson = (ScrollView) rootView.findViewById(R.id.scroll_view_json);

        String jsnoStyle = getArguments().getString(ActivityExport.KEY_STYLE_JSON);
        ArrayList<MapObject> mapObjects = (ArrayList<MapObject>) getArguments().getSerializable(ActivityExport.KEY_OBJECT_LIST);

        Gson gson = new Gson();
        String jsnoObjects = gson.toJson(mapObjects, new TypeToken<ArrayList<MapObject>>() {
        }.getType());


        finalJson = "{";

        if (jsnoStyle != null && !jsnoStyle.equals("")) {
            finalJson = finalJson + "\"styleoptions\":" + jsnoStyle;
        }

        if (jsnoObjects != null && !jsnoObjects.equals("") && mapObjects != null) {
            if (!mapObjects.isEmpty()) {
                if (jsnoStyle != null && !jsnoStyle.equals("")){
                    finalJson = finalJson +",";
                }
                finalJson = finalJson + "\"mapobjects\":" + jsnoObjects;
            }
        }

        finalJson = finalJson + "}";

        if (!finalJson.equals("{}")) {
            isJsonVisible = true;
            scrollViewJson.setVisibility(View.VISIBLE);
            textViewNoObjects.setVisibility(View.GONE);
            textViewJson.setText(Tools.formatJsonString(finalJson,getContext()));
        }else{
            isJsonVisible = false;
        }

        if(listener!=null) listener.jsonChange(3,false);

        return rootView;
    }

    public boolean isJsonVisible() {
        return isJsonVisible;
    }

    public String getJsnoStyle() {
        return finalJson;
    }
}
