package eu.fukysoft.multymaps.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;

import eu.fukysoft.multymaps.Activitys.ActivityGoogleMap;
import eu.fukysoft.multymaps.Activitys.ActivityObjectList;
import eu.fukysoft.multymaps.Adpaters.AdapterPointList;
import eu.fukysoft.multymaps.Adpaters.AdapterPolygonList;
import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.Interfaces.OnObjectOptionsButtonsClick;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Utils.Tools;

/**
 * Created by Funky on 8/25/2017.
 */

public class FragmentList extends Fragment {

    public ArrayList<MapObject> objectList;
    private OnObjectOptionsButtonsClick listener;
    private AdapterPolygonList adapterPolyline;
    private AdapterPolygonList adapterPolygon;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnObjectOptionsButtonsClick) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);

        ListView listViewObject = (ListView) rootView.findViewById(R.id.listview_objectlist);
        LinearLayout layoutEmptyList = (LinearLayout) rootView.findViewById(R.id.layout_empty_list);

        String type = getArguments().getString(ActivityObjectList.KEY_TYPE);
        objectList = (ArrayList<MapObject>) getArguments().getSerializable(ActivityObjectList.KEY_OBJECT_LIST);
        if (objectList == null) objectList = new ArrayList<>();
        if (objectList.isEmpty()) {
            layoutEmptyList.setVisibility(View.VISIBLE);
        } else {
            listViewObject.setVisibility(View.VISIBLE);
            type = objectList.get(0).type;
        }

        switch (type) {
            case ActivityGoogleMap.TYPE_TEXT:
                setInfoToolbarText(rootView, objectList);
                AdapterPointList adapterText = new AdapterPointList(getContext(), objectList);
                listViewObject.setAdapter(adapterText);
                break;
            case ActivityGoogleMap.TYPE_POINT:
                setInfoToolbarText(rootView, objectList);
                AdapterPointList adapterPoint = new AdapterPointList(getContext(), objectList);
                listViewObject.setAdapter(adapterPoint);
                break;
            case ActivityGoogleMap.TYPE_LINE:
                setInfoToolBarObjects(rootView, objectList);
                adapterPolyline = new AdapterPolygonList(getContext(), objectList, new AdapterPolygonList.OnButtonsClick() {
                    @Override
                    public void onEraseClick(int position) {
                        objectList.remove(position);
                        adapterPolyline.notifyDataSetChanged();
                    }

                    @Override
                    public void onLocateClick(MapObject mapObject) {
                        if (listener != null) listener.onLocateObject(mapObject);
                    }

                    @Override
                    public void onEditClick(int position) {
                        if (listener != null) listener.onEditObject(position, 2);
                    }
                });
                listViewObject.setAdapter(adapterPolyline);
                break;
            case ActivityGoogleMap.TYPE_POLYGON:
                setInfoToolBarObjects(rootView, objectList);
                adapterPolygon = new AdapterPolygonList(getContext(), objectList, new AdapterPolygonList.OnButtonsClick() {
                    @Override
                    public void onEraseClick(int position) {
                        objectList.remove(position);
                        adapterPolygon.notifyDataSetChanged();
                    }

                    @Override
                    public void onLocateClick(MapObject mapObject) {
                        if (listener != null) listener.onLocateObject(mapObject);
                    }

                    @Override
                    public void onEditClick(int position) {
                        if (listener != null) listener.onEditObject(position, 3);
                    }
                });
                listViewObject.setAdapter(adapterPolygon);
                break;
        }
        return rootView;
    }

    private void setInfoToolbarText(View rootView, ArrayList<MapObject> objectList) {
        TextView textCountPoints = (TextView) rootView.findViewById(R.id.text_text_count);
        LinearLayout layoutInfo = (LinearLayout) rootView.findViewById(R.id.layout_info_objects);
        LinearLayout layoutText = (LinearLayout) rootView.findViewById(R.id.layout_info_text);
        layoutInfo.setVisibility(View.GONE);
        layoutText.setVisibility(View.VISIBLE);
        textCountPoints.setText("" + objectList.size());
    }

    private void setInfoToolBarObjects(View rootView, ArrayList<MapObject> objectList) {
        TextView textCount = (TextView) rootView.findViewById(R.id.text_object_count);
        TextView textPointsCount = (TextView) rootView.findViewById(R.id.text_points_count);
        TextView textTotalArea = (TextView) rootView.findViewById(R.id.text_area_total);
        TextView textTotalDistance = (TextView) rootView.findViewById(R.id.text_distance_total);

        int points = 0;
        double area = 0;
        double distance = 0;

        for (int s = 0; s < objectList.size(); s++) {
            points = points + objectList.get(s).lonList.size();
            area = area + SphericalUtil.computeArea(Tools.getLatLongList(objectList.get(s).latList, objectList.get(s).lonList));
            distance = distance + SphericalUtil.computeLength(Tools.getLatLongList(objectList.get(s).latList, objectList.get(s).lonList));
        }

        textCount.setText("" + objectList.size());
        textPointsCount.setText("" + points);
        textTotalArea.setText("" + Tools.toUnitsQuad(area,getContext()));
        textTotalDistance.setText("" + Tools.toUnitsLenght(distance,getContext()));
    }


}