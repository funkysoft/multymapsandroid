package eu.fukysoft.multymaps.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import eu.fukysoft.multymaps.Activitys.ActivityExport;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Interfaces.ChangeJsonState;
import eu.fukysoft.multymaps.Utils.Tools;

/**
 * Created by Funky on 10/20/2017.
 */

public class FragmentExImStyle extends Fragment {

    private String jsnoStyle;
    private boolean isJsonVisible;

    private ChangeJsonState listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ChangeJsonState) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_imex_style, container, false);

        TextView textViewJson = (TextView) rootView.findViewById(R.id.textview_json);
        TextView textViewNoObjects = (TextView) rootView.findViewById(R.id.textview_noobjects);
        ScrollView scrollViewJson = (ScrollView) rootView.findViewById(R.id.scroll_view_json);

        jsnoStyle = getArguments().getString(ActivityExport.KEY_STYLE_JSON);


        if (jsnoStyle != null && !jsnoStyle.equals("")) {
            isJsonVisible = true;
            scrollViewJson.setVisibility(View.VISIBLE);
            textViewNoObjects.setVisibility(View.GONE);
            textViewJson.setText(Tools.formatJsonString(jsnoStyle,getContext()));
        }else{
            isJsonVisible = false;

        }
        if(listener!=null) listener.jsonChange(0,isJsonVisible);

        return rootView;
    }
    public boolean isJsonVisible() {
        return isJsonVisible;
    }

    public String getJsnoStyle() {
        return jsnoStyle;
    }

}
