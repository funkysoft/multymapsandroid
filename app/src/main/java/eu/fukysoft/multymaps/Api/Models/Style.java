package eu.fukysoft.multymaps.Api.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Funky on 9/11/2017.
 */

public class Style {
    @SerializedName("style")
    private String style;

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
