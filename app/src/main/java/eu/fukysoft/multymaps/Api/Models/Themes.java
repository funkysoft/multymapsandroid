package eu.fukysoft.multymaps.Api.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Funky on 9/10/2017.
 */

public class Themes {
    public class Theme {
        @SerializedName("id")
        private int id;

        public int getId() {
            return this.id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @SerializedName("name")
        private String name;

        public String getName() {
            return this.name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @SerializedName("json")
        private String json;

        public String getJson() {
            return this.json;
        }

        public void setJson(String json) {
            this.json = json;
        }

        @SerializedName("image")
        private String image;

        public String getImage() {
            return this.image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    @SerializedName("themes")
    private ArrayList<Theme> themes;

    public ArrayList<Theme> getThemes() {
        return this.themes;
    }

    public void setThemes(ArrayList<Theme> themes) {
        this.themes = themes;
    }

}
