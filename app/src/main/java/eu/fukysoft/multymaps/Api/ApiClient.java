package eu.fukysoft.multymaps.Api;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mazof on 6.5.2017.
 */

public class ApiClient {


    public static Retrofit retrofit;

    public static Retrofit getClient(String url) {
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(url)
                .build();
        return retrofit;
    }
}
