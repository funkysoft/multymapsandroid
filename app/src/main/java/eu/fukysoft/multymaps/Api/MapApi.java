package eu.fukysoft.multymaps.Api;


import eu.fukysoft.multymaps.Api.Models.Elevation;
import eu.fukysoft.multymaps.Api.Models.Style;
import eu.fukysoft.multymaps.Api.Models.Themes;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by mazof on 6.5.2017.
 */

public interface MapApi {

    String BASE_URL = "http://www.funkysoft.cz/";
    String GOOGLE_URL = "https://maps.googleapis.com/";


    @Headers({"maptoken:Zmd9s7lfQmGYGBn1phtnGyVcwNxKAUNYh7wx7y83JsjGWL0X3btAW8xIStMdptKY"})
    @GET("api/map/themes")
    Call<Themes> getThemes();

    @Headers({"maptoken:Zmd9s7lfQmGYGBn1phtnGyVcwNxKAUNYh7wx7y83JsjGWL0X3btAW8xIStMdptKY"})
    @GET("api/map/gettheme")
    Call<Style> getTheme(@Query("id") int id);


    @GET("maps/api/elevation/json")
    Call<Elevation> getElevation(@Query("locations") String locations, @Query("sensor") boolean sensor);

}
