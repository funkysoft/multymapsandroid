package eu.fukysoft.multymaps.Api.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Funky on 9/11/2017.
 */

public class Elevation {

    @SerializedName("results")
    private ArrayList<Result> results;

    public ArrayList<Result> getResults() {
        return this.results;
    }

    public void setResults(ArrayList<Result> results) {
        this.results = results;
    }

    @SerializedName("status")
    private String status;

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class Location {
        @SerializedName("lat")
        private double lat;

        public double getLat() {
            return this.lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }
        @SerializedName("lng")
        private double lng;

        public double getLng() {
            return this.lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public class Result {
        @SerializedName("elevation")
        private double elevation;

        public double getElevation() {
            return this.elevation;
        }

        public void setElevation(double elevation) {
            this.elevation = elevation;
        }

        @SerializedName("location")
        private Location location;

        public Location getLocation() {
            return this.location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        @SerializedName("resolution")
        private double resolution;

        public double getResolution() {
            return this.resolution;
        }

        public void setResolution(double resolution) {
            this.resolution = resolution;
        }
    }

}

