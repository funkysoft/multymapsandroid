package eu.fukysoft.multymaps.Adpaters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import eu.fukysoft.multymaps.R;

/**
 * Created by Majo on 24.10.2017.
 */

public class AdapterSpinnerExtension extends BaseAdapter {
    private String[] extensions;
    private Context context;

    public AdapterSpinnerExtension(Context context, String[] extensions) {
        this.context = context;
        this.extensions = extensions;
    }

    @Override
    public int getCount() {
        return this.extensions.length;
    }

    @Override
    public Object getItem(int position) {
        return this.extensions[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.spinner_style, parent, false);
        TextView textViewExtension = (TextView) convertView.findViewById(R.id.textview_spinner_extension);
        textViewExtension.setText(extensions[position]);
        return convertView;
    }
}
