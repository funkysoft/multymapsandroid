package eu.fukysoft.multymaps.Adpaters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import eu.fukysoft.multymaps.Fragments.FragmentExImMapStudio;
import eu.fukysoft.multymaps.Fragments.FragmentExImObjects;
import eu.fukysoft.multymaps.Fragments.FragmentExImStyle;


/**
 * Created by Funky on 8/25/2017.
 */

public class AdapterPageExIm extends FragmentPagerAdapter {

    private FragmentExImStyle fragmentExImStyle;
    private FragmentExImObjects fragmentExImObjects;
    private FragmentExImObjects fragmentExImGeoJson;
    private FragmentExImMapStudio fragmentExImMapStudio;

    public AdapterPageExIm(FragmentManager fm,
                           FragmentExImStyle fragmentExImStyle,
                           FragmentExImObjects fragmentExImObjects,
                           FragmentExImObjects fragmentExImGeoJson,
                           FragmentExImMapStudio fragmentExImMapStudio) {

        super(fm);
        this.fragmentExImStyle = fragmentExImStyle;
        this.fragmentExImObjects = fragmentExImObjects;
        this.fragmentExImGeoJson = fragmentExImGeoJson;
        this.fragmentExImMapStudio = fragmentExImMapStudio;

    }

    @Override
    public Fragment getItem(int i) {

        switch (i){
            case 0:
                return this.fragmentExImStyle;
            case 1:
                return this.fragmentExImObjects;
            case 2:
                return this.fragmentExImGeoJson;
            case 3:
                return this.fragmentExImMapStudio;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}

