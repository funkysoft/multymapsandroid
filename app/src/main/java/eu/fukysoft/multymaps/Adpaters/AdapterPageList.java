package eu.fukysoft.multymaps.Adpaters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import java.util.List;

import eu.fukysoft.multymaps.Fragments.FragmentList;


/**
 * Created by Funky on 8/25/2017.
 */

public class AdapterPageList extends FragmentPagerAdapter {
    private List<FragmentList> fragmentList;
    public AdapterPageList(FragmentManager fm, List<FragmentList> fragmentList) {
        super(fm);
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}

