package eu.fukysoft.multymaps.Adpaters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;

import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Utils.Tools;

/**
 * Created by Funky on 8/26/2017.
 */

public class AdapterPointList extends BaseAdapter {

    private Context context;
    private ArrayList<MapObject> objectList = new ArrayList<>();

    public AdapterPointList(Context context, ArrayList<MapObject> objectList) {

        this.context = context;
        this.objectList = objectList;

    }

    @Override
    public int getCount() {
        return objectList.size();
    }

    @Override
    public Object getItem(int position) {
        return objectList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;

        if (view == null) {
            viewHolder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.adapter_point_item, parent, false);

            viewHolder.textName = (TextView) view.findViewById(R.id.text_name);
            viewHolder.textPoints = (TextView) view.findViewById(R.id.text_points);
            viewHolder.textLat = (TextView) view.findViewById(R.id.text_latitude);
            viewHolder.textLon = (TextView) view.findViewById(R.id.text_longitude);
            viewHolder.buttonErase = (ImageView) view.findViewById(R.id.icon_erase);
            viewHolder.buttonLocate = (ImageView) view.findViewById(R.id.icon_locate);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.textName.setText(objectList.get(position).name);
        viewHolder.textPoints.setText("" + objectList.get(position).lonList.size());
        viewHolder.textLat.setText(""+objectList.get(position).latList.get(0));
        viewHolder.textLon.setText(""+objectList.get(position).lonList.get(0));


        return view;
    }

    public static class ViewHolder {
        TextView textName;
        TextView textPoints;
        TextView textLat;
        TextView textLon;
        ImageView buttonErase;
        ImageView buttonLocate;
    }
}
