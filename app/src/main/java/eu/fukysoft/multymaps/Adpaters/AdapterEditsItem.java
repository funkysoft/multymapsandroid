package eu.fukysoft.multymaps.Adpaters;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.fukysoft.multymaps.Models.ItemListEdits;
import eu.fukysoft.multymaps.R;

/**
 * Created by mazof on 29.1.2017.
 */

public class AdapterEditsItem extends BaseAdapter {

    private final Context context;
    private final ArrayList<ItemListEdits> array;
    private List<Drawable> drawables;

    public AdapterEditsItem(Context context, ArrayList<ItemListEdits> array) {
        this.array = array;
        this.context = context;
        drawables = new ArrayList<>();

    }

    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public Object getItem(int position) {
        return array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;


            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_edititem_layout, parent, false);
           TextView textview = (TextView) convertView.findViewById(R.id.item_text);

        textview.setPadding(array.get(position).getPadding()*10,0,0,0);
        if(array.get(position).getPadding()==0)
            textview.setTypeface(null, Typeface.BOLD);
        textview.setText(array.get(position).getDescription());


        return convertView;
    }

    private static class ViewHolder {
        TextView textview;
    }
}