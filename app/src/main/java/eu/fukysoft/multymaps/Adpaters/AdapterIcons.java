package eu.fukysoft.multymaps.Adpaters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import eu.fukysoft.multymaps.R;

/**
 * Created by mazof on 29.1.2017.
 */

public class AdapterIcons extends BaseAdapter {

    private final Context context;
    private final String[] array;
    private ArrayList<Drawable> drawables;
    private boolean isLeft;

    public AdapterIcons(Context context, String[] array, ArrayList<Drawable> drawables, boolean isLeft) {
        this.array = array;
        this.context = context;
        this.drawables = drawables;
        this.isLeft = isLeft;
    }

    @Override
    public int getCount() {
        return array.length;
    }

    @Override
    public Object getItem(int position) {
        return array[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            if (array[position].contains("-"))
                convertView = LayoutInflater.from(context).inflate(R.layout.adapter_clean_icon_text, parent, false);
            else
                convertView = LayoutInflater.from(context).inflate(R.layout.adapter_clean_icon, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textview = (TextView) convertView.findViewById(R.id.text_name);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image_icon);
            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        if (array[position].contains("-"))
            viewHolder.textview.setText("   " + array[position]);
        else
            viewHolder.textview.setText(array[position]);

        if (isLeft)
            viewHolder.textview.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        else
            viewHolder.textview.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

        if (drawables != null)
            if (position < drawables.size())
                if (drawables.get(position) != null) {
                    viewHolder.image.setImageDrawable(drawables.get(position));
                }

        return convertView;
    }

    private static class ViewHolder {
        TextView textview;
        ImageView image;
    }

}