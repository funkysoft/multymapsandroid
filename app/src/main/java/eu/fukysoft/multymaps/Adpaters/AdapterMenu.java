package eu.fukysoft.multymaps.Adpaters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.fukysoft.multymaps.R;

/**
 * Created by mazof on 29.1.2017.
 */

public class AdapterMenu extends BaseAdapter {

    private final Context context;
    private final String[] array;
    private ArrayList<Drawable> drawables;

    public AdapterMenu(Context context, String[] array, ArrayList<Drawable> drawables) {
        this.array = array;
        this.context = context;
        this.drawables = drawables;
    }

    @Override
    public int getCount() {
        return array.length;
    }

    @Override
    public Object getItem(int position) {
        return array[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_menuitem_layout, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textview = (TextView) convertView.findViewById(R.id.adapter_menu_text);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.iconImg);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.textview.setText(array[position]);


            viewHolder.image.setImageDrawable(drawables.get(position));

        return convertView;
    }

    private static class ViewHolder {
        TextView textview;
        ImageView image;
    }

}