package eu.fukysoft.multymaps.Adpaters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import eu.fukysoft.multymaps.Models.LibraryItem;
import eu.fukysoft.multymaps.R;

/**
 * Created by mazof on 29.1.2017.
 */

public class AdapterLibrary extends BaseAdapter {

    private final Context context;
    private final ArrayList<LibraryItem> array;
    private ArrayList<Drawable> drawables;
    public boolean isEraseList;

    public AdapterLibrary(Context context, ArrayList<LibraryItem> array, ArrayList<Drawable> drawables, boolean isEraseList) {
        this.array = array;
        this.context = context;
        this.drawables = drawables;
        this.isEraseList = isEraseList;
    }

    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public Object getItem(int position) {
        return array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_library_layout, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textview = (TextView) convertView.findViewById(R.id.adapter_menu_text);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.iconImg);
            viewHolder.checkBoxEarse = (CheckBox) convertView.findViewById(R.id.checkbox_erase);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.textview.setText(array.get(position).getName());
        viewHolder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.open_icon));
        viewHolder.checkBoxEarse.setChecked(array.get(position).isChecked());

        if (isEraseList) {
           viewHolder.image.setVisibility(View.GONE);
            viewHolder.checkBoxEarse.setVisibility(View.VISIBLE);
        } else {
            viewHolder.image.setVisibility(View.VISIBLE);
            viewHolder.checkBoxEarse.setVisibility(View.GONE);
        }
        viewHolder.checkBoxEarse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                array.get(position).setChecked(isChecked);
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        TextView textview;
        ImageView image;
        CheckBox checkBoxEarse;
    }

    public void setListErase(){
        this.isEraseList = true;
        notifyDataSetChanged();
    }

    public void setLibrary(){
        this.isEraseList = false;
        for( LibraryItem item : array){
            item.setChecked(false);
        }
        notifyDataSetChanged();
    }
}