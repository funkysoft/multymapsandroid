package eu.fukysoft.multymaps.Adpaters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;

import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Utils.Dialogs;
import eu.fukysoft.multymaps.Utils.Tools;

/**
 * Created by Funky on 8/26/2017.
 */

public class AdapterPolygonList extends BaseAdapter {

    private Context context;
    private ArrayList<MapObject> objectList = new ArrayList<>();
    private MaterialDialog dialog;
    private OnButtonsClick listener;

    public AdapterPolygonList(Context context, ArrayList<MapObject> objectList, OnButtonsClick listener) {

        this.context = context;
        this.objectList = objectList;
        this.listener = listener;

    }

    @Override
    public int getCount() {
        return objectList.size();
    }

    @Override
    public Object getItem(int position) {
        return objectList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;

        if (view == null) {
            viewHolder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.adapter_polygon_item, parent, false);

            viewHolder.textName = (TextView) view.findViewById(R.id.text_name);
            viewHolder.textPoints = (TextView) view.findViewById(R.id.text_points);
            viewHolder.textArea = (TextView) view.findViewById(R.id.text_area);
            viewHolder.textDistance = (TextView) view.findViewById(R.id.text_distance);
            viewHolder.buttonErase = (ImageView) view.findViewById(R.id.icon_erase);
            viewHolder.buttonLocate = (ImageView) view.findViewById(R.id.icon_locate);
            viewHolder.buttonEdit = (ImageView) view.findViewById(R.id.icon_edit);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.textName.setText(objectList.get(position).name);
        viewHolder.textPoints.setText("" + objectList.get(position).lonList.size());
        viewHolder.textArea.setText("" + Tools.toUnitsQuad(SphericalUtil.computeArea(
                Tools.getLatLongList(objectList.get(position).latList,
                        objectList.get(position).lonList)),context));
        viewHolder.textDistance.setText("" + Tools.toUnitsLenght(SphericalUtil.computeLength(
                Tools.getLatLongList(objectList.get(position).latList,
                        objectList.get(position).lonList)
        ),context));

        viewHolder.buttonErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = Dialogs.sureDialog(context, new Dialogs.DialogSure() {
                    @Override
                    public void onDimmis() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onSubmit() {
                        if(listener!=null)listener.onEraseClick(position);
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }
        });

        viewHolder.buttonLocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onLocateClick(objectList.get(position));
                }
            }
        });

        viewHolder.buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(listener != null) listener.onEditClick(position);
            }
        });


        return view;
    }

    public static class ViewHolder {
        TextView textName;
        TextView textPoints;
        TextView textArea;
        TextView textDistance;
        ImageView buttonErase;
        ImageView buttonLocate;
        ImageView buttonEdit;
    }

    public interface OnButtonsClick{
        void onEraseClick(int position);
        void onLocateClick(MapObject mapObject);
        void onEditClick(int position);
    }
}
