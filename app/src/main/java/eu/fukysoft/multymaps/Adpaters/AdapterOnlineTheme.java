package eu.fukysoft.multymaps.Adpaters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import eu.fukysoft.multymaps.Api.MapApi;
import eu.fukysoft.multymaps.Api.Models.Themes;
import eu.fukysoft.multymaps.R;

/**
 * Created by mazof on 29.1.2017.
 */

public class AdapterOnlineTheme extends BaseAdapter {

    private final Context context;
    private ArrayList<Themes.Theme> themes;

    public AdapterOnlineTheme(Context context, ArrayList<Themes.Theme> themes){
        this.themes = themes;
        this.context = context;
    }

    @Override
    public int getCount() {
        return themes.size();
    }

    @Override
    public Object getItem(int position) {
        return themes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_menuitem_layout, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textview = (TextView) convertView.findViewById(R.id.adapter_menu_text);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.iconImg);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.textview.setText(themes.get(position).getName());
        String url = MapApi.BASE_URL + "www/" + themes.get(position).getImage();
        url = url.replaceAll(" ", "%20");
        Picasso.with(context).load(url).into(viewHolder.image);


        return convertView;
    }

    private static class ViewHolder {
        TextView textview;
        ImageView image;
    }
}