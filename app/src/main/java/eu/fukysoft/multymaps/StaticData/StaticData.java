package eu.fukysoft.multymaps.StaticData;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.HashMap;

import eu.fukysoft.multymaps.Models.ItemListEdits;
import eu.fukysoft.multymaps.R;

/**
 * Created by mazof on 30.1.2017.
 */

public class StaticData {

    public static ArrayList<ItemListEdits> listItemFeatureType = new ArrayList<>();
    public static ArrayList<ItemListEdits> listItemElementType = new ArrayList<>();
    public static ArrayList<Drawable> drawablesDraw = new ArrayList<>();
    public static ArrayList<Drawable> drawablesDrawOptionsDown = new ArrayList<>();
    public static ArrayList<Drawable> drawablesDrawOptions = new ArrayList<>();
    public static ArrayList<Drawable> drawablesGoogle = new ArrayList<>();
    public static ArrayList<Drawable> drawablesGoogleOptions = new ArrayList<>();
    public static ArrayList<Drawable> drawablesGoogleApp = new ArrayList<>();
    public static ArrayList<Drawable> drawablesGoogleCustom = new ArrayList<>();
    public static ArrayList<Drawable> drawablesGoogleOptionsMenu = new ArrayList<>();
    public static ArrayList<Drawable> drawablesTutorialMainAction = new ArrayList<>();
    public static ArrayList<Drawable> drawablesTutorialActionbar = new ArrayList<>();
    public static ArrayList<Drawable> drawablesTutorialActionScreenbar = new ArrayList<>();
    public static ArrayList<Drawable> drawablesTutorialActionEditScreenbar = new ArrayList<>();
    public static HashMap<String, Drawable> markers = new HashMap<>();
    public static String[] visibility = new String[]{"", "", "off", "simplified", "on"};
    public static ArrayList<Integer> RAW_ID = new ArrayList<>();
    public static String[] menuGoogleCustom;

    public static void inits(Context context) {

        listItemFeatureType.clear();
        listItemElementType.clear();
        drawablesGoogle.clear();
        drawablesGoogleCustom.clear();
        drawablesGoogleOptionsMenu.clear();
        drawablesTutorialMainAction.clear();
        drawablesGoogleApp.clear();
        drawablesTutorialActionbar.clear();
        drawablesTutorialActionScreenbar.clear();
        drawablesTutorialActionEditScreenbar.clear();

        listItemFeatureType.add(new ItemListEdits("All", 0, "all"));
        listItemFeatureType.add(new ItemListEdits("Administrative", 0, "administrative"));
        listItemFeatureType.add(new ItemListEdits("Country", 5, "administrative.country"));
        listItemFeatureType.add(new ItemListEdits("Province", 5, "administrative.province"));
        listItemFeatureType.add(new ItemListEdits("Locality", 5, "administrative.locality"));
        listItemFeatureType.add(new ItemListEdits("Neighborhood", 5, "administrative.neighborhood"));
        listItemFeatureType.add(new ItemListEdits("Land parcel", 5, "administrative.land_parcel"));
        listItemFeatureType.add(new ItemListEdits("Landspace", 0, "landscape"));
        listItemFeatureType.add(new ItemListEdits("Man-made", 5, "landscape.man_made"));
        listItemFeatureType.add(new ItemListEdits("Natural", 5, "landscape.natural"));
        listItemFeatureType.add(new ItemListEdits("Landcover", 10, "landscape.natural.landcover"));
        listItemFeatureType.add(new ItemListEdits("Terrain", 10, "landscape.natural.terrain"));
        listItemFeatureType.add(new ItemListEdits("Points of interest", 0, "poi"));
        listItemFeatureType.add(new ItemListEdits("Attraction", 5, "poi.attraction"));
        listItemFeatureType.add(new ItemListEdits("Business", 5, "poi.business"));
        listItemFeatureType.add(new ItemListEdits("Government", 5, "poi.government"));
        listItemFeatureType.add(new ItemListEdits("Medical", 5, "poi.medical"));
        listItemFeatureType.add(new ItemListEdits("Park", 5, "poi.park"));
        listItemFeatureType.add(new ItemListEdits("Place of worship", 5, "poi.place_of_worship"));
        listItemFeatureType.add(new ItemListEdits("School", 5, "poi.school"));
        listItemFeatureType.add(new ItemListEdits("Sports complex", 5, "poi.sports_complex"));
        listItemFeatureType.add(new ItemListEdits("Road", 0, "road"));
        listItemFeatureType.add(new ItemListEdits("Highway", 5, "road.highway"));
        listItemFeatureType.add(new ItemListEdits("Controlled access", 10, "road.highway.controlled_access"));
        listItemFeatureType.add(new ItemListEdits("Arterial", 5, "road.arterial"));
        listItemFeatureType.add(new ItemListEdits("Local", 5, "road.local"));
        listItemFeatureType.add(new ItemListEdits("Transit", 0, "transit"));
        listItemFeatureType.add(new ItemListEdits("Line", 5, "transit.line"));
        listItemFeatureType.add(new ItemListEdits("Station", 5, "transit.station"));
        listItemFeatureType.add(new ItemListEdits("Airport", 10, "transit.station.airport"));
        listItemFeatureType.add(new ItemListEdits("Bus", 10, "transit.station.bus"));
        listItemFeatureType.add(new ItemListEdits("Rail", 10, "transit.station.rail"));
        listItemFeatureType.add(new ItemListEdits("Water", 0, "water"));

        listItemElementType.add(new ItemListEdits("All", 0, "all"));
        listItemElementType.add(new ItemListEdits("Geometry", 0, "geometry"));
        listItemElementType.add(new ItemListEdits("Fill", 5, "geometry.fill"));
        listItemElementType.add(new ItemListEdits("Stroke", 5, "geometry.stroke"));
        listItemElementType.add(new ItemListEdits("Labels", 0, "labels"));
        listItemElementType.add(new ItemListEdits("Text", 5, "labels.text"));
        listItemElementType.add(new ItemListEdits("Text Fill", 10, "labels.text.fill"));
        listItemElementType.add(new ItemListEdits("Text outline", 10, "labels.text.stroke"));
        listItemElementType.add(new ItemListEdits("Icon", 5, "labels.icon"));

        drawablesDraw.add(ContextCompat.getDrawable(context, R.drawable.text_icon));
        drawablesDraw.add(ContextCompat.getDrawable(context, R.drawable.point_icon));
        drawablesDraw.add(ContextCompat.getDrawable(context, R.drawable.line_icon));
        drawablesDraw.add(ContextCompat.getDrawable(context, R.drawable.line_draw_icon));
        drawablesDraw.add(ContextCompat.getDrawable(context, R.drawable.polygon_icon));
        drawablesDraw.add(ContextCompat.getDrawable(context, R.drawable.polygon_draw_icon));

        drawablesDrawOptions.add(ContextCompat.getDrawable(context, R.drawable.list_icon));

        drawablesDrawOptionsDown.add(ContextCompat.getDrawable(context, R.drawable.erase_icon));

        drawablesGoogle.add(ContextCompat.getDrawable(context, R.drawable.standarticon));
        drawablesGoogle.add(ContextCompat.getDrawable(context, R.drawable.ngicon));
        drawablesGoogle.add(ContextCompat.getDrawable(context, R.drawable.terarianicon));
        drawablesGoogle.add(ContextCompat.getDrawable(context, R.drawable.hybridicon));

        drawablesGoogleOptions.add(ContextCompat.getDrawable(context, R.drawable.paint_icon));
        drawablesGoogleOptions.add(ContextCompat.getDrawable(context, R.drawable.polygon_icon));
        drawablesGoogleOptions.add(ContextCompat.getDrawable(context, R.drawable.import_icon));

        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.silvericon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.retroicon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.darkicon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.nighticon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.aubergineicon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.google_gold_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.mondrian_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.hopper_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.neon_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.joker_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.trident_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.skylines_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.dark_electric_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.nightvision_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.zombie_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.esperanto_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.lightstreet_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.cobalt_icon));
        drawablesGoogleCustom.add(ContextCompat.getDrawable(context, R.drawable.icyblue_icon));

        RAW_ID.add(R.raw.google_silver_style);
        RAW_ID.add(R.raw.google_retro_style);
        RAW_ID.add(R.raw.google_dark_style);
        RAW_ID.add(R.raw.google_night_style);
        RAW_ID.add(R.raw.google_aubergine_style);
        RAW_ID.add(R.raw.google_dark_gold);
        RAW_ID.add(R.raw.google_mondrian);
        RAW_ID.add(R.raw.google_hopper);
        RAW_ID.add(R.raw.google_neon);
        RAW_ID.add(R.raw.google_joker);
        RAW_ID.add(R.raw.google_trident);
        RAW_ID.add(R.raw.google_skylines);
        RAW_ID.add(R.raw.google_dark_electric);
        RAW_ID.add(R.raw.google_nigh_vision);
        RAW_ID.add(R.raw.google_zombie);
        RAW_ID.add(R.raw.google_esperanto);
        RAW_ID.add(R.raw.google_lightstreet);
        RAW_ID.add(R.raw.google_cobalt);
        RAW_ID.add(R.raw.google_icyblue);

        menuGoogleCustom = new String[]{
                "SILVER",
                "RETRO",
                "DARK",
                "NIGHT",
                "AUBERGINE",
                "DARK GOLD",
                "MONDRIAN",
                "HOPPER",
                "NEON",
                "JOKER",
                "TRIDENT",
                "SKYLINES",
                "DARK ELECTRIC",
                "NIGHT VISION",
                "ZOMBIE SURVIVAL",
                "ESPERANTO",
                "LIGHT STREET",
                "COBALT",
                "ICE BLUE"
        };

        drawablesGoogleOptionsMenu.add(ContextCompat.getDrawable(context, R.drawable.tools_icon));
        drawablesGoogleOptionsMenu.add(ContextCompat.getDrawable(context, R.drawable.mapstudio_button));


        drawablesGoogleApp.add(ContextCompat.getDrawable(context, R.drawable.tools_icon));
        drawablesGoogleApp.add(ContextCompat.getDrawable(context, R.drawable.tutorial_button));
        drawablesGoogleApp.add(ContextCompat.getDrawable(context, R.drawable.icon_info));
        drawablesGoogleApp.add(ContextCompat.getDrawable(context, R.drawable.back_button));

        drawablesTutorialActionbar.add(ContextCompat.getDrawable(context, R.drawable.mapstudio_button));
        drawablesTutorialActionbar.add(ContextCompat.getDrawable(context, R.drawable.save_icon));
        drawablesTutorialActionbar.add(ContextCompat.getDrawable(context, R.drawable.open_icon));
        drawablesTutorialActionbar.add(ContextCompat.getDrawable(context, R.drawable.screenshot_button));
        drawablesTutorialActionbar.add(ContextCompat.getDrawable(context, R.drawable.search_button));
        drawablesTutorialActionbar.add(ContextCompat.getDrawable(context, R.drawable.location_button));

        drawablesTutorialActionScreenbar.add(ContextCompat.getDrawable(context, R.drawable.pencil_button));
        drawablesTutorialActionScreenbar.add(ContextCompat.getDrawable(context, R.drawable.share_button));
        drawablesTutorialActionScreenbar.add(ContextCompat.getDrawable(context, R.drawable.save_icon));
        drawablesTutorialActionScreenbar.add(ContextCompat.getDrawable(context, R.drawable.close_button_black));

        drawablesTutorialActionEditScreenbar.add(ContextCompat.getDrawable(context, R.drawable.ok_button_black));
        drawablesTutorialActionEditScreenbar.add(ContextCompat.getDrawable(context, R.drawable.back_button));
        drawablesTutorialActionEditScreenbar.add(ContextCompat.getDrawable(context, R.drawable.color_button));
        drawablesTutorialActionEditScreenbar.add(ContextCompat.getDrawable(context, R.drawable.number_button));
        drawablesTutorialActionEditScreenbar.add(ContextCompat.getDrawable(context, R.drawable.close_button_black));

        drawablesTutorialMainAction.add(ContextCompat.getDrawable(context, R.drawable.location_button));
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(ContextCompat.getDrawable(context, R.drawable.paint_icon));
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(ContextCompat.getDrawable(context, R.drawable.polygon_icon));
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(ContextCompat.getDrawable(context, R.drawable.icon_info));
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(ContextCompat.getDrawable(context, R.drawable.import_icon));
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(ContextCompat.getDrawable(context, R.drawable.save_icon));
        drawablesTutorialMainAction.add(null);
        drawablesTutorialMainAction.add(null);

        markers.put("null", ContextCompat.getDrawable(context, R.drawable.marker_null));
        markers.put("1", ContextCompat.getDrawable(context, R.drawable.marker_1));
        markers.put("2", ContextCompat.getDrawable(context, R.drawable.marker_2));
        markers.put("3", ContextCompat.getDrawable(context, R.drawable.marker_3));
        markers.put("4", ContextCompat.getDrawable(context, R.drawable.marker_4));
        markers.put("a1", ContextCompat.getDrawable(context, R.drawable.marker_a1));
        markers.put("a2", ContextCompat.getDrawable(context, R.drawable.marker_a2));
        markers.put("a3", ContextCompat.getDrawable(context, R.drawable.marker_a3));
        markers.put("a4", ContextCompat.getDrawable(context, R.drawable.marker_a4));
        markers.put("a5", ContextCompat.getDrawable(context, R.drawable.marker_a5));
        markers.put("b1", ContextCompat.getDrawable(context, R.drawable.marker_b1));
        markers.put("b2", ContextCompat.getDrawable(context, R.drawable.marker_b2));
        markers.put("b3", ContextCompat.getDrawable(context, R.drawable.marker_b3));
        markers.put("b4", ContextCompat.getDrawable(context, R.drawable.marker_b4));
        markers.put("b5", ContextCompat.getDrawable(context, R.drawable.marker_b5));
        markers.put("d1", ContextCompat.getDrawable(context, R.drawable.marker_d1));
        markers.put("d2", ContextCompat.getDrawable(context, R.drawable.marker_d2));
        markers.put("d3", ContextCompat.getDrawable(context, R.drawable.marker_d3));
        markers.put("d4", ContextCompat.getDrawable(context, R.drawable.marker_d4));
        markers.put("d5", ContextCompat.getDrawable(context, R.drawable.marker_d5));
        markers.put("x1", ContextCompat.getDrawable(context, R.drawable.marker_x1));
        markers.put("x2", ContextCompat.getDrawable(context, R.drawable.marker_x2));
        markers.put("x3", ContextCompat.getDrawable(context, R.drawable.marker_x3));
        markers.put("x4", ContextCompat.getDrawable(context, R.drawable.marker_x4));
        markers.put("x", ContextCompat.getDrawable(context, R.drawable.marker_x));
        markers.put("loc", ContextCompat.getDrawable(context, R.drawable.locate_marker));


    }
}
