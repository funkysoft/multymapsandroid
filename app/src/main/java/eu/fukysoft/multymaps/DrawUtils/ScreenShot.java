package eu.fukysoft.multymaps.DrawUtils;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.vision.text.Line;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;

import eu.fukysoft.multymaps.Activitys.ActivityExport;
import eu.fukysoft.multymaps.Activitys.ActivityGoogleMap;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Utils.Dialogs;
import eu.fukysoft.multymaps.Utils.Tools;

/**
 * Created by Funky on 12/2/2017.
 */

public class ScreenShot {
    private final int ANIM_DURATION = 1000;

    public LinearLayout rootLayout;
    private LinearLayout layoutDrawTools;
    private LinearLayout layoutDrawActions;
    private LinearLayout layoutDrawScreenShot;

    private View colorView;
    private TextView textViewHeight;

    private MaterialDialog weightDialog;
    private ScreenShotImage imageViewScreenShoot;
    public Bitmap screenshot;
    public Dialog progressDialog;
    private int color;
    private int height;

    public ScreenShot(final ActivityGoogleMap activity, GoogleMap googleMap) {
        color = ContextCompat.getColor(activity, R.color.colorAccent);
        height = 5;

        progressDialog = Dialogs.progressDialog(activity);
        progressDialog.show();
        final int screenHeight = Tools.getDisplayDimension(activity)[0];

        layoutDrawTools = (LinearLayout) activity.findViewById(R.id.layout_tools_screenshot);
        layoutDrawActions = (LinearLayout) activity.findViewById(R.id.layout_actions_screenshot);
        layoutDrawScreenShot = (LinearLayout) activity.findViewById(R.id.layout_draw_screenshot);

        colorView = activity.findViewById(R.id.color_view_screenshot);
        colorView.setBackgroundColor(color);

        textViewHeight = (TextView) activity.findViewById(R.id.text_weight_screenshot);
        textViewHeight.setText("" + height);

        rootLayout = (LinearLayout) activity.findViewById(R.id.layout_screenshoot);
        rootLayout.setY(screenHeight);
        imageViewScreenShoot = (ScreenShotImage) activity.findViewById(R.id.imageview_screenshoot);
        imageViewScreenShoot.initScreenSHot(color, height);

        activity.findViewById(R.id.share_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bitmapPath = MediaStore.Images.Media.insertImage(activity.getContentResolver(), screenshot, "title", null);
                Uri bitmapUri = Uri.parse(bitmapPath);

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("image/png");
                intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
                activity.startActivity(Intent.createChooser(intent, "Share"));
            }
        });

        activity.findViewById(R.id.save_screenshot_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File dir = new File(Environment.getExternalStorageDirectory(), ActivityExport.FOLDER_NAME);
                if (!dir.exists()) {
                    dir.mkdirs();
                }

                final File output = new File(dir, System.currentTimeMillis() + ".jpg");
                OutputStream os = null;
                try {
                    os = new FileOutputStream(output);
                    screenshot.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    os.flush();
                    os.close();

                    Toast.makeText(
                            activity,
                            activity.getString(R.string.file_has_been_saved) + output.getPath(),
                            Toast.LENGTH_SHORT).show();
                    //this code will scan the image so that it will appear in your gallery when you open next time
                    MediaScannerConnection.scanFile(activity, new String[]{output.toString()}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                public void onScanCompleted(String path, Uri uri) {

                                }
                            }
                    );
                } catch (Exception e) {
                }
            }
        });

        activity.findViewById(R.id.image_close_screenshot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rootLayout.setVisibility(View.GONE);
                rootLayout.animate().setDuration(1000).translationY(screenHeight);
                activity.isInScreenShotMode = false;
            }
        });

        activity.findViewById(R.id.pencil_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewScreenShoot.startDrawing();
                layoutDrawTools.setVisibility(View.GONE);
                layoutDrawActions.setVisibility(View.GONE);
                layoutDrawScreenShot.setVisibility(View.VISIBLE);
            }
        });

        activity.findViewById(R.id.back_draw_screenshot_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewScreenShoot.backDraw();
            }
        });

        activity.findViewById(R.id.close_draw_screenshot_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewScreenShoot.stopDrawing();
                layoutDrawTools.setVisibility(View.VISIBLE);
                layoutDrawActions.setVisibility(View.VISIBLE);
                layoutDrawScreenShot.setVisibility(View.GONE);

                Bitmap bitmap = Bitmap.createBitmap(
                        imageViewScreenShoot.getWidth(),
                        imageViewScreenShoot.getHeight(),
                        Bitmap.Config.ARGB_8888
                );

                Canvas canvas = new Canvas(bitmap);
                imageViewScreenShoot.draw(canvas);
                screenshot = bitmap;
            }
        });

        activity.findViewById(R.id.color_screenshot_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialogs.colorDialog(ActivityGoogleMap.COLOR_DIALOG_ID_SCREENSHOT,
                        color).show(activity);
            }
        });

        activity.findViewById(R.id.weight_screenshot_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weightDialog = Dialogs.weightDialog(activity, height, new Dialogs.DialogClose() {
                    @Override
                    public void onDimmis() {
                        weightDialog.dismiss();
                    }

                    @Override
                    public void onSubmit(int item) {
                        weightDialog.dismiss();
                        height = item;
                        textViewHeight.setText("" + height);
                        imageViewScreenShoot.newPaint(color, height);

                    }
                });
                weightDialog.show();
            }
        });

        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                screenshot = snapshot;
                try {
                    File file = new File("/mnt/sdcard/"
                            + "MyMapScreen" + System.currentTimeMillis()
                            + ".png");
                    FileOutputStream out = new FileOutputStream(file);

                    screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
                    file.delete();

                    rootLayout.setVisibility(View.VISIBLE);
                    rootLayout.animate().setDuration(ANIM_DURATION).translationY(0);
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, ANIM_DURATION);

                    imageViewScreenShoot.setImageBitmap(screenshot);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        googleMap.snapshot(callback);
    }

    public void setColor(int color) {
        this.color = color;
        colorView.setBackgroundColor(color);
        imageViewScreenShoot.newPaint(color, height);
    }


}
