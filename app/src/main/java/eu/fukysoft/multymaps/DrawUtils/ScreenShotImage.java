package eu.fukysoft.multymaps.DrawUtils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;

import java.util.ArrayList;


/**
 * Created by Funky on 12/3/2017.
 */

public class ScreenShotImage extends android.support.v7.widget.AppCompatImageView {

    private Path path;
    private Paint paint;
    private ArrayList<Path> pathList = new ArrayList<>();
    private ArrayList<Paint> paintList = new ArrayList<>();
    private boolean isDrawing;
    private boolean isTouch;
    private int color;
    private int height;
    public Canvas canvas;


    public ScreenShotImage(Context context) {
        super(context);
    }

    public ScreenShotImage(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScreenShotImage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void initScreenSHot(int color, int height) {
        this.color = color;
        this.height = height;
        pathList.clear();
        paintList.clear();
        path = new Path();
        newPaint(color,height);
        isDrawing = false;
        isTouch = false;
    }

    public void stopDrawing() {
        isDrawing = false;
    }

    public void startDrawing() {
        isDrawing = true;
        path = new Path();
        newPaint(color,height);

    }

    public void backDraw(){
        if(!pathList.isEmpty()){
            pathList.remove(pathList.size()-1);
            paintList.remove(paintList.size()-1);
        }
    }
    public void newPaint(int color, int height){
        this.color = color;
        this.height = height;
        paint = new Paint();
        paint.setStrokeWidth(height);
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

            for (int p = 0; p < pathList.size(); p++) {
                if (pathList.get(p) != null)
                    canvas.drawPath(pathList.get(p), paintList.get(p));
            }
            if (path != null  && isTouch)
                canvas.drawPath(path, paint);
       this.canvas = canvas;
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isDrawing) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    isTouch = true;
                    path = new Path();
                    path.moveTo(event.getX(), event.getY());
                    newPaint(color,height);
                    break;
                case MotionEvent.ACTION_UP:
                    isTouch = false;
                    pathList.add(path);
                    paintList.add(paint);
                    break;
            }

            path.lineTo(event.getX(), event.getY());
        }

        return true;
    }
}
