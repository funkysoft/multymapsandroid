package eu.fukysoft.multymaps.Activitys;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.RadioGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Utils.Analitics;
import eu.fukysoft.multymaps.Utils.App;
import eu.fukysoft.multymaps.Utils.PerzistDataModel;

public class ActivitySettings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Analitics.logEvents(FirebaseAnalytics.Param.ITEM_NAME,this.getClass().getSimpleName());

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_settings);

        final RadioGroup radioGroupLenght = findViewById(R.id.radio_gruop_lenght_units);
        final RadioGroup radioGroupArea = findViewById(R.id.radio_gruop_area);

        final CheckBox checkBoxScale = findViewById(R.id.checkbox_show_scale);
        final CheckBox checkBoxPosition = findViewById(R.id.checkbox_auto_save_position);
        final CheckBox checkBoxStyle = findViewById(R.id.checkbox_auto_save_style);
        final CheckBox checkBoxObjects = findViewById(R.id.checkbox_auto_save_objects);

        switch(PerzistDataModel.getLenghtUnits(this)){
            case App.METRIC_UNITS:
                radioGroupLenght.check(R.id.radio_lenght_metic);
                break;
            case App.US_UNITS:
                radioGroupLenght.check(R.id.radio_lenght_us);
                break;
        }

        switch(PerzistDataModel.getAreaUnits(this)){
            case App.METRIC_UNITS:
                radioGroupArea.check(R.id.radio_area_metric);
                break;
            case App.US_UNITS:
                radioGroupArea.check(R.id.radio_area_us);
                break;
        }

        checkBoxScale.setChecked(PerzistDataModel.isScaleShow(this));
        checkBoxPosition.setChecked(PerzistDataModel.isAutoSavePosition(this));
        checkBoxStyle.setChecked(PerzistDataModel.isAutoSaveStyle(this));
        checkBoxObjects.setChecked(PerzistDataModel.isAutoSaveObjects(this));

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.button_save_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String lenghtUnits = "";
                String areaUnits = "";

                switch(radioGroupArea.getCheckedRadioButtonId()){
                    case R.id.radio_area_metric:
                        areaUnits = App.METRIC_UNITS;
                        break;
                    case R.id.radio_area_us:
                        areaUnits = App.US_UNITS;
                        break;
                }

                switch(radioGroupLenght.getCheckedRadioButtonId()){
                    case R.id.radio_lenght_metic:
                        lenghtUnits = App.METRIC_UNITS;
                        break;
                    case R.id.radio_lenght_us:
                        lenghtUnits = App.US_UNITS;
                        break;
                }

                PerzistDataModel.initSettings(
                        ActivitySettings.this,
                        lenghtUnits,
                        areaUnits,
                        checkBoxScale.isChecked(),
                        checkBoxPosition.isChecked(),
                        checkBoxStyle.isChecked(),
                        checkBoxObjects.isChecked());

                finish();
            }
        });
    }
}
