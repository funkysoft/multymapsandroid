package eu.fukysoft.multymaps.Activitys;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jrummyapps.android.colorpicker.ColorPickerDialogListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import eu.fukysoft.multymaps.Adpaters.AdapterEditsItem;
import eu.fukysoft.multymaps.Adpaters.AdapterLibrary;
import eu.fukysoft.multymaps.Adpaters.AdapterMenu;
import eu.fukysoft.multymaps.Adpaters.AdapterOnlineTheme;
import eu.fukysoft.multymaps.Api.ApiClient;
import eu.fukysoft.multymaps.Api.MapApi;
import eu.fukysoft.multymaps.Api.Models.Style;
import eu.fukysoft.multymaps.Api.Models.Themes;
import eu.fukysoft.multymaps.BuildConfig;
import eu.fukysoft.multymaps.Design.DrawOptions;
import eu.fukysoft.multymaps.Design.MapScaleBar;
import eu.fukysoft.multymaps.Design.Menus;
import eu.fukysoft.multymaps.Design.SearchBarGoogle;
import eu.fukysoft.multymaps.DrawUtils.ScreenShot;
import eu.fukysoft.multymaps.Models.GraphicJson;
import eu.fukysoft.multymaps.Models.LibraryItem;
import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.DrawUtils.MySupportMapFragment;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.StaticData.StaticData;
import eu.fukysoft.multymaps.Design.StylerMenu;
import eu.fukysoft.multymaps.Utils.Analitics;
import eu.fukysoft.multymaps.Utils.Dialogs;
import eu.fukysoft.multymaps.Utils.PerzistDataModel;
import eu.fukysoft.multymaps.Utils.Tools;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityGoogleMap extends FragmentActivity implements LocationListener, OnMapReadyCallback, ColorPickerDialogListener {

    public static final String TYPE_TEXT = "TEXT";
    public static final String TYPE_POINT = "POINT";
    public static final String TYPE_LINE = "LINE";
    public static final String TYPE_POLYGON = "POLYGON";

    public static final int COLOR_DIALOG_ID_MAPCUSTOM = 0;
    public static final int COLOR_DIALOG_ID_POLYGON_FILL = 1;
    public static final int COLOR_DIALOG_ID_POLYGON_STROKE = 2;
    public static final int COLOR_DIALOG_ID_MAPCUSTOM_HUE = 3;
    public static final int COLOR_DIALOG_ID_SCREENSHOT = 4;

    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final int OBJECT_LIST_REQUEST_CODE = 2;
    public static final int PREFERENCES_REQUEST_CODE = 3;
    public static final int IMPORT_REQUEST_CODE = 4;

    public static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 0;

    public static Boolean isStylerOpen;
    private GoogleMap googleMapView;

    private ListView leftDrawerListviewGoogle;
    private ListView leftDrawerListviewGoogleTools;
    private ListView leftDrawerListviewGoogleCustom;
    private ListView featureListView;
    private ListView elementListView;
    private ListView rightDrawerLibraryListView;

    private TextView textViewElementMenuFeatureType;
    private TextView textViewElementTypeFeatureType;

    private Button eraseButton;
    private Button eraseFinishButton;

    public String mapType = "";

    private double latitude = 0;
    private double longitude = 0;

    private MaterialDialog saveDialog;
    private MaterialDialog dialogLostData;
    private MaterialDialog aboutDialog;
    private MaterialDialog onlineThemeDialog;
    private MaterialDialog eraseDialog;

    private LocationManager locationManager;

    private Menus menus;
    private DrawOptions drawOptions;
    private SearchBarGoogle searchBarGoogle;
    private StylerMenu stylerMenu;
    private int featurePosition;

    private AdView mAdView;
    private AdapterLibrary rightDrawerLibraryAdapter;

    private MySupportMapFragment mapFragment;
    private ScreenShot sceenshot;

    private MapScaleBar scaleBar;

    public boolean isInScreenShotMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_google_map);

        mapFragment = (MySupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapfragment);
        mapFragment.getMapAsync(this);

        mAdView = (AdView) findViewById(R.id.adView);
        if (BuildConfig.APP_TYPE.equals("free")) {
            MobileAds.initialize(this, getString(R.string.ad_mob_app_id));
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        } else {
            mAdView.setVisibility(View.GONE);
        }

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        StaticData.inits(this);

        menus = new Menus(this);
        searchBarGoogle = new SearchBarGoogle(this, menus);
        menus.createRightDrawer();
        menus.createLeftDrawer();
        menus.createTopDrawer();


        saveDialog = Dialogs.saveMapsDialog(ActivityGoogleMap.this, new Dialogs.DialogMapsSave() {
            @Override
            public void onDimmis() {
                saveDialog.dismiss();
            }

            @Override
            public void onSave(String saveText) {
                Gson gson = new Gson();
                PerzistDataModel.saveMap(ActivityGoogleMap.this, saveText,
                        stylerMenu.jSonString, mapType,
                        gson.toJson(drawOptions.mapObjectList, new TypeToken<ArrayList<MapObject>>() {
                        }.getType()));

                saveDialog.dismiss();
                refreshListSavedMaps();
            }
        });

        scaleBar = findViewById(R.id.scaleBar);

        findViewById(R.id.search_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchBarGoogle.clearText();
                if (menus.topDrawerAction()) {
                    try {
                        Intent intent =
                                new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                        .build(ActivityGoogleMap.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                    } catch (GooglePlayServicesRepairableException e) {
                        // TODO: Handle the error.
                    } catch (GooglePlayServicesNotAvailableException e) {
                        // TODO: Handle the error.
                    }
                }
            }
        });

        findViewById(R.id.location_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawOptions.drawObjectEnd();
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    grantedLocation();

                    LatLng myLocation = new LatLng(latitude, longitude);
                    MarkerOptions locMark = new MarkerOptions();
                    locMark.position(myLocation);
                    locMark.icon(BitmapDescriptorFactory.fromBitmap(((BitmapDrawable) StaticData.markers.get("loc")).getBitmap()));
                    googleMapView.addMarker(locMark);
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(myLocation)      // Sets the center of the map to Mountain View
                            .zoom(17)                   // Sets the zoom
                            .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                            .build();                   // Creates a CameraPosition from the builder
                    googleMapView.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                } else {
                    showGPSDisabledAlertToUser();
                }
            }
        });

        findViewById(R.id.load_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menus.closeObjectList();
                menus.resetLibraryList();
                menus.rightDrawerAction();
            }
        });

        findViewById(R.id.save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDialog.show();
            }
        });

        findViewById(R.id.google_options_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawOptions != null)
                    drawOptions.drawObjectEnd();
                menus.leftDrawerAction();
            }
        });

        findViewById(R.id.screenshot_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sceenshot = new ScreenShot(ActivityGoogleMap.this, googleMapView);
                isInScreenShotMode = true;

            }
        });

        eraseButton = (Button) findViewById(R.id.button_erase);
        eraseFinishButton = (Button) findViewById(R.id.button_finish_erase);

        leftDrawerListviewGoogle = (ListView) findViewById(R.id.left_drawer_listview_google);
        AdapterMenu leftDrawerAdapterGoogle = new AdapterMenu(this, getResources().getStringArray(R.array.menu_google_map_style), StaticData.drawablesGoogle);
        leftDrawerListviewGoogle.setAdapter(leftDrawerAdapterGoogle);

        leftDrawerListviewGoogleTools = (ListView) findViewById(R.id.left_drawer_listview_settings);
        leftDrawerListviewGoogleTools.setAdapter(new AdapterMenu(this, getResources().getStringArray(R.array.menu_google_map_options), StaticData.drawablesGoogleOptions));

        ListView leftDrawerListviewGoogleApp = (ListView) findViewById(R.id.left_drawer_listview_app);
        leftDrawerListviewGoogleApp.setAdapter(new AdapterMenu(this, getResources().getStringArray(R.array.menu_google_app), StaticData.drawablesGoogleApp));
        leftDrawerListviewGoogleApp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {

                    case 0:
                        Intent intent = new Intent(ActivityGoogleMap.this, ActivitySettings.class);
                        startActivityForResult(intent, ActivityGoogleMap.PREFERENCES_REQUEST_CODE);
                        break;
                    case 1:
                        Intent intentTutorial = new Intent(ActivityGoogleMap.this, ActivityTutorial.class);
                        startActivity(intentTutorial);
                        break;
                    case 2:
                        aboutDialog = Dialogs.aboutDialog(ActivityGoogleMap.this, new Dialogs.DialogDimmis() {
                            @Override
                            public void onDimmis() {
                                aboutDialog.dismiss();
                            }
                        });
                        aboutDialog.show();
                        menus.leftDrawerAction();
                        break;

                    case 3:
                        drawOptions.drawObjectEnd();
                        if (!stylerMenu.jSonString.equals("")) {
                            dialogLostData = Dialogs.lostDataDialog(ActivityGoogleMap.this, new Dialogs.DialogLostData() {
                                @Override
                                public void onSave() {
                                    saveDialog.show();
                                    dialogLostData.dismiss();
                                }

                                @Override
                                public void onDontSave() {
                                    dialogLostData.dismiss();
                                    finish();
                                }

                                @Override
                                public void onCancal() {
                                    dialogLostData.dismiss();
                                }
                            });
                            dialogLostData.show();
                        } else {
                            finish();
                        }
                        break;
                }
            }
        });

        leftDrawerListviewGoogleCustom = (ListView) findViewById(R.id.left_drawer_listview_google_custom);
        AdapterMenu leftDrawerAdapterGoogleCustom = new AdapterMenu(this, StaticData.menuGoogleCustom, StaticData.drawablesGoogleCustom);
        leftDrawerListviewGoogleCustom.setAdapter(leftDrawerAdapterGoogleCustom);

        refreshListSavedMaps();

        featureListView = (ListView) findViewById(R.id.feature_listview);
        AdapterEditsItem adapterEditsItemFeature = new AdapterEditsItem(this, StaticData.listItemFeatureType);
        featureListView.setAdapter(adapterEditsItemFeature);

        textViewElementMenuFeatureType = (TextView) findViewById(R.id.text_elementmenu_featuretype);
        textViewElementTypeFeatureType = (TextView) findViewById(R.id.text_elementtype_featuretype);

        elementListView = (ListView) findViewById(R.id.element_listview);
        AdapterEditsItem adapterEditsItemElement = new AdapterEditsItem(this, StaticData.listItemElementType);
        elementListView.setAdapter(adapterEditsItemElement);

        featureListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                textViewElementMenuFeatureType.setText(StaticData.listItemFeatureType.get(position).getDescription());
                menus.closeFeatureList();
                featurePosition = position;


            }
        });

        elementListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                textViewElementTypeFeatureType.setText(StaticData.listItemFeatureType.get(featurePosition).getDescription() + " - "
                        + StaticData.listItemElementType.get(position).getDescription());
                stylerMenu.init(featurePosition, position);
                menus.closeElementList();
            }
        });

        ListView rightDrawerOptionsListviewDown = (ListView) findViewById(R.id.right_drawer_listview_google_objects_options_down);
        AdapterMenu rightDrawerOptionsAdapterDown = new AdapterMenu(this, getResources().getStringArray(R.array.menu_google_map_custom_control), StaticData.drawablesDrawOptionsDown);
        rightDrawerOptionsListviewDown.setAdapter(rightDrawerOptionsAdapterDown);
        rightDrawerOptionsListviewDown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        dialogLostData = Dialogs.lostDataDialog(ActivityGoogleMap.this, new Dialogs.DialogLostData() {
                            @Override
                            public void onSave() {
                                saveDialog.show();
                                dialogLostData.dismiss();
                            }

                            @Override
                            public void onDontSave() {
                                dialogLostData.dismiss();
                                drawOptions.mapObjectList.clear();
                                googleMapView.clear();
                            }

                            @Override
                            public void onCancal() {
                                dialogLostData.dismiss();
                            }
                        });
                        dialogLostData.show();

                        break;
                }
            }
        });

        refreshSettings();
    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        searchBarGoogle.setMap(googleMap);
        googleMapView = googleMap;
        stylerMenu = new StylerMenu(ActivityGoogleMap.this, googleMap);
        drawOptions = new DrawOptions(ActivityGoogleMap.this, googleMapView, menus, mapFragment);


        scaleBar.mColorMode = MapScaleBar.ColorMode.COLOR_MODE_DARK;  // normal
        scaleBar.mColorMode = MapScaleBar.ColorMode.COLOR_MODE_WHITE;  // inverted
        scaleBar.mColorMode = MapScaleBar.ColorMode.COLOR_MODE_AUTO;  // detect automatically after Google Map style
        scaleBar.addTarget(googleMapView);

        loadData();

        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                try {
                    googleMapView.clear();
                    drawOptions.redrawObjects();
                    setLocation(latLng);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        leftDrawerListviewGoogle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (!stylerMenu.jSonString.equals("")) {
                    dialogLostData = Dialogs.lostDataDialog(ActivityGoogleMap.this, new Dialogs.DialogLostData() {
                        public void onSave() {
                            saveDialog.show();
                            dialogLostData.dismiss();
                        }

                        @Override
                        public void onDontSave() {
                            dialogLostData.dismiss();
                            performActionGoogleMapOptions(position);
                        }

                        @Override
                        public void onCancal() {
                            dialogLostData.dismiss();
                        }
                    });
                    dialogLostData.show();
                } else {
                    performActionGoogleMapOptions(position);
                }
                menus.leftDrawerAction();
            }
        });

        leftDrawerListviewGoogleTools.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        Analitics.logEvents(FirebaseAnalytics.Param.ITEM_NAME, "Customize Map");
                        menus.rightDrawerAction();
                        menus.closeLibraryList();
                        menus.closeObjectList();
                        break;
                    case 1:
                        Analitics.logEvents(FirebaseAnalytics.Param.ITEM_NAME, "Draw Objects");
                        menus.rightDrawerAction();
                        menus.resetObjectList();
                        menus.closeLibraryList();
                        break;
                    case 2:
                        Intent intentie = new Intent(ActivityGoogleMap.this, ActivityExport.class);
                        intentie.putExtra(ActivityExport.KEY_OBJECT_LIST, drawOptions.mapObjectList);
                        intentie.putExtra(ActivityExport.KEY_STYLE_JSON, stylerMenu.jSonString);
                        startActivityForResult(intentie, ActivityGoogleMap.IMPORT_REQUEST_CODE);
                        break;
                }
            }

        });

        leftDrawerListviewGoogleCustom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                try {
                    googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    ActivityGoogleMap.this, StaticData.RAW_ID.get(position)));
                } catch (Resources.NotFoundException e) {
                }

                stylerMenu.setGraphicJsons(Tools.getJsonFromRawResources(
                        getResources().openRawResource(
                                StaticData.RAW_ID.get(position))));
                menus.rightDrawerAction();

            }
        });

        ListView moreOptionsListview = (ListView) findViewById(R.id.right_drawer_listview_google_more_options);
        AdapterMenu adapterMenuMoreOptions = new AdapterMenu(this, getResources().getStringArray(R.array.menu_google_map_cutom_options), StaticData.drawablesGoogleOptionsMenu);
        moreOptionsListview.setAdapter(adapterMenuMoreOptions);
        moreOptionsListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        menus.closeCustomList();
                        break;
                    case 1:
                        onlineThemeDialog = onlineThemeDialog(ActivityGoogleMap.this, new Dialogs.DialogTheme() {
                            @Override
                            public void onDimmis() {
                                onlineThemeDialog.dismiss();
                            }

                            @Override
                            public void onSubmit(String json) {
                                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


                                MapStyleOptions options = new MapStyleOptions(json);

                                googleMap.setMapStyle(options);
                                stylerMenu.setGraphicJsons(Tools.getJsonFromString(json));

                                onlineThemeDialog.dismiss();
                            }
                        });
                        onlineThemeDialog.show();
                        menus.rightDrawerAction();
                        break;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isInScreenShotMode) {
            sceenshot.rootLayout.setVisibility(View.GONE);
            sceenshot.rootLayout.animate().setDuration(1000).translationY(Tools.getDisplayDimension(this)[0]);
            isInScreenShotMode = false;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);

        } else {
            grantedLocation();
        }
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityGoogleMap.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(ActivityGoogleMap.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions(ActivityGoogleMap.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    public void grantedLocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    grantedLocation();
                } else {
                    finish();
                }
                return;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void showGPSDisabledAlertToUser() {
        new MaterialDialog.Builder(ActivityGoogleMap.this)
                .title("GPS je vypnuto.")
                .titleColor(ContextCompat.getColor(this, R.color.colorPrimaryOrange))
                .content("V nastavení zapněte GPS.")
                .contentColor(ContextCompat.getColor(this, R.color.colorAccent))
                .negativeText("Ukončit")
                .backgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                .positiveText("Nastavení")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        Intent callGPSSettingIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                })
                .show();
    }

    @Override
    public void onColorSelected(int dialogId, @ColorInt int color) {
        switch (dialogId) {
            case COLOR_DIALOG_ID_MAPCUSTOM:
                stylerMenu.onColorSelected(color);
                break;
            case COLOR_DIALOG_ID_MAPCUSTOM_HUE:
                stylerMenu.onHueSelected(color);
                break;
            case COLOR_DIALOG_ID_POLYGON_FILL:
                drawOptions.colorFill.setFromInt(color);
                drawOptions.colorFillView.setBackgroundColor(color);

                drawOptions.drawObjectToMap();
                break;
            case COLOR_DIALOG_ID_POLYGON_STROKE:
                drawOptions.colorStroke.setFromInt(color);
                drawOptions.colorStrokeView.setBackgroundColor(color);
                drawOptions.drawObjectToMap();
                break;
            case COLOR_DIALOG_ID_SCREENSHOT:
                sceenshot.setColor(color);
                break;
        }
    }

    @Override
    public void onDialogDismissed(int dialogId) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                LatLng latLng = place.getLatLng();
                LatLng coordinates = place.getLatLng();
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(
                            coordinates.latitude,
                            coordinates.longitude,
                            1);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                LatLng myLocation = new LatLng(latLng.latitude, latLng.longitude);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(myLocation)      // Sets the center of the map to Mountain View
                        .zoom(17)                   // Sets the zoom
                        .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                googleMapView.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                searchBarGoogle.setAtributesFromMap(latLng.latitude, latLng.longitude, addresses);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.

            } else if (resultCode == RESULT_CANCELED) {

            }
        }
        if (requestCode == OBJECT_LIST_REQUEST_CODE) {

            switch (resultCode) {

                case ActivityObjectList.RESULT_CODE_SAVE_OBJECTS:
                    drawOptions.mapObjectList = (ArrayList<MapObject>) data.getExtras().getSerializable(ActivityObjectList.KEY_OBJECT_LIST);
                    if (drawOptions.mapObjectList != null) drawOptions.redrawObjects();
                    break;

                case ActivityObjectList.RESULT_CODE_LOCATE_OBJECTS:
                    MapObject mapObject = (MapObject) data.getExtras().getSerializable(ActivityObjectList.KEY_OBJECT);
                    drawOptions.mapObjectList = (ArrayList<MapObject>) data.getExtras().getSerializable(ActivityObjectList.KEY_OBJECT_LIST);
                    if (drawOptions.mapObjectList != null) drawOptions.redrawObjects();
                    menus.rightDrawerAction();
                    googleMapView.animateCamera(Tools.getZoomOfObject(mapObject, ActivityGoogleMap.this, menus.topBarSize));
                    break;

                case ActivityObjectList.RESULT_CODE_EDIT_OBJECTS:
                    int positionOfEditObject = data.getExtras().getInt(ActivityObjectList.KEY_OBJECT_POSITION);
                    drawOptions.mapObjectList = (ArrayList<MapObject>) data.getExtras().getSerializable(ActivityObjectList.KEY_OBJECT_LIST);
                    menus.rightDrawerAction();
                    googleMapView.animateCamera(Tools.getZoomOfObject(drawOptions.mapObjectList.get(positionOfEditObject), ActivityGoogleMap.this, menus.topBarSize));
                    drawOptions.editObject(positionOfEditObject);
                    break;
            }
        }

        if (requestCode == PREFERENCES_REQUEST_CODE) {
            refreshSettings();
        }

        if (requestCode == IMPORT_REQUEST_CODE) {
            switch (resultCode) {
                case ActivityExport.RESULT_CODE_SET_STYLER:
                    ArrayList<GraphicJson> graphicJsons = (ArrayList<GraphicJson>) data.getExtras().getSerializable(ActivityExport.KEY_STYLE_JSON);
                    if (graphicJsons != null) {
                        stylerMenu.setGraphicJsons(graphicJsons);
                        googleMapView.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        MapStyleOptions options = new MapStyleOptions(stylerMenu.jSonString);
                        googleMapView.setMapStyle(options);
                    }
                    break;

                case ActivityExport.RESULT_CODE_SET_OBJECTS:
                    ArrayList<MapObject> mapObjects = (ArrayList<MapObject>) data.getExtras().getSerializable(ActivityExport.KEY_OBJECT_LIST);
                    if (mapObjects != null) {
                        drawOptions.mapObjectList = mapObjects;
                        drawOptions.redrawObjects();
                    }
                    break;

                case ActivityExport.RESULT_CODE_SET_MAPSTUDIO:
                    ArrayList<GraphicJson> graphicJsonsMapStudio = (ArrayList<GraphicJson>) data.getExtras().getSerializable(ActivityExport.KEY_STYLE_JSON);
                    ArrayList<MapObject> mapObjectsMapStudio = (ArrayList<MapObject>) data.getExtras().getSerializable(ActivityExport.KEY_OBJECT_LIST);
                    setDataMapStudioFormat(graphicJsonsMapStudio, mapObjectsMapStudio);
                    break;
            }
        }

    }

    private void setDataMapStudioFormat(ArrayList<GraphicJson> graphicJsonsMapStudio, ArrayList<MapObject> mapObjectsMapStudio) {
        if (graphicJsonsMapStudio != null) {
            stylerMenu.setGraphicJsons(graphicJsonsMapStudio);
            googleMapView.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            MapStyleOptions options = new MapStyleOptions(stylerMenu.jSonString);
            googleMapView.setMapStyle(options);
        }
        if (mapObjectsMapStudio != null) {
            drawOptions.mapObjectList = mapObjectsMapStudio;
            drawOptions.redrawObjects();
        }
    }

    private void saveData() {

        double lat = 0, lon = 0;
        float zoom = 0, titl = 0, bearing = 0;
        if (googleMapView != null && PerzistDataModel.isAutoSavePosition(this)) {
            CameraPosition cameraPosition = googleMapView.getCameraPosition();
            if (cameraPosition != null) {
                lat = googleMapView.getCameraPosition().target.latitude;
                lon = googleMapView.getCameraPosition().target.longitude;
                zoom = googleMapView.getCameraPosition().zoom;
                titl = googleMapView.getCameraPosition().tilt;
                bearing = googleMapView.getCameraPosition().bearing;
            }
        }

        Gson gson = new Gson();

        String jsnoObjects = "";
        if (drawOptions != null && PerzistDataModel.isAutoSaveStyle(this)) {
            jsnoObjects = gson.toJson(drawOptions.mapObjectList, new TypeToken<ArrayList<MapObject>>() {
            }.getType());
        }

        String jsonStyle = "";
        if (stylerMenu != null && PerzistDataModel.isAutoSaveObjects(this)) {
            jsonStyle = stylerMenu.jSonString;
        }
        PerzistDataModel.AutoSaveMap(ActivityGoogleMap.this, jsonStyle, jsnoObjects, mapType, lat, lon, zoom, titl, bearing);
    }

    private void loadData() {
        Gson gson = new Gson();
        String[] result = PerzistDataModel.AutoLoadMap(ActivityGoogleMap.this);

        if (!result[0].equals("")) {
            setMapType(result[0]);
        } else {
            if (!result[1].equals(""))
                stylerMenu.putJsonToGoogleMap(result[1]);
            ArrayList<GraphicJson> graphicJsons = gson.fromJson(result[1], new TypeToken<ArrayList<GraphicJson>>() {
            }.getType());
            if (graphicJsons != null)
                stylerMenu.setGraphicJsons(graphicJsons);
        }
        if (!result[2].equals(""))
            drawOptions.setObjectsFromJson(result[2]);

        if (!result[3].equals("0") && !result[4].equals("0")) {
            CameraPosition cameraPosition = new CameraPosition(
                    new LatLng(Double.parseDouble(result[3]), Double.parseDouble(result[4])),
                    Float.parseFloat(result[6]),
                    Float.parseFloat(result[5]),
                    Float.parseFloat(result[7]));
            googleMapView.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private void refreshSettings() {
        if (PerzistDataModel.isScaleShow(this)) {
            scaleBar.setVisibility(View.VISIBLE);
        } else {
            scaleBar.setVisibility(View.GONE);
        }
    }

    private void setLocation(LatLng location) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        menus.topDrawerAction();
        searchBarGoogle.setAtributesFromMap(location.latitude, location.longitude, addresses);

    }

    public void refreshListSavedMaps() {
        int savedCounter = PerzistDataModel.getSavedMapsCounter(ActivityGoogleMap.this);
        LinearLayout layoutEmptySavedMaps = (LinearLayout) findViewById(R.id.layout_empty_save_map);

        if (savedCounter == 0) {

        } else {

            final ArrayList<LibraryItem> savedNames = new ArrayList<>();
            for (int i = 0; i < savedCounter; i++) {
                savedNames.add(new LibraryItem(PerzistDataModel.getSavedMapName(ActivityGoogleMap.this, i), false));
            }


            rightDrawerLibraryListView = (ListView) findViewById(R.id.right_drawer_listview_google_library);
            layoutEmptySavedMaps.setVisibility(View.GONE);
            rightDrawerLibraryListView.setVisibility(View.VISIBLE);

            rightDrawerLibraryAdapter = new AdapterLibrary(this, savedNames, null, false);
            rightDrawerLibraryListView.setAdapter(rightDrawerLibraryAdapter);

            rightDrawerLibraryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Gson gson = new Gson();
                    String jsonStyle = PerzistDataModel.getSavedMapStyleJson(ActivityGoogleMap.this, position);
                    if (jsonStyle != null && !jsonStyle.equals(""))
                        stylerMenu.putJsonToGoogleMap(jsonStyle);
                    ArrayList<GraphicJson> graphicJsons = (ArrayList<GraphicJson>) gson.fromJson(PerzistDataModel.getSavedMapStyleJson(ActivityGoogleMap.this, position), new TypeToken<ArrayList<GraphicJson>>() {
                    }.getType());

                    String mapType = PerzistDataModel.getSavedMapType(ActivityGoogleMap.this, position);

                    if (graphicJsons != null && mapType.equals(""))
                        stylerMenu.setGraphicJsons(graphicJsons);

                    if (!mapType.equals("")) {
                        setMapType(mapType);
                    }

                    drawOptions.setObjectsFromJson(PerzistDataModel.getSavedMapObjectJson(ActivityGoogleMap.this, position));
                    menus.rightDrawerAction();
                }
            });

            eraseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eraseFinishButton.setVisibility(View.VISIBLE);
                    eraseButton.setVisibility(View.GONE);
                    rightDrawerLibraryAdapter.setListErase();
                }
            });

            eraseFinishButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final ArrayList<Integer> removeLibIds = new ArrayList<>();
                    for (int s = 0; s < savedNames.size(); s++) {
                        if (savedNames.get(s).isChecked()) {
                            removeLibIds.add(s);
                        }
                    }

                    if (removeLibIds.size() > 0) {
                        eraseDialog = Dialogs.sureEraseDialog(ActivityGoogleMap.this, removeLibIds.size(), new Dialogs.DialogSure() {
                            @Override
                            public void onDimmis() {

                                eraseButton.setVisibility(View.VISIBLE);
                                eraseFinishButton.setVisibility(View.GONE);
                                rightDrawerLibraryAdapter.setLibrary();
                                eraseDialog.dismiss();

                            }

                            @Override
                            public void onSubmit() {
                                savedNames.clear();
                                savedNames.addAll(PerzistDataModel.refreshSavedMaps(ActivityGoogleMap.this, removeLibIds));

                                rightDrawerLibraryAdapter = new AdapterLibrary(ActivityGoogleMap.this, savedNames, null, false);
                                rightDrawerLibraryListView.setAdapter(rightDrawerLibraryAdapter);

                                eraseButton.setVisibility(View.VISIBLE);
                                eraseFinishButton.setVisibility(View.GONE);
                                rightDrawerLibraryAdapter.setLibrary();
                                eraseDialog.dismiss();

                            }
                        });

                        eraseDialog.show();
                    } else {
                        eraseButton.setVisibility(View.VISIBLE);
                        eraseFinishButton.setVisibility(View.GONE);
                        rightDrawerLibraryAdapter.setLibrary();
                    }

                }
            });
        }
    }

    private void performActionGoogleMapOptions(int position) {
        switch (position) {
            case 0:
                googleMapView.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                try {
                    googleMapView.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    ActivityGoogleMap.this, R.raw.google_normal_style));
                } catch (Resources.NotFoundException e) {
                }
                mapType = "";
                stylerMenu.resetGraphicJson();
                break;
            case 1:
                setMapType("SATELLITE");
                break;
            case 2:
                setMapType("TERRAIN");
                break;
            case 3:
                setMapType("HYBRID");
                break;

        }
    }

    private void setMapType(String type) {
        mapType = type;
        switch (type) {
            case "SATELLITE":
                googleMapView.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                stylerMenu.resetGraphicJson();
                break;
            case "TERRAIN":
                googleMapView.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                stylerMenu.resetGraphicJson();
                break;
            case "HYBRID":
                googleMapView.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                stylerMenu.resetGraphicJson();
                break;
        }
    }


    private MaterialDialog onlineThemeDialog(final Context context, final Dialogs.DialogTheme listener) {
        View dialogView = LayoutInflater.from(context).inflate(R.layout.layout_online_theme_dialog, null);

        final LinearLayout progressLayout = (LinearLayout) dialogView.findViewById(R.id.layout_progress_online_theme);
        final LinearLayout listLayout = (LinearLayout) dialogView.findViewById(R.id.layout_list_online_theme);
        final LinearLayout errorLayout = (LinearLayout) dialogView.findViewById(R.id.layout_error_online_theme);
        final ListView themeListView = (ListView) dialogView.findViewById(R.id.listview_online_theme);

        dialogView.findViewById(R.id.button_dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDimmis();
            }
        });

        ApiClient.getClient(MapApi.BASE_URL).create(MapApi.class).getThemes().enqueue(new Callback<Themes>() {
            @Override
            public void onResponse(Call<Themes> call, final Response<Themes> response) {
                if (response.isSuccessful()) {
                    progressLayout.setVisibility(View.GONE);
                    listLayout.setVisibility(View.VISIBLE);
                    themeListView.setAdapter(new AdapterOnlineTheme(context, response.body().getThemes()));
                    themeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            progressLayout.setVisibility(View.VISIBLE);
                            listLayout.setVisibility(View.GONE);
                            ApiClient.getClient(MapApi.BASE_URL).create(MapApi.class).getTheme(response.body().getThemes().get(position).getId()).enqueue(new Callback<Style>() {
                                @Override
                                public void onResponse(Call<Style> call, Response<Style> response) {
                                    if (response.isSuccessful()) {
                                        if (listener != null)
                                            listener.onSubmit(response.body().getStyle());
                                    } else {
                                        progressLayout.setVisibility(View.GONE);
                                        errorLayout.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onFailure(Call<Style> call, Throwable t) {
                                    progressLayout.setVisibility(View.GONE);
                                    errorLayout.setVisibility(View.VISIBLE);
                                }
                            });

                        }
                    });
                } else {
                    progressLayout.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Themes> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });
        return new MaterialDialog.Builder(context).customView(dialogView, false).build();
    }

    public void onMarker(View v) {
        drawOptions.setMarker(v.getTag().toString());

    }

    public void onNothing(View v) {

    }

}
