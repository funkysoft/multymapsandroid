package eu.fukysoft.multymaps.Activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.firebase.analytics.FirebaseAnalytics;

import eu.fukysoft.multymaps.Adpaters.AdapterIcons;
import eu.fukysoft.multymaps.Adpaters.AdapterMenu;
import eu.fukysoft.multymaps.Design.NonScrollListView;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.StaticData.StaticData;
import eu.fukysoft.multymaps.Utils.Analitics;

public class ActivityTutorial extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        Analitics.logEvents(FirebaseAnalytics.Param.ITEM_NAME,this.getClass().getSimpleName());

        ListView mainFuncList = findViewById(R.id.list_main_function);
        mainFuncList.setAdapter(new AdapterIcons(this,
                getResources().getStringArray(R.array.tutorial_main_functions),
                StaticData.drawablesTutorialMainAction,true));

        NonScrollListView ationBarList = findViewById(R.id.list_actionbar_icon);
        ationBarList.setAdapter(new AdapterIcons(this,
                getResources().getStringArray(R.array.tutorial_action_icon),
                StaticData.drawablesTutorialActionbar,false));

        NonScrollListView ationBarScreenList = findViewById(R.id.list_actionbar_screenshot_icon);
        ationBarScreenList.setAdapter(new AdapterIcons(this,
                getResources().getStringArray(R.array.tutorial_action_screen_icon),
                StaticData.drawablesTutorialActionScreenbar,false));

        NonScrollListView ationBarScreenEditList = findViewById(R.id.list_actionbar_edit_screenshot_icon);
        ationBarScreenEditList.setAdapter(new AdapterIcons(this,
                getResources().getStringArray(R.array.tutorial_action_edit_screen_icon),
                StaticData.drawablesTutorialActionEditScreenbar,false));

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
