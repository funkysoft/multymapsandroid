package eu.fukysoft.multymaps.Activitys;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import eu.fukysoft.multymaps.Adpaters.AdapterPageList;
import eu.fukysoft.multymaps.Fragments.FragmentList;
import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.Utils.Analitics;
import eu.fukysoft.multymaps.Interfaces.OnObjectOptionsButtonsClick;
import eu.fukysoft.multymaps.R;

/**
 * Created by Funky on 8/25/2017.
 */

public class ActivityObjectList extends FragmentActivity implements OnObjectOptionsButtonsClick{

    public static final int RESULT_CODE_SAVE_OBJECTS = 91;
    public static final int RESULT_CODE_EDIT_OBJECTS = 92;
    public static final int RESULT_CODE_LOCATE_OBJECTS = 93;

    public static final String KEY_OBJECT = "OBJECT";
    public static final String KEY_OBJECT_LIST = "OBJECT_LIST";
    public static final String KEY_OBJECT_POSITION = "OBJECT_POSITION";
    public static final String KEY_TYPE = "TYPE";

    private AdapterPageList adapterPageList;
    private ViewPager mViewPager;

    private LinearLayout tabTexts;
    private LinearLayout tabPoints;
    private LinearLayout tabPolylines;
    private LinearLayout tabPolygons;

    private List<FragmentList> fragmentLists;

    @Override
    public void onBackPressed() {
        ArrayList<MapObject> mapObjects = new ArrayList<>();

        for(int f = 0; f < fragmentLists.size(); f++){
            if(fragmentLists.get(f).objectList != null)
            mapObjects.addAll(fragmentLists.get(f).objectList);
        }
        Bundle data = new Bundle();
        data.putSerializable(KEY_OBJECT_LIST,mapObjects);
        Intent intent = new Intent();
        intent.putExtras(data);
        setResult(RESULT_CODE_SAVE_OBJECTS,intent);
        finish();

    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Analitics.logEvents(FirebaseAnalytics.Param.ITEM_NAME,this.getClass().getSimpleName());

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_object_list);

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ArrayList<MapObject> mapObjects = (ArrayList<MapObject>) getIntent().getSerializableExtra(KEY_OBJECT_LIST);
        fragmentLists = new ArrayList<>();
        for(int i = 0; i < 4; i++){
            FragmentList fragmentList = new FragmentList();
            Bundle bundle = new Bundle();
            switch (i){
                case 0:
                    bundle.putString(KEY_TYPE,ActivityGoogleMap.TYPE_TEXT);
                    bundle.putSerializable(KEY_OBJECT_LIST,getListOfType(mapObjects,ActivityGoogleMap.TYPE_TEXT));
                    break;
                case 1:
                    bundle.putString(KEY_TYPE,ActivityGoogleMap.TYPE_POINT);
                    bundle.putSerializable(KEY_OBJECT_LIST,getListOfType(mapObjects,ActivityGoogleMap.TYPE_POINT));
                    break;
                case 2:
                    bundle.putString(KEY_TYPE,ActivityGoogleMap.TYPE_LINE);
                    bundle.putSerializable(KEY_OBJECT_LIST,getListOfType(mapObjects,ActivityGoogleMap.TYPE_LINE));
                    break;
                case 3:
                    bundle.putString(KEY_TYPE,ActivityGoogleMap.TYPE_POLYGON);
                    bundle.putSerializable(KEY_OBJECT_LIST,getListOfType(mapObjects,ActivityGoogleMap.TYPE_POLYGON));

                    break;
            }
            fragmentList.setArguments(bundle);
            fragmentLists.add(fragmentList);
        }

        tabTexts = (LinearLayout) findViewById(R.id.tab_texts);
        tabPoints = (LinearLayout) findViewById(R.id.tab_points);
        tabPolylines = (LinearLayout) findViewById(R.id.tab_polylines);
        tabPolygons = (LinearLayout) findViewById(R.id.tab_polygons);


        adapterPageList = new AdapterPageList(getSupportFragmentManager(),fragmentLists);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(adapterPageList);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                resetTabs();
                switch (position){
                    case 0:
                        tabTexts.setBackground(ContextCompat.getDrawable(ActivityObjectList.this,R.drawable.background_tab_select));
                        break;
                    case 1:
                        tabPoints.setBackground(ContextCompat.getDrawable(ActivityObjectList.this,R.drawable.background_tab_select));
                        break;
                    case 2:
                        tabPolylines.setBackground(ContextCompat.getDrawable(ActivityObjectList.this,R.drawable.background_tab_select));
                        break;
                    case 3:
                        tabPolygons.setBackgroundColor(ContextCompat.getColor(ActivityObjectList.this,R.color.colorPrimaryLight));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabTexts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTabs();
                mViewPager.setCurrentItem(0);
                tabTexts.setBackground(ContextCompat.getDrawable(ActivityObjectList.this,R.drawable.background_tab_select));
            }
        });
        tabPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTabs();
                mViewPager.setCurrentItem(1);
                tabPoints.setBackground(ContextCompat.getDrawable(ActivityObjectList.this,R.drawable.background_tab_select));
            }
        });
        tabPolylines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTabs();
                mViewPager.setCurrentItem(2);
                tabPolylines.setBackground(ContextCompat.getDrawable(ActivityObjectList.this,R.drawable.background_tab_select));
            }
        });
        tabPolygons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTabs();
                mViewPager.setCurrentItem(3);
                tabPolygons.setBackgroundColor(ContextCompat.getColor(ActivityObjectList.this,R.color.colorPrimaryLight));
            }
        });
    }

    private void resetTabs(){
        tabTexts.setBackground(ContextCompat.getDrawable(ActivityObjectList.this,R.drawable.background_tab));
        tabPoints.setBackground(ContextCompat.getDrawable(ActivityObjectList.this,R.drawable.background_tab));
        tabPolylines.setBackground(ContextCompat.getDrawable(ActivityObjectList.this,R.drawable.background_tab));
        tabPolygons.setBackgroundColor(ContextCompat.getColor(ActivityObjectList.this,R.color.colorPrimaryDark));
    }

    private ArrayList<MapObject> getListOfType(ArrayList<MapObject> objectList,String type){
        ArrayList<MapObject> tempList = new ArrayList<>();

        for(int i = 0; i < objectList.size(); i++){
            if(objectList.get(i).type.equals(type)) tempList.add(objectList.get(i));
        }

        return tempList;
    }

    @Override
    public void onLocateObject(MapObject mapObject) {
        ArrayList<MapObject> mapObjects = new ArrayList<>();

        for(int f = 0; f < fragmentLists.size(); f++){
            if(fragmentLists.get(f).objectList != null)
                mapObjects.addAll(fragmentLists.get(f).objectList);
        }

        Bundle data = new Bundle();
        data.putSerializable(KEY_OBJECT_LIST,mapObjects);
        data.putSerializable(KEY_OBJECT,mapObject);
        Intent intent = new Intent();
        intent.putExtras(data);
        setResult(RESULT_CODE_LOCATE_OBJECTS,intent);
        finish();
    }

    @Override
    public void onEditObject(int id, int type) {
        ArrayList<MapObject> mapObjects = new ArrayList<>();

        int positionOfEditObject = -1;
        int countPosition = 0;
        for(int f = 0; f < fragmentLists.size(); f++){
            if(fragmentLists.get(f).objectList != null) {
            if(f == type){
                positionOfEditObject = countPosition + id;
            }
                countPosition += fragmentLists.get(f).objectList.size();
                mapObjects.addAll(fragmentLists.get(f).objectList);

            }
        }

        Bundle data = new Bundle();
        data.putSerializable(KEY_OBJECT_LIST,mapObjects);
        data.putInt(KEY_OBJECT_POSITION ,positionOfEditObject);
        Intent intent = new Intent();
        intent.putExtras(data);
        setResult(RESULT_CODE_EDIT_OBJECTS,intent);
        finish();
    }
}

