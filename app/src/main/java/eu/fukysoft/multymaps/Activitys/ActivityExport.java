package eu.fukysoft.multymaps.Activitys;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import eu.fukysoft.multymaps.Adpaters.AdapterPageExIm;
import eu.fukysoft.multymaps.Interfaces.ChangeJsonState;
import eu.fukysoft.multymaps.Fragments.FragmentExImMapStudio;
import eu.fukysoft.multymaps.Fragments.FragmentExImObjects;
import eu.fukysoft.multymaps.Fragments.FragmentExImStyle;
import eu.fukysoft.multymaps.Models.GeoJson;
import eu.fukysoft.multymaps.Models.GraphicJson;
import eu.fukysoft.multymaps.Models.MapObject;
import eu.fukysoft.multymaps.Models.MapStudioJson;
import eu.fukysoft.multymaps.R;
import eu.fukysoft.multymaps.Utils.Analitics;
import eu.fukysoft.multymaps.Utils.Dialogs;
import eu.fukysoft.multymaps.Utils.Tools;

public class ActivityExport extends FragmentActivity implements ChangeJsonState {

    public static final String KEY_OBJECT_LIST = "OBJECT_LIST";
    public static final String KEY_STYLE_JSON = "STYLE_JSON";
    public static final String KEY_IS_GEOJSON = "IS_GEOJSON";
    private static final String LABEL_FOR_CLIP = "MAP_STUDIO_JSON";
    public static final String FOLDER_NAME = "MapStudio";

    public static final int RESULT_CODE_SET_STYLER = 91;
    public static final int RESULT_CODE_SET_OBJECTS = 92;
    public static final int RESULT_CODE_SET_MAPSTUDIO = 93;

    private static final int PICKFILE_RESULT_CODE = 1;

    private AdapterPageExIm adapterPageExIm;
    private ViewPager mViewPager;

    private LinearLayout tabTexts;
    private LinearLayout tabPoints;
    private LinearLayout tabPolylines;
    private LinearLayout tabPolygons;

    private Button buttonCopyAll;
    private Button buttonExportToFile;
    private Button buttonImport;
    private Button buttonImportFromFile;

    private FragmentExImStyle fragmentExImStyle;
    private FragmentExImObjects fragmentExImObjects;
    private FragmentExImObjects fragmentExImGeoJson;
    private FragmentExImMapStudio fragmentExImMapStudio;

    private int currentFragment = 0;

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export);

        Analitics.logEvents(FirebaseAnalytics.Param.ITEM_NAME, this.getClass().getSimpleName());

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String styleJson = getIntent().getStringExtra(KEY_STYLE_JSON);
        ArrayList<MapObject> mapObjects = (ArrayList<MapObject>) getIntent().getSerializableExtra(KEY_OBJECT_LIST);

        fragmentExImStyle = new FragmentExImStyle();
        Bundle bundleStyle = new Bundle();
        bundleStyle.putString(KEY_STYLE_JSON, styleJson);
        fragmentExImStyle.setArguments(bundleStyle);

        fragmentExImObjects = new FragmentExImObjects();
        Bundle bundleObjects = new Bundle();
        bundleObjects.putSerializable(KEY_OBJECT_LIST, mapObjects);
        bundleObjects.putBoolean(KEY_IS_GEOJSON, false);
        fragmentExImObjects.setArguments(bundleObjects);

        fragmentExImGeoJson = new FragmentExImObjects();
        Bundle bundleGeoJson = new Bundle();
        bundleGeoJson.putSerializable(KEY_OBJECT_LIST, mapObjects);
        bundleGeoJson.putBoolean(KEY_IS_GEOJSON, true);
        fragmentExImGeoJson.setArguments(bundleGeoJson);

        fragmentExImMapStudio = new FragmentExImMapStudio();
        Bundle bundleMapStudio = new Bundle();
        bundleMapStudio.putSerializable(KEY_OBJECT_LIST, mapObjects);
        bundleMapStudio.putString(KEY_STYLE_JSON, styleJson);
        fragmentExImMapStudio.setArguments(bundleMapStudio);

        buttonCopyAll = (Button) findViewById(R.id.button_copy_all);
        buttonExportToFile = (Button) findViewById(R.id.button_export_to_file);
        buttonImport = (Button) findViewById(R.id.button_import);
        buttonImportFromFile = (Button) findViewById(R.id.button_import_from_file);

        disableButtons();

        buttonCopyAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyToClipBoard();
            }
        });

        buttonExportToFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportToFile();
            }
        });

        buttonImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importFromClipBoard();
            }
        });

        buttonImportFromFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivityForResult(intent, PICKFILE_RESULT_CODE);
            }
        });

        tabTexts = (LinearLayout) findViewById(R.id.tab_texts);
        tabPoints = (LinearLayout) findViewById(R.id.tab_points);
        tabPolylines = (LinearLayout) findViewById(R.id.tab_polylines);
        tabPolygons = (LinearLayout) findViewById(R.id.tab_polygons);

        adapterPageExIm = new AdapterPageExIm(getSupportFragmentManager(),
                fragmentExImStyle,
                fragmentExImObjects,
                fragmentExImGeoJson,
                fragmentExImMapStudio);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(adapterPageExIm);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                resetTabs();
                switch (position) {
                    case 0:
                        setProperitiesFragmentStyle();
                        tabTexts.setBackground(ContextCompat.getDrawable(ActivityExport.this, R.drawable.background_tab_select));
                        break;
                    case 1:
                        setProperitiesFragmentObject();
                        tabPoints.setBackground(ContextCompat.getDrawable(ActivityExport.this, R.drawable.background_tab_select));
                        break;
                    case 2:
                        setProperitiesFragmentGeoJson();
                        tabPolylines.setBackground(ContextCompat.getDrawable(ActivityExport.this, R.drawable.background_tab_select));
                        break;
                    case 3:
                        setProperitiesFragmentMapStudio();
                        tabPolygons.setBackgroundColor(ContextCompat.getColor(ActivityExport.this, R.color.colorPrimaryLight));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabTexts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTabs();
                mViewPager.setCurrentItem(0);
                tabTexts.setBackground(ContextCompat.getDrawable(ActivityExport.this, R.drawable.background_tab_select));
                setProperitiesFragmentStyle();
            }
        });
        tabPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTabs();
                mViewPager.setCurrentItem(1);
                tabPoints.setBackground(ContextCompat.getDrawable(ActivityExport.this, R.drawable.background_tab_select));
                setProperitiesFragmentObject();
            }
        });
        tabPolylines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTabs();
                mViewPager.setCurrentItem(2);
                tabPolylines.setBackground(ContextCompat.getDrawable(ActivityExport.this, R.drawable.background_tab_select));
                setProperitiesFragmentGeoJson();
            }
        });
        tabPolygons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTabs();
                mViewPager.setCurrentItem(3);
                tabPolygons.setBackgroundColor(ContextCompat.getColor(ActivityExport.this, R.color.colorPrimaryLight));
                setProperitiesFragmentMapStudio();
            }
        });
    }

    private void resetTabs() {
        tabTexts.setBackground(ContextCompat.getDrawable(ActivityExport.this, R.drawable.background_tab));
        tabPoints.setBackground(ContextCompat.getDrawable(ActivityExport.this, R.drawable.background_tab));
        tabPolylines.setBackground(ContextCompat.getDrawable(ActivityExport.this, R.drawable.background_tab));
        tabPolygons.setBackgroundColor(ContextCompat.getColor(ActivityExport.this, R.color.colorPrimaryDark));
    }

    private void setProperitiesFragmentStyle() {
        currentFragment = 0;
        if (fragmentExImStyle.isJsonVisible()) {
            enableButtons();
        } else {
            disableButtons();
        }
    }

    private void setProperitiesFragmentObject() {
        currentFragment = 1;
        if (fragmentExImObjects.isJsonVisible()) {
            enableButtons();
        } else {
            disableButtons();
        }
    }

    private void setProperitiesFragmentGeoJson() {
        currentFragment = 2;
        if (fragmentExImGeoJson.isJsonVisible()) {
            enableButtons();
        } else {
            disableButtons();
        }
    }

    private void setProperitiesFragmentMapStudio() {
        currentFragment = 3;
        if (fragmentExImMapStudio.isJsonVisible()) {
            enableButtons();
        } else {
            disableButtons();
        }
    }

    private void disableButtons() {
        buttonCopyAll.setEnabled(false);
        buttonExportToFile.setEnabled(false);
    }

    private void enableButtons() {
        buttonCopyAll.setEnabled(true);
        buttonExportToFile.setEnabled(true);
    }

    private void copyToClipBoard() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        String copy = getJsonOfCurrentFragment();
        Toast.makeText(ActivityExport.this, getString(R.string.json_has_been_copied), Toast.LENGTH_SHORT).show();
        ClipData clip = ClipData.newPlainText(LABEL_FOR_CLIP, copy);
        clipboard.setPrimaryClip(clip);
    }

    private void exportToFile() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = df.format(Calendar.getInstance().getTime());

        File folder = new File(Environment.getExternalStorageDirectory(), FOLDER_NAME);

        if (!folder.exists()) {
            if (folder.mkdirs()) {
            }
        }

        String fileEx = ".json";

        if (currentFragment == 2) {
            if (fragmentExImGeoJson.getExtension() == 2)
                fileEx = ".geojson";
        }


        File file = new File(folder, date + fileEx);

        try {
            FileWriter fw = new FileWriter(file);
            fw.write(getJsonOfCurrentFragment());
            fw.close();
            Toast.makeText(ActivityExport.this, getString(R.string.file_has_been_saved) + file.getPath(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void importFromClipBoard() {
        Dialogs.importDialog(ActivityExport.this, new Dialogs.DialogSave() {
            @Override
            public void onSave(String text) {
                if (text.equals("") || text == null) {
                    Dialogs.messageDialog(ActivityExport.this, getString(
                            R.string.error),
                            getString(R.string.empty_string)).show();
                } else {
                    importFromString(text);
                }
            }
        }).show();
    }

    private String getJsonOfCurrentFragment() {
        String json = "";
        switch (currentFragment) {
            case 0:
                json = fragmentExImStyle.getJsnoStyle();
                break;
            case 1:
                json = fragmentExImObjects.getJsnoStyle();
                break;
            case 2:
                json = fragmentExImGeoJson.getJsnoStyle();
                break;
            case 3:
                json = fragmentExImMapStudio.getJsnoStyle();
                break;
        }
        return Tools.formatJsonString(json, ActivityExport.this).toString();
    }

    private void importFromString(String text) {

        Gson gson = new Gson();
        switch (currentFragment) {
            case 0:
                ArrayList<GraphicJson> graphicJsons = new ArrayList<>();
                try {
                    graphicJsons = gson.fromJson(text, new TypeToken<List<GraphicJson>>() {
                    }.getType());
                } catch (JsonSyntaxException syntaxEx) {
                    String message = syntaxEx.getLocalizedMessage().substring(
                            syntaxEx.getLocalizedMessage().indexOf(":") + 2,
                            syntaxEx.getLocalizedMessage().length());
                    Dialogs.messageDialog(ActivityExport.this, getString(R.string.error), message).show();
                    break;
                }

                if (graphicJsons.isEmpty()) {
                    Dialogs.messageDialog(ActivityExport.this, getString(R.string.error), getString(R.string.no_object_to_import)).show();
                } else {
                    Bundle data = new Bundle();
                    data.putSerializable(KEY_STYLE_JSON, graphicJsons);
                    Intent intent = new Intent();
                    intent.putExtras(data);
                    setResult(RESULT_CODE_SET_STYLER, intent);
                    finish();
                }
                break;
            case 1:
                ArrayList<MapObject> mapObjects = new ArrayList<>();
                try {
                    mapObjects = gson.fromJson(text, new TypeToken<List<MapObject>>() {
                    }.getType());
                } catch (JsonSyntaxException syntaxEx) {
                    String message = syntaxEx.getLocalizedMessage().substring(
                            syntaxEx.getLocalizedMessage().indexOf(":") + 2,
                            syntaxEx.getLocalizedMessage().length());
                    Dialogs.messageDialog(ActivityExport.this, getString(R.string.error), message).show();
                    break;
                }

                if (mapObjects.isEmpty()) {
                    Dialogs.messageDialog(ActivityExport.this, getString(R.string.error), getString(R.string.no_object_to_import)).show();
                } else {
                    Bundle data = new Bundle();
                    data.putSerializable(KEY_OBJECT_LIST, mapObjects);
                    Intent intent = new Intent();
                    intent.putExtras(data);
                    setResult(RESULT_CODE_SET_OBJECTS, intent);
                    finish();
                }
                break;
            case 2:
                ArrayList<MapObject> mapObj = null;
                try {
                    mapObj = Tools.geoJsonToMapObjects(text);
                } catch (Exception e) {
                    Dialogs.messageDialog(ActivityExport.this, getString(R.string.error), getString(R.string.no_object_to_import)).show();
                }

                if (mapObj == null || mapObj.isEmpty()) {
                    Dialogs.messageDialog(ActivityExport.this, getString(R.string.error), getString(R.string.no_object_to_import)).show();
                } else {
                    Bundle data = new Bundle();
                    data.putSerializable(KEY_OBJECT_LIST, mapObj);
                    Intent intent = new Intent();
                    intent.putExtras(data);
                    setResult(RESULT_CODE_SET_OBJECTS, intent);
                    finish();
                }
                break;
            case 3:
                MapStudioJson mapStudioJson = null;
                try {
                    mapStudioJson = gson.fromJson(text, new TypeToken<MapStudioJson>() {
                    }.getType());
                } catch (JsonSyntaxException syntaxEx) {
                    String message = syntaxEx.getLocalizedMessage().substring(
                            syntaxEx.getLocalizedMessage().indexOf(":") + 2,
                            syntaxEx.getLocalizedMessage().length());
                    Dialogs.messageDialog(ActivityExport.this, getString(R.string.error), message).show();
                    break;
                }
                if (mapStudioJson != null) {

                    if (mapStudioJson.getMapobjects() == null)
                        mapStudioJson.setMapobjects(new ArrayList<MapObject>());
                    if (mapStudioJson.getStyleoptions() == null)
                        mapStudioJson.setStyleoptions(new ArrayList<GraphicJson>());

                    if (mapStudioJson.getMapobjects().isEmpty() && mapStudioJson.getStyleoptions().isEmpty()) {
                        Dialogs.messageDialog(ActivityExport.this, getString(R.string.error), getString(R.string.no_object_to_import)).show();
                    } else {
                        Bundle data = new Bundle();
                        data.putSerializable(KEY_OBJECT_LIST, mapStudioJson.getMapobjects());
                        data.putSerializable(KEY_STYLE_JSON, mapStudioJson.getStyleoptions());
                        Intent intent = new Intent();
                        intent.putExtras(data);
                        setResult(RESULT_CODE_SET_MAPSTUDIO, intent);
                        finish();
                    }
                } else {
                    Dialogs.messageDialog(ActivityExport.this, getString(R.string.error), getString(R.string.no_object_to_import)).show();
                }
                break;
        }

    }

    private String getJsonFromFile(File file) {
        InputStream is = null;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            return "";
        }
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }

    @Override
    public void jsonChange(int current, boolean enabled) {
        if (current == currentFragment) {
            if (enabled)
                enableButtons();
            else
                disableButtons();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICKFILE_RESULT_CODE:
                if (resultCode == RESULT_OK) {
                    String filePath = data.getData().getPath();
                    importFromString(getJsonFromFile(new File(filePath)));
                }
                break;

        }
    }
}
