package eu.fukysoft.multymaps.Interfaces;

/**
 * Created by Majo on 23.10.2017.
 */

public interface ChangeJsonState {
    void jsonChange(int current, boolean enable);
}
