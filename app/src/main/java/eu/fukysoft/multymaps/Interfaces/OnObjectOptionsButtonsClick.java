package eu.fukysoft.multymaps.Interfaces;

import eu.fukysoft.multymaps.Models.MapObject;

/**
 * Created by Funky on 9/16/2017.
 */

public interface OnObjectOptionsButtonsClick {
    void onLocateObject(MapObject mapObject);
    void onEditObject(int id, int type);
}
